<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 * @author      Philipp Schuele <p.schuele@metaways.de>
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 */

/**
 * event class for deleted groups
 *
 * @package     Admin
 */
class Admin_Event_DeleteMailList extends Tinebase_Event_Abstract
{
    /**
     * array of mail list ids
     *
     * @var array
     */
    public $mailListIds;

}
