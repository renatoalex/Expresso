/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Alberto Reuter Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO - Serviço Federal de Processamento de Dados (https://www.serpro.gov.br)
 *
 */

Ext.ns('Tine.Admin.MailLists');

/**
 * @namespace   Tine.Admin.MailLists
 * @class       Tine.Admin.MailListEditDialog
 * @extends     Tine.widgets.dialog.EditDialog
 */
Tine.Admin.MailListEditDialog = Ext.extend(Tine.widgets.dialog.EditDialog, {
    /*
     * @private
     */
    windowNamePrefix: 'maillistEditWindow_',
    appName: 'Admin',
    recordClass: Tine.Admin.Model.MailList,
    newRecordClass: Tine.Admin.Model.MailList,
    recordProxy: Tine.Admin.MailListsBackend,
    evalGrants: false,

    /*
     * var maillist
     */
    maillist: null,

    /*
     * var gridId: handle grid pannels containner for all lists
     */
    gridId: {
        members: 'MailListMembers',
        externals: 'MailListExternals',
        admins: 'MailListAdmins',
        moderators: 'MailListModerators'
    },

    /*
     * var gridstore: handle the grid panels components stores
     */
    gridStore: {
        members: null,
        externals: null,
        admins: null,
        moderators: null
    },

    /**
     * @private
     */
    initComponent: function (){
        Tine.log.debug('Starting up Application MailList for add/edit entry ...');
        this.maillist = this.maillist ? this.maillist : new Tine.Admin.Model.MailList({}, 0);

        Tine.Admin.MailListEditDialog.superclass.initComponent.call(this);
    },

    /**
     * generic request success handler
     *
     * @param {Object} record
     */
    onRequestSuccess: function (record, closeWindow) {
        Tine.log.debug('onRequestSuccess MailListEditDialog');

        // after getting validation errors when saving, need to do a manual reload
        if (record == null) {
            this.saving = false;
            this.initRecord();
        } else {
            Tine.Admin.MailListEditDialog.superclass.onRequestSuccess.call(this, record, closeWindow);
        }
    },

    /**
     * @private
     */
    onRecordLoad: function () {
        Tine.log.debug('onRecordLoad fetched. Populating stores by now...');

        // interrupt process flow until dialog is rendered
        if (!this.rendered) {
            this.onRecordLoad.defer(250, this);
            return;
        }

        Tine.Admin.MailListEditDialog.superclass.onRecordLoad.call(this);

        //if send control is active, adjust tab "Moderators"
        var blistType = this.record.get('moderation');

        if((blistType == false) && (typeof(blistType) == 'boolean'))
        {
            Ext.getCmp('moderationType').setValue(true);
        } else if(blistType == '')
            {
                Ext.getCmp('moderationType').setValue(false);
            }

        if (!blistType) { // list type is open
            Ext.getCmp('sendControl').setDisabled(true);
        }
        else {
            Ext.getCmp('sendControl').setDisabled(false);
        }

        var act = this.record.get('uidnumber');

        if(act){

            // mail list members
            var responseMembers = {
                responseText: Ext.util.JSON.encode(this.record.get('members'))
            };
            this.membersRecord = Tine.Admin.MailListMembersBacked.recordReader(responseMembers);

            // mail list external members
            var responseExternalMembers = {
                responseText: Ext.util.JSON.encode(this.record.get('externalmembers'))
            };
            this.externalmembersRecord = Tine.Admin.MailListMembersBacked.recordReader(responseExternalMembers);

            // mail list admins
            var responseAdmins = {
                responseText: Ext.util.JSON.encode(this.record.get('admins'))
            };
            this.adminsRecord = Tine.Admin.MailListMembersBacked.recordReader(responseAdmins);

            // mail list moderators
            var responseModerators = {
                responseText: Ext.util.JSON.encode(this.record.get('notmoderated'))
            };
            this.moderatorsRecord = Tine.Admin.MailListMembersBacked.recordReader(responseModerators);

            //grid stores population
            Tine.log.debug('Starting data grid population...');

            //Members
            var gId = this.gridId.members;
            var gridMembers = Ext.getCmp(gId);
            var storeMembers = gridMembers.getStore();
            var dataMembers = this.membersRecord.get('results');
            Tine.log.debug('List members addresses mapped are counting by ' + dataMembers.length);
            this.storeAppendData('Members', storeMembers, dataMembers);
            storeMembers.sort('address', 'ASC');

            var myReader = new Ext.data.ArrayReader({
                idIndex: 0
            }, dataMembers);

            storeMembers.proxy.data = myReader;
            Tine.log.debug('Proxy data for members store is ' + storeMembers.proxy.data);

            //Administrators
            var gId = this.gridId.admins;
            var gridAdmins = Ext.getCmp(gId);
            var storeAdmins = gridAdmins.getStore();
            var dataAdmins = this.adminsRecord.get('results');
            Tine.log.debug('Admins members addresses mapped are counting by ' + dataAdmins.length);
            this.storeAppendData('Admins', storeAdmins, dataAdmins);
            storeAdmins.sort('address', 'ASC');

            var myReader = new Ext.data.ArrayReader({
                idIndex: 0
            }, dataAdmins);

            storeAdmins.proxy.data = myReader;
            Tine.log.debug('Proxy data for admins store is ' + storeAdmins.proxy.data);

            //External members
            var gId = this.gridId.externals;
            var gridExternals = Ext.getCmp(gId);
            var storeExternals = gridExternals.getStore();
            var dataExternals = this.externalmembersRecord.get('results');
            Tine.log.debug('External members addresses mapped are counting by ' + dataExternals.length);
            this.storeAppendData('Externals', storeExternals, dataExternals);
            storeExternals.sort('address', 'ASC');

            var myReader = new Ext.data.ArrayReader({
                idIndex: 0
            }, dataExternals);

            storeExternals.proxy.data = myReader;
            Tine.log.debug('Proxy data for externals store is ' + storeExternals.proxy.data);

            //Moderators
            var gId = this.gridId.moderators;
            var gridModerators = Ext.getCmp(gId);
            var storeModerators = gridModerators.getStore();
            var dataModerators = this.moderatorsRecord.get('results');
            Tine.log.debug('Moderators addresses mapped are counting by ' + dataModerators.length);
            this.storeAppendData('Moderators', storeModerators, dataModerators);
            storeModerators.sort('address', 'ASC');

            var myReader = new Ext.data.ArrayReader({
                idIndex: 0
            }, dataModerators);

            storeModerators.proxy.data = myReader;
            Tine.log.debug('Proxy data for moderators store is ' + storeModerators.proxy.data);
            if (dataModerators.length > 0) {
                Ext.getCmp('sendControl').expand();
            }
            else {
                Ext.getCmp('sendControl').collapse();
            }
        }
    },

    /*
     * onRecordUpdate event: fired on edit/save a record
     * @returns
     */
    doRecordUpdating: function () {
        Tine.log.debug('Tine.Admin.MailListEditDialog::onRecordUpdate event fired...');
        Tine.Admin.UserEditDialog.superclass.onRecordUpdate.call(this);

        var storeExternals = this.gridStore.externals;
        var storeMembers = this.gridStore.members;
        var storeAdmins = this.gridStore.admins;
        var storeModerators = this.gridStore.moderators;

        //External members store add
        var newExternals = [];
        this.gridStore.externals.each(function (address) {
            newExternals.push(address.data);
        });
        this.record.set('externalmembers', newExternals);

        //Members store add
        var newMembers = [];
        this.gridStore.members.each(function (address) {
            newMembers.push(address.data);
        });
        this.record.set('members', newMembers);

        //Admins store add
        var newAdmins = [];
        this.gridStore.admins.each(function (address) {
            newAdmins.push(address.data);
        });
        this.record.set('admins', newAdmins);

        //Moderators store add
        var newModerators = [];
        this.gridStore.moderators.each(function (address) {
            newModerators.push(address.data);
        });
        this.record.set('notmoderated', newModerators);

        //Flag list moderation
        if(Ext.getCmp('sendControl').collapsed || Ext.getCmp('moderationType').getValue() === false){
            this.record.set('notmoderated', []);
        }

        this.record.set('moderation', Ext.getCmp('moderationType').getValue());

        //Prevent commit changes
        this.record.commit();

        Tine.log.debug('doRecordUpdating is this.record => ' + this.record);
    },

    /*
     *
     * @param Store store
     * @param Record data
     * @returns void
     */
    storeAppendData: function (gridId, store, data) {
        Tine.log.debug('storeAppendData received "' + gridId + '" as ref.');
        if (store && data) {
            if (data.length > 0) {
                store.loadData({results: data}, true);
                store.reload();
                Tine.log.debug('Apped data now points ' + store.getTotalCount() + ' elements on store now.');
                Ext.getCmp(gridId + 'Total').setValue(this.app.i18n.gettext('Participants:') + store.getTotalCount());
            }
            else {
                Tine.log.debug('The store have no data to populate!');
            }
        }
        else {
            Tine.log.debug('The store could not be appended: no data or store provided');
        }

    },

    /*
     *
     * @param {type} storeName
     * @param {type} data
     * @returns Ext.data.ArrayStore
     */

    storeDataComponents: function (record, storeName) {
        var recordData = this.record.get(record);
        if (recordData) {
            if (recordData.indexOf(' ') !== -1) {
                var storeData = [];

                var recordArray = recordData.split(' ');
                if (recordData.length > 0) {
                    Tine.log.debug('Total count of data ' + storeName + ' to handle is:' + recordArray.length);

                    for (var i = 0; i < recordArray.length; i++) {
                        storeData.push([recordArray[i]]);
                    }
                }
                else {
                    storeData.push([recordData]);
                }

                this[storeName] = new Ext.data.ArrayStore({
                    autoDestroy: true,
                    fields: ['address'],
                    data: storeData
                });
                Tine.log.debug('There are "' + this[storeName].getTotalCount() + '" itens added at store ' + storeName);
            }
            else {
                this[storeName] = new Ext.data.ArrayStore({
                    autoDestroy: true,
                    fields: ['address'],
                    data: recordData
                });
                Tine.log.debug('There is "1" item added at store ' + storeName);
            }
            return(this[storeName]);
        }
        else {
            Tine.log.debug('WARN! ' + storeName + ' does not have data');
            return(null);
        }
    },

    /*
     * Create default JSON Store for grid component
     * @param String storeRef
     * @returns Ext.data.JsonStore
     */
    createGridStore: function (storeRef) {
        //Create default Json store component to retrieve/store records from grid
        Tine.log.debug('MailList creating JSON store for grid component "' + storeRef + '"');
        storeRef = new Ext.data.JsonStore({
            root: 'results',
            id: 'address',
            fields: ['address'],
            proxy: new Ext.data.MemoryProxy([]),
            reader: new Ext.data.JsonReader({
                root: 'results',
                totalProperty: 'totalcount',
                fields: [{name: 'address'}]
            })
        });
        return(storeRef);
    },

    /*
     * Configures each grid panel with his own store and Id referals
     * @param String nameRef
     * @param Ext.data.JsonStore storeRef
     * @param String IdRef
     * @returns Ext.grid.GridPanel
     */
    setupGridAccounts: function (nameRef, storeRef, IdRef) {
        //Create default grid panel component for UI entries
        Tine.log.debug('Setting up GridPanel for "' + nameRef + '"');
        var grid = new Ext.grid.GridPanel({
            renderTo: Ext.getBody(),
            id: IdRef,
            store: storeRef,
            cm: this.columnModel(),
            autoSizeColumns: true,
            selModel: this.rowSelectionModel(),
            enableColLock: false,
            autoExpandColumn: 'email',
            border: false,
            view: new Ext.grid.GridView({
                autoFill: false,
                forceFit: false,
                ignoreAdd: true,
                emptyText: this.app.i18n.gettext('None address found.')
            }),
            listeners: {
                'rowcontextmenu': function (grid, index, event) {
                    event.stopEvent();
                    if (!grid.getSelectionModel().isSelected(index)) {
                        grid.getSelectionModel().selectRow(index);
                    }

                    if (!this.contextMenu) {
                        this.translation = new Locale.Gettext();
                        this.translation.textdomain('Admin');

                        this.contextMenu = new Ext.menu.Menu({
                            id: 'ctxMenuMembers',
                            items: [
                                new Ext.Action({
                                    text: this.translation.gettext('Remove Member'),
                                    disabled: false,
                                    handler: function () {
                                        Ext.MessageBox.confirm(this.translation.gettext('Confirm'), this.translation.gettext('Do you really want to delete the selected address(ess)?'), function (button) {
                                            if (button === 'yes') {
                                                var gridCmp = grid;
                                                var selectedRows = gridCmp.getSelectionModel().getSelections();
                                                var gridStore = gridCmp.getStore();

                                                for (var i = 0; i < selectedRows.length; ++i) {
                                                    Tine.log.debug('MailList removing the following address: ' + selectedRows[i].id);
                                                    gridStore.remove(selectedRows[i]);
                                                    gridStore.totalLenght--;
                                                }

                                                gridStore.reload();
                                                gridCmp.getView().refresh();
                                                Ext.getCmp(nameRef + 'Total').setValue(this.translation.gettext('Participants:') + gridStore.getCount());
                                            }
                                        }, this);
                                    },
                                    iconCls: 'action_delete',
                                    scope: this
                                })
                            ]
                        });
                    }
                    this.contextMenu.showAt(event.getXY());
                }
            }
        });
        return(grid);
    },

    /*
     *
     * @param String nameRef
     * @param Ext.data.JsonStore storeRef
     * @param String IdRef
     * @param Int panelHeight
     * @returns Ext.ui partial output (embeded tab content)
     */
    createGridGUI: function (nameRef, storeRef, IdRef, panelHeight, useSearch) {
        Tine.log.debug('MailList creating Grid GUI for "' + nameRef + '"');
        var xgrid = [{
                title: this.app.i18n.gettext('Subscribe new address'),
                layout: 'column',
                border: false,
                autoheight: false,
                items: [
                    [Tine.widgets.form.RecordPickerManager.get('Addressbook', 'Contact', {
                        columnWidth: .99,
                        fieldLabel: this.app.i18n._('Organizer'),
                        flex: 1,
                        name: nameRef + 'organizer',
                        id: nameRef + 'organizer',
                        userOnly: true,
                        hidden: !useSearch,
                        autoSelect: false,
                        blurOnSelect: true,
                        allowBlank: true,
                        setValue: function() {
                            var translation = new Locale.Gettext();
                            translation.textdomain('Admin');
                            if(this.selectedRecord){
                                var newaddress = this.selectedRecord.get('email');
                                if (storeRef.getById(newaddress)) {
                                    Ext.MessageBox.show({
                                        title: translation.gettext('Address already exists'),
                                        msg: translation.gettext('The selected mail already exists at this collection!'),
                                        buttons: Ext.MessageBox.OK,
                                        fn: function () {
                                            Ext.MessageBox.hide();
                                        },
                                        icon: Ext.MessageBox.INFO
                                    });
                                }
                                else{
                                    var newOne = {results: {address: newaddress}};
                                    storeRef.loadData(newOne, true);
                                    storeRef.reload();
                                    storeRef.sort('address', 'ASC');
                                    Ext.getCmp(nameRef + 'Total').setValue(translation.gettext('Participants:') + storeRef.getCount());
                                }
                            }
                            this.clearValue();
                            this.selectedRecord = null;
                        },
                    })],
                    {
                        xtype: 'textfield',
                        columnWidth: 0.89,
                        fieldLabel: this.app.i18n.gettext('E-mail'),
                        name: nameRef + 'Email',
                        id: nameRef + 'Email',
                        allowBlank: true,
                        maxLength: 255,
                        vtype: 'email',
                        hidden: useSearch,
                        vtypeText: this.app.i18n.gettext('Please inform a valid e-mail'),
                        listeners: {
                            'change': function () {
                                if (this.isValid()){
                                    Ext.getCmp('btnAddNew' + nameRef).setDisabled(false);
                                }
                                else {
                                    Ext.getCmp('btnAddNew' + nameRef).setDisabled(true);
                                }
                            }
                        }
                    },
                    {
                        columnWidth: 0.1,
                        id: 'btnAddNew' + nameRef,
                        xtype: 'button',
                        hidden:useSearch,
                        text: this.app.i18n.gettext('Add'),
                        handler: function () {
                            var translation = new Locale.Gettext();
                            translation.textdomain('Admin');

                            var newaddress = Ext.getCmp(nameRef + 'Email').getValue();

                            if (storeRef.getById(newaddress)) {
                                Ext.MessageBox.show({
                                    title: translation.gettext('Address already exists'),
                                    msg: translation.gettext('The selected mail already exists at this collection!'),
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        Ext.MessageBox.hide();
                                    },
                                    icon: Ext.MessageBox.INFO
                                });
                            }
                            else {
                                var newOne = {results: {address: newaddress}};
                                storeRef.loadData(newOne, true);
                                storeRef.reload();
                                storeRef.sort('address', 'ASC');
                                Ext.getCmp(nameRef + 'Total').setValue(translation.gettext('Participants:') + storeRef.getCount());
                                Ext.getCmp(nameRef + 'Email').setValue('');
                            }
                        }
                    },
                    {
                        xtype: 'panel',
                        border: false,
                        title: this.app.i18n.gettext('Already subscribed'),
                        layout: 'fit',
                        autoheight: false,
                        autoScroll: true,
                        height: panelHeight,
                        columnWidth: 1,
                        items: [
                            this.setupGridAccounts(nameRef, storeRef, IdRef)
                        ]
                    },
                    {
                        columnWidth: 0.75,
                        height: 30,
                        xtype: 'displayfield',
                        name: nameRef + 'TotalSpacer',
                        id: nameRef + 'TotalSpacer',
                        fieldLabel: '',
                        value: ''
                    },
                    {
                        style: {
                            marginTop: '5px'
                        },
                        columnWidth: 0.25,
                        height: 30,
                        xtype: 'displayfield',
                        name: nameRef + 'Total',
                        id: nameRef + 'Total',
                        fieldLabel: 'This total records count',
                        value: this.app.i18n.gettext('Participants:') + '0'
                    }
                ]
            }];
        return(xgrid);
    },

    /*
     * Initialize mail list resource
     * @param String listName
     * @returns Ext.ui resource (tab panel grid with embeded tabs)
     */
    initMailList: function (listName) {
        //Starting create default stores for component
        Tine.log.debug('MailList initialization for "' + listName + '"...');
        switch (listName) {
            case 'Members':
                this.gridStore.members = this.createGridStore(listName);
                var idRef = this.gridId.members;
                var storeList = this.gridStore.members;
                var panelHeight = 268;
                var useSearch = true;
                break;
            case 'Externals':
                this.gridStore.externals = this.createGridStore(listName);
                var idRef = this.gridId.externals;
                var storeList = this.gridStore.externals;
                var panelHeight = 268;
                var useSearch = false;
                break;
            case 'Admins':
                this.gridStore.admins = this.createGridStore(listName);
                var idRef = this.gridId.admins;
                var storeList = this.gridStore.admins;
                var panelHeight = 268;
                var useSearch = true;
                break;
            case 'Moderators':
                this.gridStore.moderators = this.createGridStore(listName);
                var idRef = this.gridId.moderators;
                var storeList = this.gridStore.moderators;
                var panelHeight = 150;
                var useSearch = true;
                break;
        }

        //Create GUI component
        var gridUI = this.createGridGUI(listName, storeList, idRef, panelHeight, useSearch);
        return(gridUI);
    },

    // grid defaults column model
    columnModel: function () {
        return(new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                resizable: false
            },
            columns: [
                {
                    id: 'email',
                    header: 'E-mail',
                    dataIndex: 'address',
                    hidden: false,
                    width: 200
                }
            ]
        }));
    },

    // grid defaults row selection model
    rowSelectionModel: function () {
        return(new Ext.grid.RowSelectionModel({multiSelect: false}));
    },

    /**
     * initModeration
     */
    initModeration: function () {
        return {
                xtype: 'fieldset',
                title: this.app.i18n.gettext('Activate send control'),
                id: 'sendControl',
                checkboxToggle: true,
                collapsed: true,
                autoScroll: true,
                columnWidth: 1,
                items: this.initMailList('Moderators'),
                validator: function() {
                                if (!this.collapsed && Ext.getCmp('MailListModerators').getStore().data.length === 0) {
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
            };
    },

    handlerApplyChanges: function (button, event, closeWindow) {
        console.log('No handlerApplyChanges:' + this.record);
    },


    onSaveAndClose: function () {
        Ext.MessageBox.wait(this.app.i18n.gettext('Please wait'), this.app.i18n.gettext('Updating Mail List...'));
        this.doRecordUpdating();
        if (this.isValid()) {
            if (Ext.getCmp('sendControl').validator()) {
                Ext.Ajax.request({
                    params: {
                        method: 'Admin.saveMailList',
                        data: this.record.data
                    },
                    success: function (response) {
                        var jsonData = Ext.util.JSON.decode(response.responseText);
                        Ext.MessageBox.hide();
                        if((jsonData.success == true) && (!jsonData.errormsg)){
                            Ext.MessageBox.alert(this.app.i18n.gettext('Success'), this.app.i18n.gettext('Mail list have been saved!'), function(btn){
                                if (btn == 'ok'){
                                    if (Tine.Tinebase.registry.get('preferences').get('windowtype')=='Browser') {
                                        this.window.opener.Tine.Admin.MailLists.gridPanel.loadGridData();
                                        this.window.close();
                                    }
                                    else {
                                        this.window.window.Tine.Admin.MailLists.gridPanel.loadGridData();
                                        Ext.WindowMgr.getActive().close();
                                    }
                                }
                            });
                        }
                        else{
                            Ext.MessageBox.alert(this.app.i18n.gettext('Failure'), this.app.i18n.gettext(jsonData.errormsg), function(btn){});
                        }
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert(this.app.i18n.gettext('Failed'), this.app.i18n.gettext('Could not save mail list!'));
                    },
                    scope: this
                });
            }
            else {
                Ext.MessageBox.alert(this.app.i18n.gettext('Failed'), this.app.i18n.gettext('Send control is active, please choose at least one sender'));
            }
        }
        else {
            Ext.MessageBox.alert(this.app.i18n.gettext('Failed'), this.app.i18n.gettext('Could not save mail list! Please fix all errors before continuing'));
        }
    },

    onRender: function (ct, position) {
        Tine.log.debug('MailList onRender event reached.');
        Tine.widgets.dialog.EditDialog.superclass.onRender.call(this, ct, position);
        // generalized keybord map for edit dlgs
        var map = new Ext.KeyMap(this.el, [{
                key: [10, 13], // ctrl + return
                ctrl: true,
                fn: this.handlerApplyChanges.createDelegate(this, [true], true),
                scope: this
            }]);

        this.loadMask = new Ext.LoadMask(ct, {msg: String.format(_(this.app.i18n.gettext('Transfering {0}...')), this.app.i18n.gettext('mail list'))});
        this.loadMask.show();
    },

    /**
     * returns dialog
     */
    getFormItems: function () {
        this.displayFieldStyle = {
            border: 'silver 1px solid',
            padding: '3px',
            height: '11px'
        };

        var editGroupDialog = {
            xtype: 'tabpanel',
            id: 'tabpanelMailList',
            border: false,
            plain: true,
            activeTab: 0,
            defaults: {
                border: false,
                frame: true,
                layoutConfig: {type: 'fit', align: 'stretch', pack: 'start'}
            },
            items: [{//tab main
                    title: this.app.i18n.gettext('Main'),
                    id: 'tabMain',
                    items: [{
                            xtype: 'columnform',
                            border: false,
                            autoHeight: true,
                            items: [[
                                    {
                                        columnWidth: 1,
                                        fieldLabel: this.app.i18n.gettext('Address'),
                                        xtype: 'textfield',
                                        name: 'mail',
                                        id: 'mail',
                                        allowBlank: false,
                                        maxLength: 255,
                                        vtype: 'email',
                                        vtypeText: this.app.i18n.gettext('Please inform a valid e-mail')
                                    }, {
                                        columnWidth: 1,
                                        fieldLabel: this.app.i18n.gettext('Name'),
                                        xtype: 'textfield',
                                        name: 'name',
                                        id: 'name',
                                        allowBlank: true,
                                        maxLength: 255
                                    }, {
                                        columnWidth: 1,
                                        fieldLabel: this.app.i18n.gettext('Description'),
                                        xtype: 'textfield',
                                        name: 'description',
                                        id: 'description',
                                        allowBlank: true,
                                        maxLength: 255
                                    }, {
                                        columnWidth: 0.25,
                                        xtype: 'combo',
                                        fieldLabel: this.app.i18n.gettext('List type'),
                                        name: 'moderation',
                                        id: 'moderationType',
                                        mode: 'local',
                                        triggerAction: 'all',
                                        allowBlank: false,
                                        editable: false,
                                        store: [[false, this.app.i18n.gettext('Open')], [true, this.app.i18n.gettext('Close')]],
                                        listeners: {
                                            'scope': this,
                                            'select': function (combo, record) {
                                                if (combo.getValue() === false) {
                                                    Ext.getCmp('sendControl').setDisabled(true);
                                                    Ext.getCmp('sendControl').collapse();
                                                }
                                                else {
                                                    Ext.getCmp('sendControl').setDisabled(false);
                                                }
                                            },
                                            'afterrender': function (combo) {
                                                var recordSelected = combo.getStore().getAt(0);
                                                combo.setValue(recordSelected.get('field1'));
                                            }
                                        }
                                    },
                                        this.initModeration()
                                    ]
                            ]
                        }
                    ]
                }, //tab members
                {
                    title: this.app.i18n.gettext('Members'),
                    id: 'tabMembers',
                    border: false,
                    frame: true,
                    layout: 'fit',
                    items: this.initMailList('Members')
                }
                , //tab external users
                {
                    title: this.app.i18n.gettext('External Members'),
                    id: 'tabExternal',
                    border: false,
                    frame: true,
                    layout: 'fit',
                    items: this.initMailList('Externals')
                }, //tab admins
                {
                    title: this.app.i18n.gettext('Administrators'),
                    id: 'tabAdmin',
                    border: false,
                    frame: true,
                    layout: 'fit',
                    items: this.initMailList('Admins')
                }],
            listeners: {
                'tabchange': function () {
                }
            }
        };

        return editGroupDialog;
    }
});

/**
 * Main window opener event
 */
Tine.Admin.MailListEditDialog.openWindow = function (config) {
    var window = Tine.WindowFactory.getWindow({
        width: 600,
        height: 460,
        name: Tine.Admin.MailListEditDialog.prototype.windowNamePrefix + Ext.id(),
        contentPanelConstructor: 'Tine.Admin.MailListEditDialog',
        contentPanelConstructorConfig: config
    });
    return window;
};
