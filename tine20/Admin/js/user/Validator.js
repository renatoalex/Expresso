/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 */

/*global Ext, Tine*/

Ext.ns('Tine.Admin.user');

/**
 * @namespace   Tine.Admin.user
 * @class       Tine.Admin.UserValidator
 *
 * NOTE: this class dosn't use the user namespace as this is not yet supported by generic grid
 *
 * <p>User Validator</p>
 * <p>
 * </p>
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 *
 * @constructor
 * Create a new Tine.Admin.UserValidator
 */
Tine.Admin.UserValidator = {
    /**
     * General validation for account login name
     */
    checkAccountLoginName: function(value){
        var ok = true;

        var locale = Tine.Tinebase.registry.get('locale').locale;

        switch (locale) {
            case 'en':
                break;
            case 'pt_BR':
                ok = this.checkBrazilianAccountLoginName(value);
                break;
            default:
                break;
        }
        return ok;
    },

    /**
     * Validation for a brazilian pattern of account login name
     * named CPF (Cadastro de Pessoa Física)
     */
    checkBrazilianAccountLoginName: function(value){
        var ok = true;
        if ((value.length != 11) || (/[^0-9]/g.test(value)) || (value == '00000000000')){
            ok = false;
        }
        if (ok){
            var total = 0;
            for (i=1; i<=9; i++){
                total = i * 11111111111;
                if (total.toString() == value){
                    i = 10;
                    ok = false;
                }
            }
            if (ok){
                total = 0;
                var remainder;
                for (var i=1; i<=9; i++){
                    total = total + parseInt(value.substring(i-1, i)) * (11 - i);
                }
                remainder = (total * 10) % 11;
                if ((remainder == 10) || (remainder == 11)){
                    remainder = 0;
                }
                if (remainder != parseInt(value.substring(9, 10)) ){
                    ok = false;
                }
                if (ok){
                    total = 0;
                    for (i = 1; i <= 10; i++){
                        total = total + parseInt(value.substring(i-1, i)) * (12 - i);
                    }
                    remainder = (total * 10) % 11;
                    if ((remainder == 10) || (remainder == 11)){
                        remainder = 0;
                    }
                    if (remainder != parseInt(value.substring(10, 11) ) ){
                        ok = false;
                    }
                }
            }
        }
        return ok;
    }
};