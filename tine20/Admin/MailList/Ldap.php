<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailList
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 */

/**
 * MailList LDAP backend
 *
 * @package     Admin
 * @subpackage  MailList
 */
use \Tinebase_Backend_Interface,
    \Tinebase_Model_Filter_FilterGroup,
    \Tinebase_Core,
    \Tinebase_Record_Interface;

class Admin_MailList_Ldap implements Tinebase_Backend_Interface
{
    /**
     * index of the id attribute of the mail list on the LDAP
     */
    const IDCOL = 'id';

    /**
     * Default attribute to order on a LDAP search operation
     */
    const DEFAULTORDER = 'mail';

    /**
     * Max number of results in a LDAP search if no maxresults is set
     */
    const DEFAULTMAXRESULTS = NULL;

    /**
     * date representation used by ldap
     */
    const LDAPDATEFORMAT = 'YmdHis';

    /**
     * Array of LDAP base options for internal use
     * @var array
     */
    protected $_options = array();

    /**
     * Object Tinebase_Ldap backend internals object
     * @var Tinebase_Ldap
     */
    protected $_backend = NULL;

    /**
     * Name of the LDAP attribute which identifies a mail list uniquely
     * @var string
     */
    protected $_mailListUIDAttribute;

    /**
     * Mapping array from LDAP attributes to Admin class properties object
     * @var array
     */
    protected $_attributes = array(
        'uid'                   => 'uid',
        'uidnumber'             => 'uidnumber',
        'gidNumber'             => 'gidNumber',
        'name'                  => 'cn',
        'sname'                 => 'sn',
        'description'           => 'description',
        'mail'                  => 'mail',
        'admins'                => 'admlista',
        'moderation'            => 'defaultMemberModeration',
        'notmoderated'          => 'naoModerado',
        'members'               => 'mailforwardingaddress',
        'externalmembers'       => 'mailforwardingaddress',
        'accountStatus'         => 'accountStatus',
        'accountDN'             => 'dn',
        'homeDirectory'         => 'homeDirectory',
        'loginShell'            => 'loginShell',
        'deliveryMode'          => 'deliveryMode',
        'phpgwAccountType'      => 'phpgwAccountType',
        'phpgwAccountStatus'    => 'phpgwAccountStatus',
        'phpgwAccountExpires'   => 'phpgwAccountExpires',
        'givenName'             => 'givenName',
        'archivePrivate'        => 'archivePrivate',
    );

    /**
     * Mapping array from LDAP basic attributes to Admin class properties object
     * @var array
     */
    protected $_basicAttributes = array(
        'uid'         => 'uid',
        'uidnumber'   => 'uidnumber',
        'mail'        => 'mail',
        'name'        => 'cn',
        'sname'       => 'sn',
        'description' => 'description',
        'admins'      => 'admlista',
        'moderation'  => 'defaultMemberModeration'
    );

    /**
     * Array of object classes required by mail list backend structure on LDAP server
     * @var array
     */
    protected $_requiredObjectClass = array(
        'posixAccount',
        'inetOrgPerson',
        'shadowAccount',
        'top',
        'person',
        'organizationalPerson',
        'mailman'
    );

    /*
     * Array of mandatory LDAP attributes to fetch any new entry
     * @var array
     */

    protected $_mandatoryAttributes = Array(
        'homeDirectory'         => '/dev/null',
        'loginShell'            => '/bin/false',
        'deliveryMode'          => 'forwardOnly',
        'givenName'             => 'MailList',
        'accountStatus'         => 'active',
    );

    /**
     * Array of options to default grid pagination functionality
     * @var array
     */
    protected $_defaultPagination = array(
        'filter' => null,
        'start' => 0,
        'limit' => 50,
        'sort' => self::DEFAULTORDER,
        'dir' => 'ASC',
    );

    /**
     * Array of options to essential pagination functionality on editing register
     * @var array
     */
    protected $_uniquePagination = array(
        'filter' => null,
        'start' => 0,
        'limit' => 1,
        'sort' => null,
        'dir' => 'ASC',
    );

    /**
     * LDAP base dn to work on
     *
     * @var string
     */
    protected $_baseDn;

    /**
     * LDAP gid number attribute
     *
     * @var string
     */
    protected $_gid;

    /**
     * LDAP basic group filter for mail lists
     *
     * @var string
     */
    protected $_mailListBaseFilter = 'objectclass=posixaccount';

    /**
     * LDAP basic user search scope
     *
     * @var integer
     */
    protected $_mailListSearchScope = Zend_Ldap::SEARCH_SCOPE_SUB;

    /**
     * LDAP query filter to search
     *
     * @var string
     */
    protected $_queryFilter = '(uid=%s)(mail=%s)(cn=%s)(admlista=%s)(mailforwardingaddress=%s)(defaultmembermoderation=%s)(notmoderated=%s)(givenName=%s)';

    /**
     * LDAP backend oprataion access control
     *
     * @var boolean
     */
    protected $_isReadOnlyBackend = false;

    /**
     * Default string do perform a search operation
     *
     * @var string
     */
    protected $_default_sort_attribute = 'mail';

    /**
     * Default registry counter information
     *
     * @var integer
     */
    protected $_totalCount = 0;

    /**
     * The MailList object constructor
     *
     * @param  array  $_options  Options used in connecting, binding, etc. at LDAP server
     * @throws Tinebase_Exception_Backend_Ldap
     */
    public function __construct(array $_options = array())
    {
        try {
            if (empty($_options['mailListIdNumber'])) {
                $_options['mailListIdNumber'] = 'uidNumber';
            }
            if (empty($_options['baseDn'])) {
                $_options['baseDn'] = $_options['mailListDn'];
            }  
            if (empty($_options['gidNumber'])) {
                $_options['gidNumber'] = $_options['mailListGid'];
            }
            if (empty($_options['mailListSearchScope'])) {
                $_options['mailListSearchScope'] = $this->_mailListSearchScope;
            }
            if (empty($_options['mailListFilter'])) {
                $_options['mailListFilter'] = $this->_mailListBaseFilter;
            }
            if (isset($_options['requiredObjectClass'])) {
                $_options['objectclass'] = (array) $this->_requiredObjectClass;
            }
            if (isset($_options['mailListSchemas'])) {
                $this->_getExtraLDAPschemas();
            }
            if (array_key_exists('readonly', $_options)) {
                $this->_isReadOnlyBackend = (bool) $_options['readonly'];
            }

            $this->_options = $_options;

            $this->_mailListIdNumber = strtolower($this->_options['mailListIdNumber']);
            $this->_baseDn = $this->_options['mailListDn'];
            $this->_gid = $this->_options['gidNumber'];
            $this->_mailListBaseFilter = $this->_options['mailListFilter'];
            $this->_mailListSearchScope = $this->_options['mailListSearchScope'];

            $this->_rowNameMapping['mailListId'] = $this->_mailListIdNumber;

            try {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG))
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList default backend will try to create a LDAP object using following options:" . print_r($this->_options, true));

                //If there is a Mail List DN (setup)
                if ((empty($this->_baseDn) || ($this->_baseDn == ''))) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::ERR))
                        Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList backend LDAP BASE DN is empty!");
                    throw new Tinebase_Exception_Backend_Ldap('Admin MailList backend LDAP BASE DN is empty!');
                }

                //If there is a Mail List GID Number (setup)
                if ((empty($this->_gid) || ($this->_gid == ''))) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::ERR))
                        Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList backend LDAP GIDNUMBER is empty!");
                    throw new Tinebase_Exception_Backend_Ldap('Admin MailList backend LDAP GIDNUMBER is zero or empty!');
                }

                //If read only is not flagged (write access), check if there are master host configured too
                if (!$this->_isReadOnlyBackend) {
                    if ((isset($this->_options['masterLdapHost']) && $this->_options['masterLdapHost'] != "") && (isset($this->_options['masterLdapPassword']) && $this->_options['masterLdapPassword'] != "")) {
                        //Read and write backend LDAP Master
                        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList will use LDAP MASTER host to read/write ops.");
                        $this->__useLdapMaster();
                        Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . "  Admin MailList default read/write backend LDAP MASTER host started up successfully.");
                    }
                    else {
                        //Setup configuration is write access, but there is no Master host entry
                        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList LDAP configuration for read/write access: LDAP beckend is setted to read/write, but there is no entry for LDAP MASTER access.");
                        $this->__useLdapDefault(false);
                        Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . "  Admin MailList default read/write backend LDAP host started up successfully.");
                    }
                }
                else {
                    //Read only backend LDAP
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP is configured to read only access.");
                    $this->__useLdapDefault(true);
                    Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " System is not configured to allow user to do CUD operations on Mail Lists administration resources: LDAP host is read only access.");
                }
                //Bind perform
                $this->_backend = new Tinebase_Ldap($this->_options);
                $this->_backend->bind();
                Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . "  Admin MailList default backend LDAP started up successfully with bind operation.");
            } catch (Zend_Ldap_Exception $zle) {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to be build with bind operation.\n" . print_r($zle, true));
                throw new Tinebase_Exception_Backend_Ldap('Admin_MailList_Ldap could not bind to LDAP: ' . $zle->getMessage());
            }
        } catch (Tinebase_Exception_Backend_Ldap $tebl) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList default backend LDAP fails to be build\n." . print_r($tebl, true));
            throw new Tinebase_Exception_Backend_Ldap($tebl->getMessage());
        }
    }

    /**
     * Search for records matching given filter
     *
     * @param  Tinebase_Model_Filter_FilterGroup    $_filter
     * @param  Tinebase_Model_Pagination            $_pagination
     * @param  array|string|boolean                 $_cols columns to get, * per default / use self::IDCOL or TRUE to get only ids
     * @param  boolean                              $_doCache
     * @param  boolean                              $_doSaveCache
     * @param  boolean                              $_domainMismatch
     * @return Tinebase_Record_RecordSet
     *
     * @TODO use the $_cols option
     */
    public function search(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Model_Pagination $_pagination = NULL, $_cols = '*', $_doCache = TRUE, $_doSaveCache = TRUE, $_domainMismatch = FALSE)
    {
        try {
            if (!$this->_backend) {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP seems not to have a internal LDAP source to perform a \"serch\" operation:\n" . print_r($this, true));
                throw new Tinebase_Exception_Backend_Ldap("No LDAP link");
            }

            $pagination = $_pagination;
            if (is_null($pagination)) {
                $pagination = new Tinebase_Model_Pagination($this->_defaultPagination);
            }
            $cache = Tinebase_Core::getCache();
            $cacheId = $this->_getCacheId($_filter);
            $this->_cacheid = $cacheId;
            if ($_doCache === TRUE) {
                $result = $cache->load($cacheId);
                $maxresults = self::DEFAULTMAXRESULTS;
            } else {
                $result = FALSE;
                $maxresults = NULL;
            }

            $filter = $this->_decodeFilter($_filter, $pagination);
            //Still not in cache: will perform a ldap search
            if ($result === FALSE) {
                Tinebase_Core::getLogger()->debug(__METHOD__ . ':' . __LINE__ . ' Executing LDAP MailList Search with filter: "' . $filter['filter'] . '".');
                if ($_doSaveCache === false) {
                    $_cols = 'uid,uidnumber,name,gidNumber,description,mail,admins,moderation,notmoderated,members,accountStatus';
                }
                $attributes = $this->_generateAttributesArray($_cols);

                try {
                    $return = $this->_backend->search($filter['filter'], $this->_options['baseDn'], $this->_mailListSearchScope, $attributes, null, null, $maxresults);
                    $this->_totalCount = count($return);

                } catch (Tinebase_Exception_Backend_Ldap $tebl) {
                    Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to perform a \"serch\" operation:\n" . print_r($tebl, true));
                    throw $tebl;
                }

                $result = $this->_ldap2MailLists($return, $_domainMismatch);

                // Workaround. Prevent save empty searches to cache.because of connection to timeout
                if ($_doSaveCache === true && count($result) > 0) {
                    $cache->save($result, $cacheId, array('container', 'ldap'), null);
                }
            }
            try {
                $this->_totalCount = count($result);
                $result->sort($filter['sort'], $filter['dir'], 'natcasesort', SORT_REGULAR, true);
            } catch (Tinebase_Exception $te) {
                Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to perform a \"serch\" operation:\n" . print_r($tebl, true));
                throw $te;
            }

            if (!(is_null($_pagination))) {
                $result->returnPagination($filter['start'], $filter['limit']);
            }

            return $result;
        } catch (Tinebase_Exception_Backend_Ldap $tebl) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to perform \"serch\" operation: " . $tebl->getMessage() . "\nData dump:\n" . print_r($tebl, true));
            return($tebl);
        }
    }

    /**
     * Generate the Cache ID for the LDAP query
     *
     * @param $_filter
     * @return string
     */
    protected function _getCacheId($_filter)
    {
        $filters = $_filter->getFilterObjects();
        $params = array('MailListAdminBackendLdap');
        foreach ($filters as $filter){
            $params[] = implode('|', $filter->toArray());
        }
        $cacheId  = Tinebase_Helper::arrayHash($params);
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP is getting CacheID: " . $cacheId);
        return $cacheId;
    }

    /**
     * Decodes given Tine filter to LDAP functions
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param Tinebase_Model_Paginatin          $_pagination
     * @return array
     */
    protected function _decodeFilter(Tinebase_Model_Filter_FilterGroup $_filter, Tinebase_Model_Pagination $_pagination)
    {
        $arrFilters = array();
        $arrFields = array();
        $filters = $_filter->getFilterObjects();
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP will decode " . count($filters) . " filters now.");
        foreach ($filters as $filter) {
            $objClass = get_class($filter);
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP will decode filter class \"" . $objClass . "\" now.");
            switch ($objClass) {
                case 'Tinebase_Model_Filter_Query':
                    $value = $filter->getValue();
                    $operator = $filter->getOperator();
                    $arrFields = array();
                    $options = $filter->getOptions();
                    if (isset($options['fields'])) {
                        foreach ($options['fields'] as $field) {
                            $arrFields[] = $this->_attributes[$field];
                        }
                    }
                    $filter = '';
                    foreach ($arrFields as $field) {
                        $filter .= $this->_generateLdapFilter($value, $field, $operator);
                    }
                    break;

                default:
                    $value = $filter->getValue();
                    $field = $filter->getField();
                    $operator = $filter->getOperator();
                    $filter = '';
                    $filter .= $this->_generateLdapFilter($value, $this->_attributes[$field], $operator);
                    break;
            }

            if (!(empty($filter))) {
                if (count($arrFields) > 1) {
                    $filter = '(|' . $filter . ')';
                }
                $arrFilters[] = $filter;
            }
        }
        if (count($arrFilters) >= 1) {
            $filter = $this->_formatLdapFilter($arrFilters);
        }
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Admin MailList LDAP backend filter :"' . $filter . '"');
        $pagination = $_pagination->toArray();
        $return = array
            (
            'filter' => $filter,
            'start' => (isset($pagination['start'])) ? $pagination['start'] : $this->_defaultPagination['start'],
            'limit' => (isset($pagination['limit'])) ? $pagination['limit'] : $this->_defaultPagination['limit'],
            'sort' => (isset($pagination['sort'])) ? $pagination['sort'] : $this->_defaultPagination['sort'],
            'dir' => (isset($pagination['dir'])) ? strtoupper($pagination['dir']) : $this->_defaultPagination['dir']
        );
        return $return;
    }

    /**
     * Format filter to LDAP model
     *
     * @param array $_filter
     * @return string $filter
     */
    protected function _formatLdapFilter(array $_filter)
    {
        $filterP = $this->_options['mailListFilter'];
        $filterE = '';
        if (preg_match('/^\([&|].*\)$/', $filterP)) {
            $filterP = substr($filterP, 0, -1);
            $filterE = ')';
        } else if (preg_match('/^[^(].*[^)]$/', $filterP)) {
            $filterP = '(' . $filterP . ')';
        }

        $filter = '(&' . $filterP . implode('', $_filter) . $filterE . ')';

        return $filter;
    }

    /**
     * Generates the LDAP filter for a given Attribute. If no attribute is given it generates the search by id
     *
     * @param string  $_value
     * @param array   $_attribute
     * @param string  $_operator
     * @return string $return
     */
    protected function _generateLdapFilter($_value, $_attribute = NULL, $_operator = 'equals')
    {
        $return = '';
        $attribute = $_attribute;
        if (is_null($attribute)) {
            $attribute = $this->_attributes[self::IDCOL];
        }
        if ((!is_null($_value)) && (!empty($attribute))) {
            switch ($_operator) {
                case 'contains'://something=*value*
                    //google like search...
                    $_value = trim($_value);
                    $arrValue = explode(' ', $_value);
                    if (count($arrValue) > 1) {
                        $return = '(&';
                        foreach ($arrValue as $value) {
                            $return .= $this->_generateLdapFilter($value, $_attribute, $_operator);
                        }
                        $return .= ')';
                    } else {
                        $return = '(' . $attribute . '=*' . (!empty($_value) ? $_value . '*)' : ')');
                    }
                    break;
                case 'equals'://something=value
                    $return = '(' . $attribute . '=' . $_value . ')';
                    break;
                case 'startswith'://something=value*
                    $return = '(' . $attribute . '=' . $_value . '*)';
                    break;
                case 'endswith'://something=*value
                    $return = '(' . $attribute . '=*' . $_value . ')';
                    break;
                case 'not'://(!(uid=94813566049))
                    $return = '(!(' . $attribute . '=*' . $_value . '*))';
                    break;
                case 'in'://(|(something=*value*)(something=*value*)(something=*value*)...)
                    $arrValues = $_value;
                    if (!(is_array($arrValues))) {
                        $arrValues = explode(',', $_value);
                    }
                    $return = '';
                    foreach ($arrValues as $value) {
                        $return .= $this->_generateLdapFilter(trim($value), $attribute, 'contains');
                    }
                    $return = '(|' . $return . ')';
                    break;
                default:
                    throw new Tinebase_Exception_NotImplemented('Operator Not Implemented');
            }
        }
        return $return;
    }

    /**
     * Generate the Atrribute Array for ldapsearch
     * @param mixed $_cols
     * @return array
     */
    protected function _generateAttributesArray($_cols)
    {
        $return = array();
        if (($_cols === TRUE) || ($_cols == self::IDCOL)) {
            $return = array(self::IDCOL);
        } elseif (($_cols == '*') || (empty($_cols))) {
            //$return = array_values($this->_attributes);
            $return = array_values($this->_basicAttributes);
        } else {
            $attr = explode(',', $_cols);
            foreach ($attr as $atrib) {
                $return[] = $this->_attributes[$atrib];
            }
        }
        return $return;
    }

    /**
     * Get moderetion attribute
     *
     * @param array    $_data
     * @return boolean $val
     */
    protected function _moderationType($_data)
    {
        if (($_data[0] == '1') || ($_data[0] === 1) || ($_data[0] === true) || (strtolower($_data[0]) === 'true'))
            $val = true;
        else
            $val = false;
        return($val);
    }

    /**
    * Filter validate email default PHP pattern if applicable
    *
    * @param string $_data
    * @return boolean
    */
    protected function _validEmail($_data)
    {
        if (filter_var($_data, FILTER_VALIDATE_EMAIL)) {
            return(true);
        } else {
            return(false);
        }
    }

    /*
     * Check list members from app domain
     *
     * @param boolean $_check
     * @param array   $_list
     * @return array  $data
     */
    protected function _fromDomain($_check, $_list)
    {
        $admfront = new Admin_Frontend_Json();
        $defaultDomain = $admfront->getRegistryData();
        $domain = $defaultDomain['primarydomain'];

        $data = array();

        if ($_check) {
            for ($i = 0; $i < count($_list); $i++) {
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  checking if email is FROMDOMAIN: \"" . $_list[$i]['address'] . "\"");

                $partial = explode('@', $_list[$i]['address']);
                if ((count($partial) > 0) && (!empty($partial[1]))) {
                    if ($partial[1] === $domain) {
                        $data[] = $_list[$i];
                    }
                }
            }
        } else {
            for ($i = 0; $i < count($_list); $i++) {
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  checking if email is OUTSIDE: \"" . $_list[$i]['address'] . "\"");
                $partial = explode('@', $_list[$i]['address']);
                if ((count($partial) > 0) && (!empty($partial[1]))) {
                    if ($partial[1] != $domain) {
                        $data[] = $_list[$i];
                    }
                }
            }
        }

        return($data);
    }

    /*
     * Return a collection of email addresses
     *
     * @param integer $_index
     * @param array   $_data
     * @param string  $_key
     * @return array  $entries
     */
    protected function _entryToArraykey($_index, $_data, $_key)
    {
        $entries = array();
        $arrdata = array();
        for ($i = 0; $i < count($_data); $i++) {
            $arrdata[$_key] = $_data[$i];
            $entries[] = $arrdata;
        }
        return($entries);
    }

    /**
    * Get extra LDAP schemas from config and add it to $this->_requiredObjectClass
    *
    * @return void
    */
    protected function _getExtraLDAPschemas()
    {
        if(Tinebase_User::getBackendConfiguration('mailListSchemas') != ''){
          $extraSchemas = explode(';', Tinebase_User::getBackendConfiguration('mailListSchemas'));
          $extraSchemas = array_diff($extraSchemas, $this->_requiredObjectClass);
          $extraSchemas = array_map('strtolower', array_merge($extraSchemas, $this->_requiredObjectClass));
          $this->_requiredObjectClass = $extraSchemas;
        }
    }

     /**
     * Gets the ExtraLdapAttributes from Ldap Config and add it to the $ldapData array
     *
     * @param array $_LdapData
     * @return array
     */
    protected function _getExtraLDAPAttributes(array $_ldapData)
    {
        if(Tinebase_User::getBackendConfiguration('mailListAttributes') != ''){
            $extraAttributes = explode(';', Tinebase_User::getBackendConfiguration('mailListAttributes'));
            foreach ($extraAttributes as $attribute){
                $tmpArray = explode('=', $attribute);
                if(!array_key_exists($tmpArray[0],$_ldapData)){
                    $_ldapData[$tmpArray[0]] = $tmpArray[1];
                }
            }
        }
        return $_ldapData;
    }

    /**
     * Returns a record set of Admin_Model_MailList filled from raw LDAP data
     *
     * @param array  $_data raw LDAP mail list data
     * @param boolean $_domainmatch to separate members at main domain from others
     * @return Tinebase_Record_RecordSet of Admin_Model_MailList
     */
    protected function _ldap2MailLists($_data, $_domainmatch = FALSE)
    {
        try {
            $mailLists = array();
            foreach ($_data as $ldapEntry) {
                $data = array();
                foreach ($this->_attributes as $key => $value) {
                    $arrayIndex = strtolower($value);

                    switch ($arrayIndex) {
                        //Moderation flag handler
                        case 'defaultmembermoderation':
                            if (empty($ldapEntry[$arrayIndex])) {
                                $data[$key] = '';
                            } else {
                                $data[$key] = $this->_moderationType($ldapEntry[$arrayIndex]);
                            }
                            break;

                        //all the collection entries
                        case 'mailforwardingaddress':
                        case 'naomoderado':
                        case 'admlista':
                            if (empty($ldapEntry[$arrayIndex])) {
                                $data[$key] = false;
                            } else {
                                $data[$key] = $this->_entryToArraykey($arrayIndex, $ldapEntry[$arrayIndex], 'address');
                            }
                            break;

                        //default ldap entry values
                        default:
                            if (!empty($value)) {
                                if (!empty($ldapEntry[$arrayIndex])) {
                                    $data[$key] = implode((array)$ldapEntry[$arrayIndex], ' ');
                                } else {
                                    $data[$key] = '';
                                }
                            } else {
                                $data[$key] = '';
                            }
                    }
                }

                if ($_domainmatch) {
                    //Separate domain users from outside domain users, with sanitize output
                    if (count($data['members']) > 0) {
                        $fromDomain = $this->_fromDomain(true, $data['members']);
                        $fromOutside = $this->_fromDomain(false, $data['members']);
                        $data['externalmembers'] = array('results' => $fromOutside, 'totalcount' => count($fromOutside));
                        $data['members'] = array('results' => $fromDomain, 'totalcount' => count($fromDomain));
                    }
                }

                //Output sanitize for admins
                if (count($data['admins']) > 0) {
                    $data['admins'] = array('results' => $data['admins'], 'totalcount' => count($data['admins']));
                }

                //Output sanitize for moderators
                if (count($data['notmoderated']) > 0) {
                    $data['notmoderated'] = array('results' => $data['notmoderated'], 'totalcount' => count($data['notmoderated']));
                }

                $mailLists[] = new Admin_Model_MailList($data, true);
            }

            $mailLists = new Tinebase_Record_RecordSet('Admin_Model_MailList', $mailLists, true, self::LDAPDATEFORMAT);
            return $mailLists;
        } catch (Tinebase_Exception_Record_DefinitionFailure $ter) {
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList LDAP backend fails to build a RecordSet from fetch data:" . print_r($ter, true));
            throw $ter;
        }
    }

    /**
     * Returns a unique associative array with fetched data store email addresses containner
     *
     * @param array $__vecData
     * @return array $return
     */
    private function __CollectionToArray($__vecData)
    {
        $number = count($__vecData);
        $return = array();
        $data = array();

        for ($i = 0; $i < $number; $i++) {
            $data[] = $__vecData[$i]['address'];
        }
        $return = $data;
        return($return);
    }

    /*
     * Settings to use LDAP connection with read/write access
     * @return void
     */
    private function __useLdapMaster()
    {
        $this->_options['readonly'] = false;
        $this->_options['host'] = $this->_options['masterLdapHost'];
        $this->_options['username'] = $this->_options['masterLdapUsername'];
        $this->_options['password'] = $this->_options['masterLdapPassword'];
    }

     /*
     * Settings to use LDAP connection using read only access
     * @param  bool $writeable
     * @return void
     */
    private function __useLdapDefault($writeable)
    {
        $this->_options['readonly'] = (bool) $writeable;
        $this->_options['host'] = $this->_options['host'];
        $this->_options['username'] = $this->_options['username'];
        $this->_options['password'] = $this->_options['password'];
    }

    /**
     * Generates LDAP DN for new Mail List entry
     *
     * @param  Admin_Model_MailList $_maillist
     * @return string                  $newDn
     */
    private function __generateDn(Admin_Model_MailList $_maillist)
    {
        $baseDn = $this->_baseDn;

        $uidProperty = array_search('uid', $this->_attributes);
        if($_maillist->accountDN){
            $newDn = "uid={$_maillist->$uidProperty},{$_maillist->accountDN}";
        }else{
            $newDn = "uid={$_maillist->$uidProperty},{$baseDn}";
        }
        return $newDn;
    }


    /*
     * Generates new Mail List UIDNumber attribute
     */
    private function __generateUIDNumber()
    {
        $return = (int)time();
        return($return);
    }

    /**
     * Models the Admin_Model_MailList data to LDAP array format
     *
     * @param Admin_Model_MailList $_data the Admin_Model_MailList data from XMLHTTPRequest
     * @return array                  $_ldapdata array in LDAP format
     *
     */
    protected function _MailList2ldap(Admin_Model_MailList $_data)
    {
        $_ldapData = array();
        $_ldapData[$this->_attributes['admins']] = array("");
        $_ldapData[$this->_attributes['notmoderated']] = array("");
        $_ldapData[$this->_attributes['members']] = array("");
        $_ldapData[$this->_attributes['externalmembers']] = array("");

        foreach ($_data as $key => $value) {
            $ldapProperty = array_key_exists($key, $this->_attributes) ? $this->_attributes[$key] : false;

            if ($ldapProperty) {
                switch ($key) {
                    case 'admins':
                        $admlist = $this->__CollectionToArray($value);
                        if (count($admlist) > 0)
                            $_ldapData[$ldapProperty] = $admlist;
                        break;

                    case 'notmoderated':
                        $notmoderatedlist = $this->__CollectionToArray($value);
                        if (count($notmoderatedlist) > 0)
                            $_ldapData[$ldapProperty] = $notmoderatedlist;
                        break;

                    case 'members':
                    case 'externalmembers':
                        $memberslist = $this->__CollectionToArray($value);
                        if (count($memberslist) > 0){
                            if(array_key_exists($ldapProperty, $_ldapData)){
                                for($i = 0; $i<count($memberslist); $i++){
                                    array_push($_ldapData[$ldapProperty], $memberslist[$i]);
                                }
                            }
                            else{
                                $_ldapData[$ldapProperty] = $memberslist;
                            }
                        }
                        break;

                    default:
                        $_ldapData[$ldapProperty] = $value;
                        break;
                }
            }
        }

        $_ldapData = $this->_getExtraLDAPAttributes($_ldapData);

        return($_ldapData);
    }

     /**
     * countingSearch - Performs a compute operation to calculate entryes
     *
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param Tinebase_Model_Pagination         $_pagination
     * @return integer
     *
     */
    public function countingSearch(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Model_Pagination $_pagination = NULL)
    {
        if (!$this->_backend) {

        }
        $attributes = array("mail", "uid");

        $filter = $this->_decodeFilter($_filter, $_pagination);

        $countfilter = array(
            "filter" => $this->_mailListBaseFilter,
            "start" => 0,
            "limit" => null,
            "sort" => "mail",
            "dir" => "ASC"
        );

        try {
            $return = $this->_backend->search($this->_mailListBaseFilter, $this->_options['baseDn'], $this->_mailListSearchScope, $attributes, null, null, null);
        } catch (Tinebase_Exception_Backend_Ldap $tebl) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to perform a \"serch\" operation:\n" . print_r($tebl, true));
            throw $tebl;
        }
        return(count($return));
    }

     /**
     * searchCount - Returns count property
     *
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @return integer
     *
     */
    public function searchCount(Tinebase_Model_Filter_FilterGroup $_filter)
    {
        $_count = $this->_totalCount;
        return ($_count);
    }

    /**
     * get mail list by id
     *
     * @param   string $_name
     * @return  Admin_Model_MailList
     * @throws  Tinebase_Exception_Backend_Ldap
     */
    public function getById($_mailListId)
    {
        try {
            $mailListId = Admin_Model_MailList::convertMailListIdToInt($_mailListId);
            $searchfilter = array(0 => array("field" => "uid",
                    "operator" => "equals",
                    "value" => $mailListId));

            $filter = new Admin_Model_MailListFilter($searchfilter);
            //search uses FILTER, PAGINATION, COLUMNS AND CACHE
            $pg = new Tinebase_Model_Pagination($this->_uniquePagination);

            //search parameters: filter, pagination, columns, do_cache, save_cahce

            $fetchResult = $this->search($filter, $pg, NULL, FALSE, FALSE, TRUE);
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Mail List "' . $_mailListId . '" getById fetch result:' . print_r($fetchResult, true));

            if (!$fetchResult) {
                throw new Tinebase_Exception_Backend_Ldap('Mail list "' . $_mailListId . '" was not found at database.');
            }

            return $fetchResult;
        } catch (Tinebase_Exception_Backend_Ldap $tebl) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP fails to perform a \"getById\" operation:\n" . print_r($tebl, true));
            return(false);
        }
    }

    /**
     * delete mail list(s) in ldap backend
     * @param array $_mailListId
     * @return  boolean
     * @throws  Tinebase_Exception_Backend_Ldap
     */
    public function deleteMailList($_mailListId)
    {
        try {
            if ($this->_isReadOnlyBackend) {
                throw new Tinebase_Exception_Backend_Ldap('Backend LDAP is configured with READ ONLY access!<br/>Sorry, but none entry will be changed until setup review.');
            }

            $dropped = 0;
            for ($i = 0; $i < count($_mailListId); $i++) {
                $metaData = $this->_getMetaData($_mailListId[$i]);

                // check if mail list still exist in ldap
                if (!empty($metaData['dn'])) {
                    $this->_backend->delete($metaData['dn']);
                    Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' Mail List "' . $_mailListId[$i] . '" was dropped from LDAP backend.' . print_r($metaData, true));
                    $dropped++;
                }
                else {
                    Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' Mail List "' . $_mailListId[$i] . '" fails do be dropped from LDAP backend: His own "dn" is empty!' . print_r($metaData, true));
                    throw new Tinebase_Exception_Backend_Ldap('DN for mail list id "' . $_mailListId . '" is empty. Delete operation fails.');
                }
            }

            //All mail list was dropped
            if ($dropped == count($_mailListId)) {
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Mail List delete operation receive "' . count($_mailListId) . '" list(s) to remove, and all them was dropped.');
                $cache = Tinebase_Core::getCache();
                $cache->remove($this->_getCacheId());
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Admin MailList Backend LDAP will remove the CacheID: " . $this->_cacheid);
                return(true);
            }
            //Some drop fails
            else {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Mail List delete operation receive "' . count($_mailListId) . '" list(s) to remove, but some of them fails: ' . $dropped . ' lists has gone.');
                return(true);
            }
        } catch (Tinebase_Exception_Backend_Ldap $tenf) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' mail list not found in LDAP backend: ' . $_mailListId . print_r($tenf, true));
            throw $tenf;
        }
    }

    /*
     * Save mail list entry on LDAP backend
     *
     * @param array    $_listData
     * @return boolean $_succeded
     */
    public function saveMailList($_listData)
    {
        try {
            if ($this->_isReadOnlyBackend) {
                throw new Tinebase_Exception_Backend_Ldap('Backend LDAP is configured with READ ONLY access!<br/>Sorry, but none entry will be changed until setup review.');
            }

            if ((empty($_listData)) || (count($_listData) == 0)) {
                throw new Tinebase_Exception_InvalidArgument('Mail list data to save is empty or null!');
            }

            try {
                //New entry
                if (!array_key_exists('uidnumber', $_listData)) {
                    $newUID = $this->__generateUIDNumber();
                    $_listData['uidnumber'] = $newUID;
                    $partial = explode('@', $_listData['mail']);
                    $_listData['uid'] = $partial[0];
                    $_listData['sname'] = $_listData['name'];
                    $_listData['gidNumber'] = $this->_gid;

                    // Open/Close MailList control
                    if($_listData['moderation'] === true)
                    {
                        if(count($_listData['notmoderated']) > 0)
                        {
                            $_listData['moderation'] = 'TRUE';
                        }
                        else{
                            $_listData['moderation'] = 'FALSE';
                        }
                        $_listData['archivePrivate'] = 'mailListRestriction';
                    }

                    $mlist = array_merge($_listData, $this->_mandatoryAttributes);
                    $nlist = $this->_getExtraLDAPAttributes($mlist);
                    $mailList = new Admin_Model_MailList($nlist);

                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  New MailList entry model is mapped." . print_r($mailList, true));

                    $dn = $this->__generateDn($mailList);

                    $mailListFormated = $this->_MailList2ldap($mailList);
                    $mailListFormated['objectclass'] = $this->_requiredObjectClass;

                    Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . "  Ldap NEW Mail List will be added." . print_r($dn, true) . " MAIIL LIST DATA: " . print_r($mailListFormated, true));

                    try {
                        $this->_backend->add($dn, $mailListFormated);
                        //force clean up cache
                        $cache = Tinebase_Core::getCache();
                        $cache->remove($this->_getCacheId());
                        return(true);
                    } catch (Tinebase_Exception_Backend_Ldap $tebl) {
                        Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Ldap Mail List FAILS on adding new entry: " . print_r($tebl, true));
                        throw ($tebl->getMessage());
                    }
                }

                //Update
                else {
                    //Fetch mandatory data
                    $_listData['sname'] = $_listData['name'];
                    $_listData['gidNumber'] = $this->_gid;

                    // Open/Close MailList control
                    if($_listData['moderation'] === true)
                    {
                        if(count($_listData['notmoderated']) > 0)
                        {
                            $_listData['moderation'] = 'TRUE';
                        }
                        else{
                            $_listData['moderation'] = 'FALSE';
                        }
                        $_listData['archivePrivate'] = 'mailListRestriction';
                    }
                    else
                    {
                        unset($_listData['archivePrivate']);
                        unset($_listData['moderation']);
                        $_listData['moderation'] = '';
                        $_listData['archivePrivate'] = array();
                    }

                    $mlist = array_merge($_listData, $this->_mandatoryAttributes);
                    $nlist = $this->_getExtraLDAPAttributes($mlist);

                    $mailList = new Admin_Model_MailList($nlist);

                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Updated MailList entry model is mapped." . print_r($mailList, true));

                    $mailListFormated = $this->_MailList2ldap($mailList);

                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Ldap Update Mail List is : " . print_r($mailListFormated, true));

                    $metaData = $this->_getMetaData($mailListFormated['uid']);

                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Ldap metadata from list to update is : " . print_r($metaData, true));

                    try {
                        $this->_backend->save($metaData['dn'], $mailListFormated);

                        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Mail List backend LDAP succeeded on save data.");
                        //force clean up cache
                        $cache = Tinebase_Core::getCache();
                        $cache->remove($this->_getCacheId());

                        return(true);
                    } catch (Tinebase_Exception_Backend_Ldap $tebl) {
                        Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . "  Ldap Update Mail List FAILURE at : " . print_r($tebl, true));
                        throw $tebl->getMessage();
                    }
                }
            } catch (Tinebase_Exception_InvalidArgument $mldata) {
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  Ldap model fails to be created with data : " . print_r($_listData, true));
                throw ($mldata->getMessage());
            }
        } catch (Tinebase_Exception_Backend_Ldap $tebl) {
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' mail list not could not be saved on LDAP backend: ' . print_r($tebl, true));
            $_succeded = array(false, $tebl->getMessage());
            throw $tebl;
        }
    }

    /**
     * get metatada of existing mail list
     *
     * @param  string  $_mailListId
     * @return array
     */
    protected function _getMetaData($_mailListId)
    {
        $mailListId = Admin_Model_MailList::convertMailListIdToInt($_mailListId);

        $filter = Zend_Ldap_Filter::equals(
                        $this->_attributes['uid'], Zend_Ldap::filterEscape($mailListId)
        );

        $result = $this->_backend->search(
                $filter, $this->_baseDn, $this->_mailListSearchScope
        );

        if (count($result) !== 1) {
            throw new Tinebase_Exception_NotFound("mail list with id $_mailListId not found");
        }

        return $result->getFirst();
    }

    public function create(Tinebase_Record_Interface $_record)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function delete($_identifier)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function get($_id, $_getDeleted = FALSE)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function getAll($_orderBy = 'id', $_orderDirection = 'ASC')
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function getMultiple($_ids, $_containerIds = NULL)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function getType()
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function update(Tinebase_Record_Interface $_record)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }

    public function updateMultiple($_ids, $_data)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::WARN)) Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . 'This method is defined but it is empty!');
    }
}
