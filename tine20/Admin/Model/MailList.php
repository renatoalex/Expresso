<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailList
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 */

/**
 * defines the datatype for the group object
 *
 * @package     Tinebase
 * @subpackage  MailList
 * @property    string  uid
 * @property    string  description
 * @property    string  mailaddress
 * @property    array   mangers
 * @property    string  visibility
 */
class Admin_Model_MailList extends Tinebase_Record_Abstract
{
    /**
     * key in $_validators/$_properties array for the filed which
     * represents the identifier
     *
     * @var string
     */
    protected $_identifier = 'uid';

    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Tinebase';

    /**
     * list of zend inputfilter
     *
     * this filter get used when validating user generated content with Zend_Input_Filter
     *
     * @var array
     */
    protected $_filters = array(
        'mail' => 'StringTrim'
    );

    /**
     * list of zend validator
     *
     * this validators get used when validating user generated content with Zend_Input_Filter
     *
     * @var array
     */
    protected $_validators = array(
        'uid'                   => array('presence' => 'required', 'allowEmpty' => true),
        'uidnumber'             => array('presence' => 'required', 'allowEmpty' => true),
        'name'                  => array('presence' => 'required', 'allowEmpty' => false),
        'sname'                 => array('presence' => 'required', 'allowEmpty' => false),
        'description'           => array('presence' => 'required', 'allowEmpty' => true),
        'mail'                  => array('presence' => 'required', 'allowEmpty' => false),
        'admins'                => array('allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => array()),
        'moderation'            => array('presence' => 'required', 'allowEmpty' => true),
        'notmoderated'          => array('allowEmpty' => true),
        'members'               => array('presence' => 'required', 'allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => array()),
        'externalmembers'       => array('allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => array()),
        'objectclass'           => array('allowEmpty' => true),
        'result'                => array('allowEmpty' => true),
        'clienttype'            => array('allowEmpty' => true),
        'homeDirectory'         => array('presence' => 'required', 'allowEmpty' => false),
        'loginShell'            => array('presence' => 'required', 'allowEmpty' => false),
        'deliveryMode'          => array('presence' => 'required', 'allowEmpty' => false),
        'phpgwAccountType'      => array('allowEmpty' => true),
        'phpgwAccountStatus'    => array('allowEmpty' => true),
        'phpgwAccountExpires'   => array('allowEmpty' => true),
        'givenName'             => array('presence' => 'required', 'allowEmpty' => false),
        'gidNumber'             => array('presence' => 'required', 'allowEmpty' => false),
        'archivePrivate'        => array('allowEmpty' => true),
        'accountStatus'         => array('presence' => 'required', 'allowEmpty' => false),
    );

    /**
     * converts a int, string or Tinebase_Model_Group to a groupid
     *
     * @param   int|string|Tinebase_Model_Group $_groupId the groupid to convert
     * @return  string
     * @throws  Tinebase_Exception_InvalidArgument
     *
     * @todo rename this function because we now have string ids
     */
    static public function convertMailListIdToInt($_mailListId)
    {
        return self::convertId($_mailListId, 'Admin_Model_MailList');
    }

}
