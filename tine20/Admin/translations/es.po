#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: ExpressoBr - Admin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-09 17:09-0300\n"
"PO-Revision-Date: 2015-08-24 14:59-0300\n"
"Last-Translator: Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>\n"
"Language-Team: ExpressoBr Translators\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: js\n"

#: js/AccessLog.js:100 js/Admin.js:87
msgid "Access Log"
msgstr "Bitácora de acceso"

#: js/TagEditDialog.js:188
msgid "Account Rights"
msgstr "Derechos de Cuentas"

#: js/Groups.js:104
msgid "Add Group"
msgstr "Agregar Grupo"

#: js/RoleEditDialog.js:371
msgid "Add New Role"
msgstr "Agregar Nuevo Cargo"

#: js/TagEditDialog.js:300
msgid "Add New Tag"
msgstr "Añadir etiqueta nueva"

#: js/Roles.js:102
msgid "Add Role"
msgstr "Agregar Rol"

#: js/Tags.js:97
msgid "Add Tag"
msgstr "Agregar Etiqueta"

#: js/GroupEditDialog.js:227 js/GroupEditDialog.js:238
msgid "Add new group"
msgstr "Agregar Nuevo Grupo"

#: js/Admin.js:173
msgid "Admin"
msgstr "Admin"

#: js/TagEditDialog.js:169
msgid "Allowed Contexts"
msgstr "Contextos Permitidos"

#: js/ApplicationFilter.js:33
msgid "Application"
msgstr "Aplicación"

#: js/Admin.js:74
msgid "Applications"
msgstr "Aplicaciones"

#: js/AccessLog.js:105 js/AccessLog.js:133
msgid "Client Type"
msgstr "Tipo de Cliente"

#: js/TagEditDialog.js:232 js/Tags.js:222
msgid "Color"
msgstr "Color"

#: js/SambaMachineEditDialog.js:42 js/SambaMachineGrid.js:56
msgid "Computer Name"
msgstr "Nombre del Computador"

#: js/Admin.js:61
msgid "Computers"
msgstr "Computadores"

#: js/Groups.js:52 js/Roles.js:47 js/Tags.js:64
msgid "Confirm"
msgstr "Confirmar"

#: js/Admin.js:113
msgid "Containers"
msgstr "Contenedores"

#: js/TagEditDialog.js:175
msgid "Context"
msgstr "Contexto"

#: js/GroupEditDialog.js:73
msgid "Could not save group."
msgstr "No se pudo guardar el grupo."

#: js/RoleEditDialog.js:108
msgid "Could not save role."
msgstr "No se pudo guardar cargo."

#: js/TagEditDialog.js:83
msgid "Could not save tag."
msgstr "No se pudo guardar la etiqueta."

#: js/Admin.js:125
msgid "Customfields"
msgstr "Campos personalizados"

#: js/AdminPanel.js:46
msgid "Default Addressbook for new contacts and groups"
msgstr "Libreta de direcciones por defecto para contactos y grupos"

#: js/Groups.js:120
msgid "Delete Group"
msgstr "Borrar Grupo"

#: js/Roles.js:118
msgid "Delete Role"
msgstr "Borrar Rol"

#: js/Tags.js:113
msgid "Delete Tag"
msgstr "Borrar Etiqueta"

#: js/Groups.js:69
msgid "Deleting group(s)..."
msgstr "Borrando grupo(s)..."

#: js/Roles.js:64
msgid "Deleting role(s)..."
msgstr "Borrando cargo(s)..."

#: js/RoleEditDialog.js:128
msgid "Deleting role..."
msgstr "Borrando cargo..."

#: js/Tags.js:82
msgid "Deleting tag(s)..."
msgstr "Eliminando etiqueta(s)..."

#: js/GroupEditDialog.js:129 js/Groups.js:236 js/RoleEditDialog.js:294
#: js/Roles.js:239 js/SambaMachineGrid.js:89 js/TagEditDialog.js:226
#: js/Tags.js:226
msgid "Description"
msgstr "Descripción"

#: js/Applications.js:92
msgid "Disable Application"
msgstr "Deshabilitar Aplicación"

#: js/GroupEditDialog.js:143
msgid "Display in addressbook"
msgstr "Mostrar en libreta de direcciones"

#: js/Applications.js:271
msgid "Displaying application {0} - {1} of {2}"
msgstr "Mostrando aplicación {0} - {1} of {2}"

#: js/Groups.js:223
msgid "Displaying groups {0} - {1} of {2}"
msgstr "Mostrando grupos {0} - {1} of {2}"

#: js/Roles.js:226
msgid "Displaying roles {0} - {1} of {2}"
msgstr "Mostrando cargos {0} - {1} of {2}"

#: js/Tags.js:211
msgid "Displaying tags {0} - {1} of {2}"
msgstr "Mostrando etiquetas {0} - {1} de {2}"

#: js/Groups.js:52
msgid "Do you really want to delete the selected groups?"
msgstr "¿Realmente desea borrar los grupos seleccionados?"

#: js/Roles.js:47
msgid "Do you really want to delete the selected roles?"
msgstr "¿Realmente desea borrar los cargos seleccionados?"

#: js/Tags.js:64
msgid "Do you really want to delete the selected tags?"
msgstr "¿Realmente quiere eliminar las etiquetas seleccionadas?"

#: js/Groups.js:112
msgid "Edit Group"
msgstr "Editar Grupo"

#: js/GroupEditDialog.js:240
msgid "Edit Group \"{0}\""
msgstr "Editar Grupo \"{0}\""

#: js/Roles.js:110
msgid "Edit Role"
msgstr "Editar Rol"

#: js/RoleEditDialog.js:373
msgid "Edit Role \"{0}\""
msgstr "Editar Cargo \"{0}\""

#: js/Tags.js:105
msgid "Edit Tag"
msgstr "Editar Etiqueta"

#: js/TagEditDialog.js:302
msgid "Edit Tag \"{0}\""
msgstr "Editar etiqueta «{0}»"

#: js/Applications.js:85
msgid "Enable Application"
msgstr "Habilitar Aplicación"

#: js/GroupEditDialog.js:78 js/RoleEditDialog.js:115 js/TagEditDialog.js:88
msgid "Errors"
msgstr "Errores"

#: js/GroupEditDialog.js:73 js/Groups.js:74 js/RoleEditDialog.js:108
#: js/RoleEditDialog.js:136 js/Roles.js:69 js/TagEditDialog.js:83
msgid "Failed"
msgstr "Falló"

#: js/GroupEditDialog.js:222
msgid "Group"
msgstr "Grupo"

#: js/GroupEditDialog.js:170
msgid "Group Members"
msgstr "Miembros del Grupo"

#: js/GroupEditDialog.js:121
msgid "Group Name"
msgstr "Nombre del Grupo"

#: js/Admin.js:36
msgid "Groups"
msgstr "Grupos"

#: js/GroupEditDialog.js:143
msgid "Hide from addressbook"
msgstr "Esconder de la libreta de direcciones"

#: js/Groups.js:234 js/Roles.js:237 js/SambaMachineGrid.js:76
msgid "ID"
msgstr "ID"

#: js/AccessLog.js:101 js/AccessLog.js:129
msgid "IP Address"
msgstr "Dirección IP"

#: js/AccessLog.js:127
msgid "Login Name"
msgstr "Nombre de Usuario"

#: js/AccessLog.js:103 js/AccessLog.js:130
msgid "Login Time"
msgstr "Hora de Ingreso"

#: js/AccessLog.js:104 js/AccessLog.js:131
msgid "Logout Time"
msgstr "Hora de Salida"

#: js/RoleEditDialog.js:262
msgid "Members"
msgstr "Miembros"

#: js/AccessLog.js:128 js/Applications.js:282 js/Groups.js:235 js/Roles.js:238
#: js/SambaMachineGrid.js:83 js/Tags.js:225
msgid "Name"
msgstr "Nombre"

#: js/Applications.js:272
msgid "No applications to display"
msgstr "No hay aplicaciones para mostrar"

#: js/Groups.js:224 js/Groups.js:278
msgid "No groups to display"
msgstr "No hay grupos para mostrar"

#: js/Roles.js:227 js/Roles.js:281
msgid "No roles to display"
msgstr "Sin cargos para mostrar"

#: js/Tags.js:212 js/Tags.js:268
msgid "No tags to display"
msgstr "No hay etiquetas que mostrar"

#: js/Applications.js:281
msgid "Order"
msgstr "Ordenar"

#: js/GroupEditDialog.js:78 js/RoleEditDialog.js:115 js/TagEditDialog.js:88
msgid "Please fix the errors noted."
msgstr "Por favor corrija los errores indicados."

#: js/GroupEditDialog.js:38 js/RoleEditDialog.js:70 js/TagEditDialog.js:38
msgid "Please wait"
msgstr "Por favor espere"

#: js/Admin.js:257
msgid "Refresh"
msgstr "Refrescar"

#: js/AccessLog.js:132
msgid "Result"
msgstr "Resultado"

#: js/RoleEditDialog.js:160
msgid "Rights"
msgstr "Derechos"

#: js/RoleEditDialog.js:361
msgid "Role"
msgstr "Rol"

#: js/RoleEditDialog.js:286
msgid "Role Name"
msgstr "Nombre del Cargo"

#: js/Admin.js:49
msgid "Roles"
msgstr "Cargos"

#: js/GroupEditDialog.js:159
msgid "Saved in Addressbook"
msgstr "Guardado en libreta de direcciones"

#: js/Applications.js:187 js/Groups.js:176 js/Roles.js:176 js/Tags.js:169
msgid "Search:"
msgstr "Buscar:"

#: js/Admin.js:137
msgid "Server Information"
msgstr "Información del servidor"

#: js/AccessLog.js:126
msgid "Session ID"
msgstr "ID de Sesión"

#: js/Applications.js:99
msgid "Settings"
msgstr "Seteos"

#: js/Admin.js:100
msgid "Shared Tags"
msgstr "Etiquetas Compartidas"

#: js/Groups.js:74
msgid "Some error occurred while trying to delete the group."
msgstr "Ocurrió un error al intentar borrar este grupo."

#: js/RoleEditDialog.js:136 js/Roles.js:69
msgid "Some error occurred while trying to delete the role."
msgstr "Ocurrió un error al intentar borrar el cargo."

#: js/Applications.js:283
msgid "Status"
msgstr "Estado"

#: js/TagEditDialog.js:290
msgid "Tag"
msgstr "Etiqueta"

#: js/TagEditDialog.js:219
msgid "Tag Name"
msgstr "Nombre de la etiqueta"

#: js/GroupEditDialog.js:222 js/RoleEditDialog.js:361 js/TagEditDialog.js:290
msgid "Transferring {0}..."
msgstr "Transfiriendo {0}..."

#: js/GroupEditDialog.js:38 js/RoleEditDialog.js:70
msgid "Updating Memberships"
msgstr "Actualizando Cuentas de Usuario"

#: js/TagEditDialog.js:38
msgid "Updating Tag"
msgstr "Actualizando Etiqueta"

#: js/Tags.js:223
msgid "Usage:&#160;"
msgstr "Uso:&#160;"

#: js/TagEditDialog.js:202
msgid "Use"
msgstr "Usar"

#: js/AccessLog.js:102 js/Admin.js:23
msgid "User"
msgid_plural "Users"
msgstr[0] "Usuario"
msgstr[1] "Usuarios"

#: js/Applications.js:284
msgid "Version"
msgstr "Versión"

#: js/TagEditDialog.js:197
msgid "View"
msgstr "Ver"

#: js/GroupEditDialog.js:137
msgid "Visibility"
msgstr "Visibilidad"

#: js/AccessLog.js:169
msgid "ambiguous username"
msgstr "nombre de usuario ambiguo"

#: js/Applications.js:149
msgid "disable application"
msgstr "desactivar aplicación"

#: js/Applications.js:230
msgid "disabled"
msgstr "deshabilitado"

#: js/Applications.js:148
msgid "enable application"
msgstr "activar aplicación"

#: js/Applications.js:233
msgid "enabled"
msgstr "habilitado"

#: js/AccessLog.js:177
msgid "failure"
msgstr "Error"

#: js/AccessLog.js:165
msgid "invalid password"
msgstr "Contraseña incorrecta"

#: js/AccessLog.js:157
msgid "password expired"
msgstr "clave expirada"

#: js/Applications.js:150
msgid "settings"
msgstr "opciones"

#: js/AccessLog.js:181
msgid "success"
msgstr "Exitoso"

#: js/Applications.js:237
msgid "unknown status ({0})"
msgstr "Estado desconocido ({0})"

#: js/AccessLog.js:153
msgid "user blocked"
msgstr "usuario bloqueado"

#: js/AccessLog.js:161
msgid "user disabled"
msgstr "usuario deshabilitado"

#: js/AccessLog.js:173
msgid "user not found"
msgstr "usuario no encontrado"

#: js/Applications.js:45
msgid "{0} Settings"
msgstr "{0} Configuraciones"

#: js/container/EditDialog.js:269 js/user/EditDialog.js:775
msgid "Account"
msgstr "Cuenta"

#: js/customfield/EditDialog.js:288
msgid "Add a New ID..."
msgstr "Agregar un Nuevo ID..."

#: js/customfield/EditDialog.js:297
msgid "Add a New Value..."
msgstr "Agregar un Nuevo Valor..."

#: js/user/EditDialog.js:665
msgid "Add a forward address..."
msgstr "Agregar una dirección de correo de reenvio..."

#: js/user/EditDialog.js:631
msgid "Add an alias address..."
msgstr "Agregar un pseudónimo de dirección..."

#: js/container/EditDialog.js:204
msgid "Backend"
msgstr "Backend"

#: js/user/EditDialog.js:887
msgid ""
"Blocked status is only valid if the user tried to login with a wrong "
"password to often. It is not possible to set this status here."
msgstr ""
"El estado de usuario bloqueado es válido sólo cuando el usuario intentó "
"ingresar al sistema con una contraseña incorrecta varias veces. No es "
"posible setearlo acá"

#: js/customfield/EditDialog.js:577
msgid "Boolean"
msgstr "Buleano"

#: js/customfield/EditDialog.js:471 js/user/EditDialog.js:733
msgid "Cancel"
msgstr "Cancelar"

#: js/customfield/EditDialog.js:504
msgid "Configure store"
msgstr "Configure respaldo"

#: js/container/GridPanel.js:98
msgid "Container"
msgstr "Contenedor"

#: js/container/GridPanel.js:66
msgid "Container Name"
msgstr "Nombre del contenedor"

#: js/user/EditDialog.js:496
msgid "Current Mailbox size"
msgstr "Tamaño actual de la casilla"

#: js/user/EditDialog.js:534
msgid "Current Sieve size"
msgstr "Tamaño actual de Sieve"

#: js/customfield/EditDialog.js:620
msgid "Custom field additional properties"
msgstr "Propiedades adicionales de campo personalizado"

#: js/customfield/EditDialog.js:561
msgid "Custom field definition"
msgstr "Definición de campo personalizado"

#: js/customfield/GridPanel.js:110
msgid "Customfield"
msgstr "Campo personalizado"

#: js/customfield/EditDialog.js:653
msgid "Customfield already exists. Please choose another name."
msgstr "Campo personalizado existente. Por favor elija otro nombre."

#: js/customfield/EditDialog.js:574
msgid "Date"
msgstr "Fecha"

#: js/customfield/EditDialog.js:575
msgid "DateTime"
msgstr "FechaHora"

#: js/customfield/EditDialog.js:244
msgid "Default"
msgstr "Default"

#: js/user/GridPanel.js:60
msgid "Disable Account"
msgstr "Deshabilitar Cuenta"

#: js/user/GridPanel.js:173
msgid "Display name"
msgstr "Nombre a mostrar"

#: js/container/EditDialog.js:255
msgid "Distinguished Name"
msgstr "Nombre Completo"

#: js/user/EditDialog.js:187
msgid "Domain is not allowed. Check your SMTP domain configuration."
msgstr ""
"El dominio no está permitido. Compruebe su configuración de dominios SMTP."

#: js/user/EditDialog.js:615
msgid "Domain not allowed"
msgstr "Dominio no permitido"

#: js/user/EditDialog.js:840 js/user/GridPanel.js:177
msgid "Email"
msgstr "Email"

#: js/user/EditDialog.js:625
msgid "Email Alias"
msgstr "Pseudónimo de Correo"

#: js/user/EditDialog.js:659
msgid "Email Forward"
msgstr "Reenvío de Correo"

#: js/user/GridPanel.js:50
msgid "Enable Account"
msgstr "Habilitar Cuenta"

#: js/customfield/EditDialog.js:307 js/customfield/EditDialog.js:313
msgid "Error"
msgstr "Error"

#: js/user/EditDialog.js:909 js/user/GridPanel.js:182
msgid "Expires"
msgstr "Expira"

#: js/user/EditDialog.js:988
msgid "Fileserver"
msgstr "Servidor de Archivos"

#: js/user/EditDialog.js:790 js/user/GridPanel.js:176
msgid "First name"
msgstr "Nombres"

#: js/user/EditDialog.js:678
msgid "Forward Only"
msgstr "Sólo Reenviar"

#: js/user/EditDialog.js:649
msgid "Forwarding to self"
msgstr "Reenvío a sí mismo"

#: js/customfield/EditDialog.js:432
msgid "Given record class not found"
msgstr "Record class ingresado no encontrado"

#: js/user/EditDialog.js:377
msgid "Home Directory"
msgstr "Carpeta personal"

#: js/user/EditDialog.js:401
msgid "Home Drive"
msgstr "Unidad Home"

#: js/user/EditDialog.js:411
msgid "Home Path"
msgstr "Ubicación Home"

#: js/container/EditDialog.js:229
msgid "Host"
msgstr "Servidor"

#: js/customfield/EditDialog.js:307
msgid "ID already exists"
msgstr "Ya existe el ID"

#: js/user/EditDialog.js:994
msgid "IMAP"
msgstr "IMAP"

#: js/user/EditDialog.js:466
msgid "IMAP Quota (MB)"
msgstr "Cuota (MB) IMAP"

#: js/user/EditDialog.js:543 js/user/EditDialog.js:946
msgid "Information"
msgstr "Información"

#: js/user/EditDialog.js:886
msgid "Invalid Status"
msgstr "Estado inválido"

#: js/customfield/EditDialog.js:579
msgid "Key Field"
msgstr "Campo clave"

#: js/user/EditDialog.js:446
msgid "Kick Off Time"
msgstr "Tiempo de Expulsión"

#: js/customfield/EditDialog.js:602 js/customfield/GridPanel.js:66
msgid "Label"
msgstr "Etiqueta"

#: js/user/EditDialog.js:557
msgid "Last Login"
msgstr "Último Ingreso"

#: js/user/EditDialog.js:961 js/user/GridPanel.js:179
msgid "Last login at"
msgstr "Último ingreso el"

#: js/user/EditDialog.js:965 js/user/GridPanel.js:180
msgid "Last login from"
msgstr "Último ingreso desde"

#: js/user/EditDialog.js:800 js/user/GridPanel.js:175
msgid "Last name"
msgstr "Apellidos"

#: js/container/EditDialog.js:206
msgid "Ldap"
msgstr "Ldap"

#: js/customfield/EditDialog.js:608
msgid "Length"
msgstr "Ancho"

#: js/user/EditDialog.js:381
msgid "Login Shell"
msgstr "Intérprete (shell) de acceso"

#: js/user/EditDialog.js:805 js/user/EditDialog.js:847
#: js/user/GridPanel.js:174
msgid "Login name"
msgstr "Nombre de Login"

#: js/user/EditDialog.js:416
msgid "Logoff Time"
msgstr "Hora de Salida"

#: js/user/EditDialog.js:431
msgid "Logon Script"
msgstr "Script de Inicio"

#: js/user/EditDialog.js:406
msgid "Logon Time"
msgstr "Tiempo de Ingreso"

#: js/container/EditDialog.js:292
msgid "Max Result"
msgstr "Resultado máximo"

#: js/container/EditDialog.js:157 js/customfield/EditDialog.js:550
#: js/customfield/GridPanel.js:70
msgid "Model"
msgstr "Modelo"

#: js/container/EditDialog.js:183
msgid "Note for Owner"
msgstr "Nota para el dueño"

#: js/customfield/EditDialog.js:573
msgid "Number"
msgstr "Número"

#: js/customfield/EditDialog.js:465
msgid "OK"
msgstr "OK"

#: js/user/EditDialog.js:740
msgid "Ok"
msgstr "Aceptar"

#: js/user/EditDialog.js:846 js/user/GridPanel.js:178
msgid "OpenID"
msgstr "OpenID"

#: js/container/EditDialog.js:277 js/user/EditDialog.js:810
msgid "Password"
msgstr "Contraseña"

#: js/user/EditDialog.js:436
msgid "Password Can Change"
msgstr "La Contraseña Puede Cambiar"

#: js/user/EditDialog.js:426
msgid "Password Last Set"
msgstr "Última Contraseña Guardada"

#: js/user/EditDialog.js:441
msgid "Password Must Change"
msgstr "La Contraseña Debe Cambiar"

#: js/user/GridPanel.js:181
msgid "Password changed"
msgstr "Contraseña cambiada"

#: js/user/EditDialog.js:697
msgid "Password confirmation"
msgstr "Confirmación de Contraseña"

#: js/user/EditDialog.js:969
msgid "Password set"
msgstr "Contraseña guardada"

#: js/user/EditDialog.js:234 js/user/EditDialog.js:237
#: js/user/EditDialog.js:730
msgid "Passwords do not match!"
msgstr "¡Las contraseñas no coinciden!"

#: js/user/EditDialog.js:240
msgid "Passwords match!"
msgstr "¡Las contraseñas coinciden!"

#: js/customfield/EditDialog.js:137
msgid "Please configure store for this field type"
msgstr "Por favor configure el almacenamiento para este campo"

#: js/user/GridPanel.js:240
msgid "Please enter the new password:"
msgstr "Porfavor escriba la nueva contraseña:"

#: js/container/EditDialog.js:236
msgid "Port"
msgstr "Puerto"

#: js/user/EditDialog.js:286 js/user/EditDialog.js:852
msgid "Primary group"
msgstr "Grupo primario"

#: js/user/EditDialog.js:421
msgid "Profile Path"
msgstr "Ubicación del Perfil"

#: js/container/EditDialog.js:284
msgid "Quick Search"
msgstr "Búsqueda rápida:"

#: js/user/EditDialog.js:485 js/user/EditDialog.js:523
msgid "Quota"
msgstr "Cuota"

#: js/customfield/EditDialog.js:580
msgid "Record"
msgstr "Registro"

#: js/customfield/EditDialog.js:407
msgid "Record Class"
msgstr ""

#: js/container/EditDialog.js:302
msgid "Recursive"
msgstr "Recursivo"

#: js/user/EditDialog.js:714
msgid "Repeat password"
msgstr "Repetir Contraseña"

#: js/customfield/EditDialog.js:613
msgid "Required"
msgstr "Requerido"

#: js/user/GridPanel.js:70
msgid "Reset Password"
msgstr "Resetear contraseña"

#: js/customfield/GridPanel.js:128
msgid "Restart application to apply new customfields?"
msgstr "¿Relanzar aplicación para aplicar nuevos campos personalizados?"

#: js/user/EditDialog.js:1003
msgid "SMTP"
msgstr "SMTP"

#: js/customfield/EditDialog.js:578
msgid "Search Combo"
msgstr "Combo de búsqueda"

#: js/container/EditDialog.js:262
msgid "Search Filter"
msgstr "Filtro de búsqueda:"

#: js/user/GridPanel.js:240
msgid "Set new password"
msgstr "Establecer nueva contraseña"

#: js/user/EditDialog.js:504
msgid "Sieve Quota (MB)"
msgstr "Cuota (MB) Sieve"

#: js/container/EditDialog.js:206
msgid "Sql"
msgstr "Sql"

#: js/customfield/EditDialog.js:572
msgid "Text"
msgstr "Texto"

#: js/user/EditDialog.js:616
msgid ""
"The domain {0} of the alias {1} you tried to add is neither configured as "
"primary domain nor set as a secondary domain in the setup. Please add this "
"domain to the secondary domains in SMTP setup or use another domain which is "
"configured already."
msgstr ""
"El dominio {0} para el alias {1} que intentó agregar no está configurado "
"como dominio principal o secundario en el setup. Por favor agregue este "
"dominio como dominio secundario en el setup de SMTP o use un dominio que ya "
"esté configurado."

#: js/customfield/EditDialog.js:576
msgid "Time"
msgstr "Hora"

#: js/container/EditDialog.js:164 js/container/GridPanel.js:68
#: js/container/GridPanel.js:99 js/customfield/EditDialog.js:584
#: js/customfield/GridPanel.js:68
msgid "Type"
msgstr "Tipo"

#: js/user/EditDialog.js:363
msgid "Unix"
msgstr "Unix"

#: js/container/EditDialog.js:245
msgid "Use SSL"
msgstr "Usar SSL"

#: js/user/EditDialog.js:976
msgid "User groups"
msgstr "Grupos de usuarios"

#: js/user/EditDialog.js:982
msgid "User roles"
msgstr "Roles de usuarios"

#: js/customfield/EditDialog.js:292
msgid "Value"
msgstr "Valor"

#: js/customfield/EditDialog.js:313
msgid "Value already exists"
msgstr "Ese valor ya existe"

#: js/user/EditDialog.js:387
msgid "Windows"
msgstr "Windows"

#: js/user/EditDialog.js:650
msgid ""
"You are not allowed to set a forward email address that is identical to the "
"users primary email or one of his aliases."
msgstr ""
"No es posible definir una dirección de reenvío a una dirección idéntica al "
"email primario del usuario o a un alias."

#: js/user/EditDialog.js:879
msgid "blocked"
msgstr "bloqueado"

#: js/user/EditDialog.js:963 js/user/EditDialog.js:967
msgid "don't know"
msgstr "desconocido"

#: js/user/EditDialog.js:878
msgid "expired"
msgstr "expirado"

#: js/container/EditDialog.js:247 js/container/EditDialog.js:287
#: js/container/EditDialog.js:304
msgid "false"
msgstr "falso"

#: js/user/EditDialog.js:428 js/user/EditDialog.js:911
#: js/user/EditDialog.js:971
msgid "never"
msgstr "nunca"

#: js/user/EditDialog.js:408 js/user/EditDialog.js:963
#: js/user/EditDialog.js:967
msgid "never logged in"
msgstr "nunca ha ingresado"

#: js/user/EditDialog.js:418
msgid "never logged off"
msgstr "nunca ha salido"

#: js/user/EditDialog.js:486 js/user/EditDialog.js:524
msgid "no quota set"
msgstr "sin seteo de cuota"

#: js/user/EditDialog.js:438 js/user/EditDialog.js:443
#: js/user/EditDialog.js:448
msgid "not set"
msgstr "sin guardar"

#: js/container/EditDialog.js:165
msgid "personal"
msgstr "personal"

#: js/container/EditDialog.js:165
msgid "shared"
msgstr "compartido"

#: js/container/EditDialog.js:247 js/container/EditDialog.js:287
#: js/container/EditDialog.js:304
msgid "true"
msgstr "verdadero"
