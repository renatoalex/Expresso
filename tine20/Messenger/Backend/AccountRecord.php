<?php
/**
 * Tine 2.0
 *
 * @package     Messenger
 * @subpackage  Backend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 */


/**
 * backend for AccountRecords
 *
 * @package     Messenger
 * @subpackage  Backend
 */
class Messenger_Backend_AccountRecord extends Tinebase_Backend_Sql_Abstract
{
    /**
     * Table name without prefix
     *
     * @var string
     */
    protected $_tableName = 'accounts';
    
    /**
     * Model name
     *
     * @var string
     */
    protected $_modelName = 'Messenger_Model_AccountRecord';
}
