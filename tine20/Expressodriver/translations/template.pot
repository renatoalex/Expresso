msgid ""
msgstr ""
"Project-Id-Version: Tine 2.0 - Expressodriver\n"
"POT-Creation-Date: 2008-05-17 22:12+0100\n"
"PO-Revision-Date: 2008-07-29 21:14+0100\n"
"Last-Translator: Cornelius Weiss <c.weiss@metaways.de>\n"
"Language-Team: Tine 2.0 Translators\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: en\n"
"X-Poedit-Country: GB\n"
"X-Poedit-SourceCharset: utf-8\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: Acl/Rights.php:97
msgid "manage shared folders"
msgstr ""

#: Acl/Rights.php:98
msgid "Create new shared folders"
msgstr ""

#: Controller.php:102
#, python-format
msgid "%s's personal files"
msgstr ""

#: js/PathFilterModel.js:50
msgid "path"
msgstr ""

#: js/NodeEditDialog.js:44 js/Expressodriver.js:44 js/NodeGridPanel.js:346
msgid "Save locally"
msgstr ""

#: js/NodeEditDialog.js:95 js/NodeEditDialog.js:108
msgid "Node"
msgstr ""

#: js/NodeEditDialog.js:114 js/NodeGridPanel.js:135
msgid "Name"
msgstr ""

#: js/NodeEditDialog.js:121 js/Model.js:530
msgid "Type"
msgstr ""

#: js/NodeEditDialog.js:129 js/NodeGridPanel.js:189
msgid "Created By"
msgstr ""

#: js/NodeEditDialog.js:132 js/Model.js:532 js/NodeGridPanel.js:181
msgid "Creation Time"
msgstr ""

#: js/NodeEditDialog.js:141
msgid "Modified By"
msgstr ""

#: js/NodeEditDialog.js:144
msgid "Last Modified"
msgstr ""

#: js/NodeEditDialog.js:167
msgid "Description"
msgstr ""

#: js/NodeEditDialog.js:181
msgid "Enter description"
msgstr ""

#: js/NodeTreePanel.js:660 js/NodeTreePanel.js:770 js/NodeGridPanel.js:653
#: js/NodeGridPanel.js:805
msgid "Upload Failed"
msgstr ""

#: js/NodeTreePanel.js:661 js/NodeGridPanel.js:654
msgid ""
"Could not upload file. Filesize could be too big. Please notify your "
"Administrator. Max upload size: "
msgstr ""

#: js/NodeTreePanel.js:771 js/NodeGridPanel.js:806
msgid "Putting files in this folder is not allowed!"
msgstr ""

#: js/GridContextMenu.js:34
msgid "Please enter the new name of the {0}:"
msgstr ""

#: js/GridContextMenu.js:49
msgid "You have to supply a different name!"
msgstr ""

#: js/GridContextMenu.js:40
msgid "Not renamed {0}"
msgstr ""

#: js/GridContextMenu.js:40 js/NodeGridPanel.js:505
msgid "You have to supply a {0} name!"
msgstr ""

#: js/GridContextMenu.js:144 js/NodeGridPanel.js:546
msgid "Do you really want to delete the following files?"
msgstr ""

#: js/Model.js:40
msgid "File"
msgid_plural "Files"
msgstr[0] ""
msgstr[1] ""

#: js/Model.js:43 js/NodeGridPanel.js:157
msgid "Folder"
msgid_plural "Folders"
msgstr[0] ""
msgstr[1] ""

#: js/Model.js:281 js/Model.js:297
msgid "Copying data .. {0}"
msgstr ""

#: js/Model.js:284 js/Model.js:299
msgid "Moving data .. {0}"
msgstr ""

#: js/Model.js:303
msgid "Please wait"
msgstr ""

#: js/Model.js:529
msgid "Quick Search"
msgstr ""

#: js/Model.js:531 js/NodeGridPanel.js:149
msgid "Contenttype"
msgstr ""

#: js/Expressodriver.js:36
msgid "Expressodriver"
msgstr ""

#: js/Expressodriver.js:91
msgid "Service Unavailable"
msgstr ""

#: js/Expressodriver.js:92
msgid ""
"The Expressodriver is not configured correctly. Please refer to the {0}Tine 2.0 "
"Admin FAQ{1} for configuration advice or contact your administrator."
msgstr ""

#: js/Expressodriver.js:114
msgid "Files already exists"
msgstr ""

#: js/Expressodriver.js:114
msgid "Do you want to replace the following file(s)?"
msgstr ""

#: js/Expressodriver.js:147
msgid "Failure on create folder"
msgstr ""

#: js/Expressodriver.js:148
msgid "Item with this name already exists!"
msgstr ""

#: js/NodeGridPanel.js:127
msgid "Tags"
msgstr ""

#: js/NodeGridPanel.js:142
msgid "Size"
msgstr ""

#: js/NodeGridPanel.js:166
msgid "Revision"
msgstr ""

#: js/NodeGridPanel.js:196
msgid "Last Modified Time"
msgstr ""

#: js/NodeGridPanel.js:203
msgid "Last Modified By"
msgstr ""

#: js/NodeGridPanel.js:251
msgid "The max. Upload Filesize is {0} MB"
msgstr ""

#: js/NodeGridPanel.js:289
msgid "Upload"
msgstr ""

#: js/NodeGridPanel.js:313
msgid "Properties"
msgstr ""

#: js/NodeGridPanel.js:324
msgid "Create Folder"
msgstr ""

#: js/NodeGridPanel.js:335
msgid "Folder Up"
msgstr ""

#: js/NodeGridPanel.js:356 js/NodeGridPanel.js:357 js/NodeGridPanel.js:359
msgid "Delete"
msgstr ""

#: js/GridPanel.js:366 js/GridPanel.js:367 js/GridPanel.js:369
msgid "Rename"
msgstr ""

#: js/NodeGridPanel.js:501
msgid "New Folder"
msgstr ""

#: js/NodeGridPanel.js:501
msgid "Please enter the name of the new folder:"
msgstr ""

#: js/NodeGridPanel.js:505
msgid "No {0} added"
msgstr ""

#: Controller/Node.php:318
msgid "My folders"
msgstr ""

#: Controller/Node.php:325
msgid "Shared folders"
msgstr ""

#: Controller/Node.php:332
msgid "Other users folders"
msgstr ""

#: Controller/Node/Filesystem.php:348
msgid "External folders"
msgstr ""

#: js/GridContextMenu:72
msgid "Renaming nodes..."
msgstr ""

#: js/GridContextMenu:175
msgid "Deleting nodes..."
msgstr ""

#: js/ExternalAdapter:156
msgid "Use e-mail as login name"
msgstr ""

#: js/CredentialsDialog:33
msgid "Credentials for Expressodriver"
msgstr ""

#: js/CredentialsDialog:85
msgid "Authentication success."
msgstr ""

#: js/CredentialsDialog:96
msgid "Credentials error"
msgstr ""

#: js/CredentialsDialog:97
msgid "Invalid Credentials"
msgstr ""
