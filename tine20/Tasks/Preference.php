<?php
/**
 * Tine 2.0
 * 
 * @package     Tasks
 * @subpackage  Preference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Goekmen Ciyiltepe <g.ciyiltepe@metaways.de>
 * @copyright   Copyright (c) 2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Tasks preferences
 *
 * @package     Tasks
 * @subpackage  Preference
 */
class Tasks_Preference extends Tinebase_Preference_Abstract
{
    /**************************** application preferences/settings *****************/
    
    /**
     * have name of default favorite on a central place
     * _("All my tasks")
     */
    const DEFAULTPERSISTENTFILTER_NAME = "All my tasks";
    
    /**
     * default task list where all new tasks are placed in
     */
    const DEFAULTTASKLIST = 'defaultTaskList';

    /**
     * general notification level
     */
    const NOTIFICATION_LEVEL = 'notificationLevel';

    /**
     * send notifications of own updates
     */
    const SEND_NOTIFICATION_TO_ORGANIZER = 'sendNotificationsForOrganizer';
    
    /**
     * @var string application
     */
    protected $_application = 'Tasks';
        
    /**************************** public functions *********************************/
    
    /**
     * get all possible application prefs
     *
     * @return  array   all application prefs
     */
    public function getAllApplicationPreferences()
    {
        $allPrefs = array(
            self::DEFAULTPERSISTENTFILTER,
            self::DEFAULTTASKLIST,
            self::NOTIFICATION_LEVEL,
            self::SEND_NOTIFICATION_TO_ORGANIZER,
        );
            
        return $allPrefs;
    }
    
    /**
     * get translated right descriptions
     * 
     * @return  array with translated descriptions for this applications preferences
     */
    public function getTranslatedPreferences()
    {
        $translate = Tinebase_Translation::getTranslation($this->_application);

        $prefDescriptions = array(
            self::DEFAULTPERSISTENTFILTER  => array(
                'label'         => $translate->_('Default Favorite'),
                'description'   => $translate->_('The default favorite which is loaded on Tasks startup'),
            ),
            self::DEFAULTTASKLIST  => array(
                'label'         => $translate->_('Default Task List'),
                'description'   => $translate->_('The default task list to create new tasks in.'),
            ),
            self::NOTIFICATION_LEVEL => array(
                'label'         => $translate->_('Get Notification Emails'),
                'description'   => $translate->_('Receive email notification of task that was assigned to you'),
            ),
            self::SEND_NOTIFICATION_TO_ORGANIZER => array(
                'label'         => $translate->_('Send Notifications for assigned organizer'),
                'description'   => $translate->_('Define the option to send e-mail notifications to assigned organizer'),
            ),

        );
        
        return $prefDescriptions;
    }
    
    /**
     * get preference defaults if no default is found in the database
     *
     * @param string $_preferenceName
     * @param string|Tinebase_Model_User $_accountId
     * @param string $_accountType
     * @return Tinebase_Model_Preference
     */
    public function getApplicationPreferenceDefaults($_preferenceName, $_accountId = NULL, $_accountType = Tinebase_Acl_Rights::ACCOUNT_TYPE_USER)
    {
        $preference = $this->_getDefaultBasePreference($_preferenceName);
        
        switch($_preferenceName) {
            case self::DEFAULTPERSISTENTFILTER:
                $preference->value          = Tinebase_PersistentFilter::getPreferenceValues('Tasks', $_accountId, self::DEFAULTPERSISTENTFILTER_NAME);
                break;
            case self::DEFAULTTASKLIST:
                $this->_getDefaultContainerPreferenceDefaults($preference, $_accountId);
                break;
            case self::SEND_NOTIFICATION_TO_ORGANIZER:
                $translate = Tinebase_Translation::getTranslation($this->_application);
                // need to put the translations strings here because they were not found in the xml below :/
                // _('Never') _('On invitation and cancellation only') _('On time changes') _('On all updates but attendee responses') _('On attendee responses too')
                $preference->value      = Tasks_Controller_TaskNotifications::NOTIFICATION_LEVEL_NONE;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <option>
                            <value>'. Tasks_Controller_TaskNotifications::NOTIFICATION_LEVEL_NONE . '</value>
                            <label>'. $translate->_('Never') . '</label>
                        </option>
                        <option>
                            <value>'. Tasks_Controller_TaskNotifications::NOTIFICATION_LEVEL_ASK . '</value>
                            <label>'. $translate->_('Ask me before send notification') . '</label>
                        </option>
                        <option>
                            <value>'. Tasks_Controller_TaskNotifications::NOTIFICATION_LEVEL_AUTOMATIC . '</value>
                            <label>'. $translate->_('Send notification automatically') . '</label>
                        </option>
                    </options>';
                break;
            case self::NOTIFICATION_LEVEL:
                $preference->value      = 0;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <special>' . Tinebase_Preference_Abstract::YES_NO_OPTIONS . '</special>
                    </options>';
                break;
            default:
                throw new Tinebase_Exception_NotFound('Default preference with name ' . $_preferenceName . ' not found.');
        }
        
        return $preference;
    }
}
