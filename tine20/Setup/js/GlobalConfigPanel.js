/*
 * Tine 2.0
 *
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/*global Ext, Tine*/

Ext.ns('Tine', 'Tine.Setup');

/**
 * Setup Configuration Manager
 *
 * @namespace   Tine.Setup
 * @class       Tine.Setup.GlobalConfigPanel
 * @extends     Tine.Tinebase.widgets.form.ConfigPanel
 *
 * <p>Configuration Panel</p>
 * <p><pre>
 * </pre></p>
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Jeferson Jose de Miranda <jeferson.miranda@serpro.gov.br>
 *
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Setup.GlobalConfigPanel
 */
Tine.Setup.GlobalConfigPanel = Ext.extend(Tine.Tinebase.widgets.form.ConfigPanel, {

    /**
     * @property idPrefix DOM Id prefix
     * @type String
     */
    idPrefix: null,

    /**
     * @private
     * panel cfg
     */
    saveMethod: 'Setup.saveConfig',
    registryKey: 'configData',
    defaults: {
        xtype: 'fieldset',
        autoHeight: 'auto',
        defaults: {width: 300},
        defaultType: 'textfield'
    },

    /**
     * session backend DOM Id prefix
     *
     * @property sessionBackendIdPrefix
     * @type String
     */
    sessionBackendIdPrefix: null,

    /**
     * @private
     * field index counter
     */
    tabIndexCounter: 1,

    /**
     * @private
     */
    initComponent: function () {
        this.idPrefix                  = Ext.id();
        this.sessionBackendIdPrefix    = this.idPrefix + '-sessionBackend-';
        this.cacheBackendIdPrefix    = this.idPrefix + '-cacheBackend-';
        this.queueBackendIdPrefix    = this.idPrefix + '-queueBackend-';
        this.keyEscrowPrefix = this.idPrefix + '-keyEscrow-';


        Tine.Setup.GlobalConfigPanel.superclass.initComponent.call(this);

        this.action_saveConfig.setDisabled(false);
    },

    /**
     * Change session card layout depending on selected combo box entry
     */
    onChangeSessionBackend: function () {
        this.changeCard(this.sessionBackendCombo, this.sessionBackendIdPrefix);
    },

    /**
     * Change cache card layout depending on selected combo box entry
     */
    onChangeCacheBackend: function () {
        this.changeCard(this.cacheBackendCombo, this.cacheBackendIdPrefix);
    },

    /**
     * @private
     */
    onRender: function (ct, position) {
        Tine.Setup.GlobalConfigPanel.superclass.onRender.call(this, ct, position);

        this.onChangeSessionBackend.defer(250, this);
    },

    /**
     * get tab index for field
     *
     * @return {Integer}
     */
    getTabIndex: function () {
        return this.tabIndexCounter++;
    },

    /**
     * returns config manager form
     *
     * @private
     * @return {Array} items
     */
    getFormItems: function () {
        // common config for all combos in this setup
        var commonComboConfig = {
            xtype: 'combo',
            listWidth: 300,
            mode: 'local',
            forceSelection: true,
            allowEmpty: false,
            triggerAction: 'all',
            editable: false,
            tabIndex: this.getTabIndex
        };

        this.sessionBackendCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'session_backend',
            fieldLabel: this.app.i18n._('Backend'),
            value: 'File',
            store: [['File', this.app.i18n._('File')], ['Redis','Redis']],
            listeners: {
                scope: this,
                change: this.onChangeSessionBackend,
                select: this.onChangeSessionBackend
            }
        }, commonComboConfig));

        this.cacheBackendCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'caching_backend',
            fieldLabel: this.app.i18n._('Backend'),
            value: 'File',
            store: [['File', this.app.i18n._('File')], ['Redis','Redis'], ['Memcached', 'Memcached']],
            listeners: {
                scope: this,
                change: this.onChangeCacheBackend,
                select: this.onChangeCacheBackend
            }
        }, commonComboConfig));

        var formItens = [{
            title: this.app.i18n._('Setup Authentication'),
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'setupuser_username',
                fieldLabel: this.app.i18n._('Username'),
                allowBlank: false,
                listeners: {
                    afterrender: function (field) {
                        field.focus(true, 500);
                    }
                }
            }, {
                name: 'setupuser_password',
                fieldLabel: this.app.i18n._('Password'),
                inputType: 'password',
                allowBlank: false
            }]
        }];

        if (Tine.Tinebase.registry.get('multidomain')) {
            var domainData = Tine.Setup.registry.get('domainData');
            var domainStore = [];
            Ext.each(domainData['domains'], function(name) {
                domainStore.push([name, name]);
            });
            formItens = formItens.concat({
            title: this.app.i18n._('Domain'),
                defaults: {
                    width: 300,
                    tabIndex: this.getTabIndex
                },
                items: [Ext.applyIf({
                    name: 'domaindata_domain',
                    id: this.idPrefix+'domaindata-domain',
                    fieldLabel: this.app.i18n._('Default domain'),
                    store: domainStore,
                    value: domainData['activeDomain'],
                    listeners: {
                        scope: this,
                        change: this.onChangeDomain,
                        select: this.onChangeDomain
                    }
                }, commonComboConfig)]
            });
        }

        formItens = formItens.concat({
            title: this.app.i18n._('Logging'),
            id: this.idPrefix+'setup-logger-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'logger_filename',
                fieldLabel: this.app.i18n._('Filename')
            }, Ext.applyIf({
                name: 'logger_priority',
                fieldLabel: this.app.i18n._('Priority'),
                store: [[0, 'Emergency'], [1, 'Alert'], [2, 'Critical'], [3, 'Error'], [4, 'Warning'], [5, 'Notice'], [6, 'Informational'], [7, 'Debug'], [8, 'Trace']]
            }, commonComboConfig)]
        }, {
            title: this.app.i18n._('Caching'),
            id: this.idPrefix+'setup-caching-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [ {
                xtype: 'numberfield',
                name: 'caching_lifetime',
                fieldLabel: this.app.i18n._('Lifetime (seconds)'),
                minValue: 0,
                maxValue: 43200
            }, this.cacheBackendCombo,
            {
                id: this.cacheBackendIdPrefix + 'CardLayout',
                xtype: 'panel',
                layout: 'card',
                activeItem: this.cacheBackendIdPrefix + 'File',
                border: false,
                width: '100%',
                defaults: {border: false},
                items: [{
                    id: this.cacheBackendIdPrefix + 'File',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'caching_path',
                        fieldLabel: this.app.i18n._('Path')
                    }]
                }, {
                    id: this.cacheBackendIdPrefix + 'Redis',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'caching_redis_host',
                        fieldLabel: this.app.i18n._('Hostname'),
                        value: 'localhost'
                    }, {
                        name: 'caching_redis_port',
                        fieldLabel: this.app.i18n._('Port'),
                        xtype: 'numberfield',
                        minValue: 0,
                        value: 6379
                    }]
                }, {
                    id: this.cacheBackendIdPrefix + 'Memcached',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'caching_memcached_host',
                        fieldLabel: this.app.i18n._('Hostname'),
                        value: 'localhost'
                    }, {
                        name: 'caching_memcached_port',
                        fieldLabel: this.app.i18n._('Port'),
                        xtype: 'numberfield',
                        minValue: 0,
                        value: 11211
                    }]
                }]
            }, Ext.applyIf({
                name: 'caching_customexpirable',
                fieldLabel: this.app.i18n._('Custom expirable'),
                store: [[false, this.app.i18n._('No')], [true, this.app.i18n._('Yes')]],
                value: false
            }, commonComboConfig)]
        }, {
            title: this.app.i18n._('Session'),
            id: this.idPrefix+'setup-session-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'session_lifetime',
                fieldLabel: this.app.i18n._('Lifetime (seconds)'),
                xtype: 'numberfield',
                value: 86400,
                minValue: 0
            }, this.sessionBackendCombo,
            {
                id: this.sessionBackendIdPrefix + 'CardLayout',
                xtype: 'panel',
                layout: 'card',
                activeItem: this.sessionBackendIdPrefix + 'File',
                border: false,
                width: '100%',
                defaults: {border: false},
                items: [{
                    // file config options
                    id: this.sessionBackendIdPrefix + 'File',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'session_path',
                        fieldLabel: this.app.i18n._('Path')
                    }]
                }, {
                    // redis config options
                    id: this.sessionBackendIdPrefix + 'Redis',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'session_host',
                        fieldLabel: this.app.i18n._('Hostname'),
                        value: 'localhost'
                    }, {
                        name: 'session_port',
                        fieldLabel: this.app.i18n._('Port'),
                        xtype: 'numberfield',
                        minValue: 0,
                        value: 6379
                    }]
                }]
            },
            {
                name: 'session_storeAclIntoSession',
                fieldLabel: this.app.i18n._('Store ACL into session'),
                xtype: 'checkbox',
                value: false
            },
            {
                name: 'session_storePreferenceIntoSession',
                fieldLabel: this.app.i18n._('Store Preferences into session'),
                xtype: 'checkbox',
                value: false
            }
            ]
        }, {
            title: this.app.i18n._('Bugs Report'),
            id: this.idPrefix+'setup-bugReport-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'bugreportUrl',
                fieldLabel: this.app.i18n._('URL To Report Bugs'),
                value: Tine.Setup.registry.get(this.registryKey).bugreportUrl
            }]
        },{
            title: this.app.i18n._('Help and documentation'),
            id: this.idPrefix+'setup-helpdoc-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'helpdoc_text',
                fieldLabel: this.app.i18n._('Text label'),
                value: Tine.Setup.registry.get(this.registryKey)['text']
            }, {
                name: 'helpdoc_title',
                fieldLabel: this.app.i18n._('Mouseover title'),
                value: Tine.Setup.registry.get(this.registryKey)['title']
            }, {
                name: 'helpdoc_url',
                fieldLabel: this.app.i18n._('Hyperlink/URL'),
                value: Tine.Setup.registry.get(this.registryKey)['url']
            }]
        }, {
            title: this.app.i18n._('Theme'),
            id: this.idPrefix+'setup-theme-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'theme_load',
                fieldLabel: this.app.i18n._('Load theme'),
                xtype: 'checkbox'
            },
            {
                name: 'theme_path',
                fieldLabel: this.app.i18n._('Theme path')
            },
            {
                name: 'theme_useBlueAsBase',
                fieldLabel: this.app.i18n._('Theme useBlueAsBase'),
                xtype: 'checkbox',
                value: true
            },
            {
                name: 'theme_backgroundImageUrl',
                fieldLabel: this.app.i18n._('Background image url')
            },
            {
                name: 'theme_messageImageUrl',
                fieldLabel: this.app.i18n._('Message image url')
            },
            {
                name: 'theme_messageLinkUrl',
                fieldLabel: this.app.i18n._('Message link url')
            },
            {
                name: 'theme_accessibleLinkUrl',
                fieldLabel: this.app.i18n._('Accessible link url')
            }]
        });

        return formItens;
    },

    /**
     * @private
     */
    onChangeDomain: function () {
        Ext.Msg.confirm(this.app.i18n._('Change domain'), this.app.i18n._('Do you really want to change default domain?'), function(confirmbtn, value) {
            if (confirmbtn == 'yes') {
                this.onSaveConfig();
                this.afterSaveConfig = function() {
                    location.reload();
                    this.loadMask.show();
                }
            } else {
                Ext.getCmp(this.idPrefix+'domaindata-domain').setValue(Tine.Setup.registry.get('domainData')['activeDomain']);
            }
        }, this);
    },

    /**
     * applies registry state to this cmp
     */
    applyRegistryState: function () {
        Ext.getCmp(this.idPrefix+'setup-logger-group').setIconClass(Tine.Setup.registry.get('checkLogger') ? 'setup_checks_success' : 'setup_checks_fail');
        Ext.getCmp(this.idPrefix+'setup-caching-group').setIconClass(Tine.Setup.registry.get('checkCaching') ? 'setup_checks_success' : 'setup_checks_fail');
        Ext.getCmp(this.idPrefix+'setup-session-group').setIconClass(Tine.Setup.registry.get('checkSession') ? 'setup_checks_success' : 'setup_checks_fail');
        //Ext.getCmp(this.idPrefix+'setup-helpdoc-group').setIconClass(Tine.Setup.registry.get('helpdoc') ? 'setup_checks_success' : 'setup_checks_fail');
    },

    /**
     * @private
     */
    initActions: function () {
        this.action_downloadConfig = new Ext.Action({
            text: this.app.i18n._('Download config file'),
            iconCls: 'setup_action_download_config',
            scope: this,
            handler: this.onDownloadConfig
            //disabled: true
        });

        this.actionToolbarItems = [this.action_downloadConfig];

        Tine.Setup.GlobalConfigPanel.superclass.initActions.apply(this, arguments);
    },

    onDownloadConfig: function() {
        if (this.isValid()) {
            var configData = this.form2config();

            var downloader = new Ext.ux.file.Download({
                url: Tine.Tinebase.tineInit.requestUrl,
                params: {
                    method: 'Setup.downloadConfig',
                    data: Ext.encode(configData)
                }
            });
            downloader.start();
        } else {
            this.alertInvalidData();
        }
    }
});
