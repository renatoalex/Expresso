<?php
/**
 * convert functions for records from/to json (array) format
 * 
 * @package     Webconference
 * @subpackage  Convert
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * convert functions for records from/to json (array) format
 *
 * @package     Webconference
 * @subpackage  Convert
 */
class Webconference_Convert_Room_Json extends Tinebase_Convert_Json
{
    /**
    * converts Tinebase_Record_Abstract to external format
    *
    * @param  Tinebase_Record_Abstract $_record
    * @return mixed
    */
    public function fromTine20Model(Tinebase_Record_Abstract $_record)
    {
        self::resolveRelatedData($_record);
        return parent::fromTine20Model($_record);
    }
    
    /**
     * resolve related room data: attendee and organizer
     * 
     * @param Webconference_Model_Room $_record
     */
    static public function resolveRelatedData($_record)
    {
        if (! $_record instanceof Webconference_Model_Room) {
            return;
        }
        Webconference_Model_Attender::resolveAttendee($_record->attendee, TRUE, $_record);
        self::resolveOrganizer($_record);
    }
   
    
    /**
    * resolves organizer of given event
    *
    * @param Tinebase_Record_RecordSet|Webconference_Model_Room $_rooms
    */
    static public function resolveOrganizer($_rooms)
    {
        $rooms = $_rooms instanceof Tinebase_Record_RecordSet || is_array($_rooms) ? $_rooms : array($_rooms);
    
        $organizerIds = array();
        foreach ($rooms as $room) {
            if ($room->organizer) {
                $organizerIds[] = $room->organizer;
            }
        }
        $organizers = Addressbook_Controller_Contact::getInstance()->getMultiple(array_unique($organizerIds), TRUE);
        foreach ($rooms as $room) {
            if ($room->organizer && is_scalar($room->organizer)) {
                $idx = $organizers->getIndexById($room->organizer);
                if ($idx !== FALSE) {
                    $room->organizer = $organizers[$idx];
                }
            }
        }
    }
    
    /**
     * converts Tinebase_Record_RecordSet to external format
     * 
     * @param Tinebase_Record_RecordSet         $_records
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param Tinebase_Model_Pagination         $_pagination
     *
     * @return mixed
     */
    public function fromTine20RecordSet(Tinebase_Record_RecordSet $_records, $_filter = NULL, $_pagination = NULL)
    {
        if (count($_records) == 0) {
            return array();
        }
	
	Tinebase_Notes::getInstance()->getMultipleNotesOfRecords($_records);
	   
        Webconference_Model_Attender::resolveAttendee($_records->attendee, TRUE, $_records);
        Webconference_Convert_Room_Json::resolveOrganizer($_records);
        
        $_records->sortByPagination($_pagination);  
        Tinebase_Frontend_Json_Abstract::resolveContainerTagsUsers($_records, array('container_id'));
	$_records->setTimezone(Tinebase_Core::getUserTimezone());
        $roomsData = $_records->toArray();
        return $roomsData;
    }
}
