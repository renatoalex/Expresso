<?php
/**
 * Tine 2.0
 *
 * @package     Webconference
 * @subpackage  Backend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * sql backend class for the webconference
 *
 * @package     Webconference
 * @subpackage  Backend
 */
class Webconference_Backend_Sql extends Tinebase_Backend_Sql_Abstract
{
    /**
     * Table name without prefix
     *
     * @var string
     */
    protected $_tableName = 'wconf_room';
    
    /**
     * Model name
     *
     * @var string
     */
    protected $_modelName = 'Webconference_Model_Room';

    /**
     * if modlog is active, we add 'is_deleted = 0' to select object in _getSelect()
     *
     * @var boolean
     */
    protected $_modlogActive = TRUE;
    
     /**
     * attendee backend
     * 
     * @var Webconference_Backend_Sql_Attendee
     */
    protected $_attendeeBackend = NULL;
    
    /**
     * default column(s) for count
     *
     * @var string
     */
    //protected $_defaultCountCol = 'id';

     /**
     * list of record based grants
     */
    protected $_recordBasedGrants = array(
        Tinebase_Model_Grants::GRANT_READ, 
        Tinebase_Model_Grants::GRANT_SYNC,
        Tinebase_Model_Grants::GRANT_EXPORT,
        Tinebase_Model_Grants::GRANT_EDIT, 
        Tinebase_Model_Grants::GRANT_DELETE, 
        Tinebase_Model_Grants::GRANT_PRIVATE,
    );
    
    /**
     * should the class return contacts of disabled users
     * 
     * @var boolean
     */
    protected $_getDisabledContacts = FALSE;
    
    
    public function __construct($_dbAdapter=null, array $_options = array())
    {
    	parent::__construct($_dbAdapter, $_options);
	$this->_attendeeBackend = new Webconference_Backend_Sql_Attendee($_dbAdapter);
    }
    
     /**
     * Webconference optimized search function
     * 
     * 1. get all rooms neglecting grants filter
     * 2. get all related container grants (via resolveing)
     * 3. compute effective grants in PHP and only keep events 
     *    user has required grant for
     * 
     * @TODO rethink if an outer container filter could help
     *
     * @param  Tinebase_Model_Filter_FilterGroup    $_filter
     * @param  Tinebase_Model_Pagination            $_pagination
     * @param  boolean                              $_onlyIds
     * @param  bool   $_getDeleted
     * @return Tinebase_Record_RecordSet|array
     */
    public function search(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Model_Pagination $_pagination = NULL, $_onlyIds = FALSE, $_getDeleted = FALSE)    
    {
        if ($_pagination === NULL) {
            $_pagination = new Tinebase_Model_Pagination();
        }
        
        $select = parent::_getSelect('*', $_getDeleted);
        $select->joinLeft(
            /* table  */ array('attendee' => $this->_tablePrefix . 'wconf_attendee'),
            /* on     */ $this->_db->quoteIdentifier('attendee.wconf_room_id') . ' = ' . $this->_db->quoteIdentifier('wconf_room.id'),
            /* select */ array());
	
        if (! $_getDeleted) {
            $select->joinLeft(
                /* table  */ array('dispcontainer' => $this->_tablePrefix . 'container'), 
                /* on     */ $this->_db->quoteIdentifier('dispcontainer.id') . ' = ' . $this->_db->quoteIdentifier('attendee.displaycontainer_id'),
                /* select */ array());
            
            $select->where($this->_db->quoteIdentifier('dispcontainer.is_deleted') . ' = 0 OR ' . $this->_db->quoteIdentifier('dispcontainer.is_deleted') . 'IS NULL');
        }
	
        // remove grantsfilter here as we do grants computation in PHP
        $grantsFilter = $_filter->getFilter('grants');
        if ($grantsFilter) {
            $_filter->removeFilter('grants');
        }
        
        $this->_addFilter($select, $_filter);
        $_pagination->appendPaginationSql($select);
        
        $select->group($this->_tableName . '.' . 'id');
        Tinebase_Backend_Sql_Abstract::traitGroup($select);
	        
        $stmt = $this->_db->query($select);
        $rows = (array)$stmt->fetchAll(Zend_Db::FETCH_ASSOC);
        $result = $this->_rawDataToRecordSet($rows);
        $clones = clone $result;
        
        Tinebase_Container::getInstance()->getGrantsOfRecords($clones, Tinebase_Core::getUser());
        Webconference_Model_Attender::resolveAttendee($clones->attendee, TRUE, $clones);
        
        $me = Tinebase_Core::getUser()->contact_id;
        $inheritableGrants = array(
            Tinebase_Model_Grants::GRANT_READ, 
            Tinebase_Model_Grants::GRANT_SYNC, 
            Tinebase_Model_Grants::GRANT_EXPORT, 
            Tinebase_Model_Grants::GRANT_PRIVATE,
        );
        $toRemove = array();
        
        foreach($result as $room) {
            $clone = $clones->getById($room->getId());
            if ($room->organizer == $me) {
                foreach($this->_recordBasedGrants as $grant) {
		    // if room is expired don't edit record 
		    if ($room->status == "E" && $grant == Tinebase_Model_Grants::GRANT_EDIT){
			$room->{$grant} = FALSE;
		    } else {
			$room->{$grant} = TRUE;
		    }
                }
            } else {
                // grants to original container
                if ($clone->container_id instanceof Tinebase_Model_Container && $clone->container_id->account_grants) {
                    foreach($this->_recordBasedGrants as $grant) {
                        $room->{$grant} = $clone->container_id->account_grants[$grant] || $clone->container_id->account_grants[Tinebase_Model_Grants::GRANT_ADMIN];
                    }
                }
                
                // check grant inheritance
                foreach($inheritableGrants as $grant) {
                    if (! $room->{$grant} && $clone->attendee instanceof Tinebase_Record_RecordSet) {
                        foreach($clone->attendee as $attendee) {
                            if (   $attendee->displaycontainer_id instanceof Tinebase_Model_Container
                                && $attendee->displaycontainer_id->account_grants 
                                && (    $attendee->displaycontainer_id->account_grants[$grant]
                                     || $attendee->displaycontainer_id->account_grants[Tinebase_Model_Grants::GRANT_ADMIN]
                                   )
                            ){
                                $room->{$grant} = TRUE;
                                break;
                            }
                        }
                    }
                }
                
                if ($grantsFilter) {
                    $requiredGrants = array_intersect($grantsFilter->getRequiredGrants(), $this->_recordBasedGrants);
                    
                    $hasGrant = FALSE;
                    foreach($requiredGrants as $requiredGrant) {
                        if ($room->{$requiredGrant}) {
                            $hasGrant |= $room->{$requiredGrant};
                        }
                    }
                    
                    if (! $hasGrant) {
                        $toRemove[] = $room;
                    }
                }
            }
        }
	
        foreach($toRemove as $room) {
            $result->removeRecord($room);
        }
        return $_onlyIds ? $result->{is_bool($_onlyIds) ? $this->_getRecordIdentifier() : $_onlyIds} : $result;
    }
 
    /**
     * get the basic select object to fetch records from the database
     *  
     * @param array|string|Zend_Db_Expr $_cols columns to get, * per default
     * @param boolean $_getDeleted get deleted records (if modlog is active)
     * @return Zend_Db_Select
     */
    protected function _getSelectSimple($_cols = '*', $_getDeleted = FALSE)
    {
        $select = $this->_db->select();
	$select->from(array($this->_tableName => $this->_tablePrefix . $this->_tableName), $_cols);
        if (!$_getDeleted && $this->_modlogActive) {
            $select->where($this->_db->quoteIdentifier($this->_tableName . '.is_deleted') . ' = 0');
        
	}    
        return $select;
    }    
 
    /**
     * get the basic select object to fetch records from the database
     *  
     * @param array|string|Zend_Db_Expr $_cols columns to get, * per default
     * @param boolean $_getDeleted get deleted records (if modlog is active)
     * @return Zend_Db_Select
     */
    protected function _getSelect($_cols = '*', $_getDeleted = FALSE)
    {
	if ($_cols !== '*' ) {
            $cols = array();
            // make sure cols is an array, prepend tablename and fix keys
            foreach ((array) $_cols as $id => $col) {
                $key = (is_numeric($id)) ? ($col === self::IDCOL) ? $this->_identifier : $col : $id;
                $cols[$key] = ($col === self::IDCOL) ? $this->_tableName . '.' . $this->_identifier : $col;
            }
        } else {
            $cols = array('*');
        }
        $select = $this->_getSelectSimple($cols, $_getDeleted);
        $this->_appendEffectiveGrantCalculationSql($select);
        $select->group($this->_tableName . '.' . 'id');
        return $select;
    }
    
        /**
     * Gets total count of search with $_filter
     *
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param string $_action for right/acl check
     * @return int
     */
 /*
    public function searchCount(Tinebase_Model_Filter_FilterGroup $_filter, $_action = 'get')
    {
        
	$this->checkFilterACL($_filter, $_action);

        $count = $this->_backend->searchCount($_filter);

        return $count;
    }
     */
        /**
     * Gets total count of search with $_filter
     * 
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @return int
     */
    /*
    public function searchCount(Tinebase_Model_Filter_FilterGroup $_filter)
    {
        $select = $this->_getSelect(array('count' => 'COUNT(*)'));
        $this->_addFilter($select, $_filter);

        $result = $this->_db->fetchOne($select);
        
        return $result;
    } 
     */
   
    /**
     * appends effective grant calculation to select object
     *
     * @param Zend_Db_Select $_select
     */
    protected function _appendEffectiveGrantCalculationSql($_select, $_attendeeFilters = NULL)
    {
        // groupmemberships of current user, needed to compute phys and inherited grants
        $_select->joinLeft(
            /* table  */ array('groupmemberships' => $this->_tablePrefix . 'group_members'), 
            /* on     */ $this->_db->quoteInto($this->_db->quoteIdentifier('groupmemberships.account_id') . ' = ?' , Tinebase_Core::getUser()->getId()),
            /* select */ array());
        
        // attendee joins the attendee we need to compute the curr users effective grants
        // NOTE: 2010-04 the behaviour changed. Now, only the attendee the client filters for are 
        //       taken into account for grants calculation 
        $attendeeWhere = FALSE;
        if (is_array($_attendeeFilters) && !empty($_attendeeFilters)) {
            $attendeeSelect = $this->_db->select();
            foreach ((array) $_attendeeFilters as $attendeeFilter) {
                if ($attendeeFilter instanceof Webconference_Model_AttenderFilter) {
                    $attendeeFilter->appendFilterSql($attendeeSelect, $this);
                }
            }
            
            $whereArray = $attendeeSelect->getPart(Zend_Db_Select::SQL_WHERE);
            if (! empty($whereArray)) {
                $attendeeWhere = ' AND ' . Tinebase_Helper::array_value(0, $whereArray);
            }
        }
   	
        $_select->joinLeft(
            /* table  */ array('attendee' => $this->_tablePrefix . 'wconf_attendee'),
            /* on     */ $this->_db->quoteIdentifier('attendee.wconf_room_id') . ' = ' . $this->_db->quoteIdentifier('wconf_room.id') . 
                            $attendeeWhere,
            /* select */ array());
        $_select->joinLeft(
                /* table  */ array('attendeeaccounts' => $this->_tablePrefix . 'accounts'),
                /* on     */ $this->_db->quoteIdentifier('attendeeaccounts.contact_id') . ' = ' . $this->_db->quoteIdentifier('attendee.user_id') . ' AND (' .
                $this->_db->quoteInto($this->_db->quoteIdentifier('attendee.user_type') . '= ?', Webconference_Model_Attender::USERTYPE_USER) . ' OR ' .
                $this->_db->quoteInto($this->_db->quoteIdentifier('attendee.user_type') . '= ?', Webconference_Model_Attender::USERTYPE_GROUPMEMBER) .
                ')',
                /* select */ array());

        $_select->joinLeft(
                /* table  */ array('attendeegroupmemberships' => $this->_tablePrefix . 'group_members'),
                /* on     */ $this->_db->quoteIdentifier('attendeegroupmemberships.account_id') . ' = ' . $this->_db->quoteIdentifier('attendeeaccounts.contact_id'),
                /* select */ array());



        $_select->joinLeft(
                /* table  */ array('dispgrants' => $this->_tablePrefix . 'container_acl'),
                /* on     */ $this->_db->quoteIdentifier('dispgrants.container_id') . ' = ' . $this->_db->quoteIdentifier('attendee.displaycontainer_id') .
                ' AND ' . $this->_getContainGrantCondition('dispgrants', 'groupmemberships'),
                /* select */ array());

        $_select->joinLeft(
                /* table  */ array('physgrants' => $this->_tablePrefix . 'container_acl'),
                /* on     */ $this->_db->quoteIdentifier('physgrants.container_id') . ' = ' . $this->_db->quoteIdentifier('wconf_room.container_id'),
                /* select */ array());

        $allGrants = Tinebase_Model_Grants::getAllGrants();

        foreach ($allGrants as $grant) {
            if (in_array($grant, $this->_recordBasedGrants)) {
                $_select->columns(array($grant => "\n MAX( CASE WHEN ( \n" .
                    '  /* physgrant */' . $this->_getContainGrantCondition('physgrants', 'groupmemberships', $grant) . " OR \n" .
                    '  /* implicit  */' . $this->_getImplicitGrantCondition($grant) . " OR \n" .
                    '  /* inherited */' . $this->_getInheritedGrantCondition($grant) . " \n" .
                    ") THEN 1 ELSE 0 END ) "));
            } else {
                $_select->columns(array($grant => "\n MAX( CASE WHEN ( \n" .
                    '  /* physgrant */' . $this->_getContainGrantCondition('physgrants', 'groupmemberships', $grant) . "\n" .
                    ") THEN 1 ELSE 0 END ) "));
            }
        }
    }

    /**
     * returns SQL with container grant condition
     *
     * @param  string                               $_aclTableName
     * @param  string                               $_groupMembersTableName
     * @param  string|array                         $_requiredGrant (defaults none)
     * @param  Zend_Db_Expr|int|Tinebase_Model_User $_user (defaults current user)
     * @return string
     */
    protected function _getContainGrantCondition($_aclTableName, $_groupMembersTableName, $_requiredGrant = NULL, $_user = NULL)
    {
        $quoteTypeIdentifier = $this->_db->quoteIdentifier($_aclTableName . '.account_type');
        $quoteIdIdentifier = $this->_db->quoteIdentifier($_aclTableName . '.account_id');

        if ($_user instanceof Zend_Db_Expr) {
            $userExpression = $_user;
        } else {
            $accountId = $_user ? Tinebase_Model_User::convertUserIdToInt($_user) : Tinebase_Core::getUser()->getId();
            $userExpression = new Zend_Db_Expr($this->_db->quote($accountId));
        }

        $sql = $this->_db->quoteInto("($quoteTypeIdentifier = ?", Tinebase_Acl_Rights::ACCOUNT_TYPE_USER) . " AND $quoteIdIdentifier = $userExpression)" .
                $this->_db->quoteInto(" OR ($quoteTypeIdentifier = ?", Tinebase_Acl_Rights::ACCOUNT_TYPE_GROUP) . ' AND ' . $this->_db->quoteIdentifier("$_groupMembersTableName.group_id") . " = $quoteIdIdentifier" . ')' .
                $this->_db->quoteInto(" OR ($quoteTypeIdentifier = ?)", Tinebase_Acl_Rights::ACCOUNT_TYPE_ANYONE);

        if ($_requiredGrant) {
            $sql = "($sql) AND " . $this->_db->quoteInto($this->_db->quoteIdentifier($_aclTableName . '.account_grant') . ' IN (?)', (array) $_requiredGrant);
        }

        return "($sql)";
    }

    /**
     * returns SQL condition for implicit grants
     *
     * @param  string               $_requiredGrant
     * @param  Tinebase_Model_User  $_user (defaults to current user)
     * @return string
     */
    protected function _getImplicitGrantCondition($_requiredGrant, $_user = NULL)
    {
        $accountId = $_user ? $_user->getId() : Tinebase_Core::getUser()->getId();
        $contactId = $_user ? $_user->contact_id : Tinebase_Core::getUser()->contact_id;

        // delte grant couldn't be gained implicitly
        if ($_requiredGrant == Tinebase_Model_Grants::GRANT_DELETE) {
            return '1=0';
        }

        // organizer gets all other grants implicitly
        $sql = $this->_db->quoteIdentifier('wconf_room.organizer') . " = " . $this->_db->quote($contactId);

        // attendee get read, sync, export and private grants implicitly
        if (in_array($_requiredGrant, array(Tinebase_Model_Grants::GRANT_READ, Tinebase_Model_Grants::GRANT_SYNC, Tinebase_Model_Grants::GRANT_EXPORT, Tinebase_Model_Grants::GRANT_PRIVATE))) {
            $readCond = $this->_db->quoteIdentifier('attendeeaccounts.id') . ' = ' . $this->_db->quote($accountId) . ' AND (' .
                    $this->_db->quoteInto($this->_db->quoteIdentifier('attendee.user_type') . ' = ?', Webconference_Model_Attender::USERTYPE_USER) . ' OR ' .
                    $this->_db->quoteInto($this->_db->quoteIdentifier('attendee.user_type') . ' = ?', Webconference_Model_Attender::USERTYPE_GROUPMEMBER) .
                    ')';

            $sql = "($sql) OR ($readCond)";
        }

        return "($sql)";
    }

    /**
     * returns SQL for inherited grants
     *
     * @param  string $_requiredGrant
     * @return string
     */
    protected function _getInheritedGrantCondition($_requiredGrant)
    {
        // current user needs to have grant on display webconference
        $sql = $this->_getContainGrantCondition('dispgrants', 'groupmemberships', $_requiredGrant);

        // _AND_ attender(admin) of display webconference needs to have grant on phys webconference
        // @todo include implicit inherited grants
        if (!in_array($_requiredGrant, array(Tinebase_Model_Grants::GRANT_READ))) {
            $userExpr = new Zend_Db_Expr($this->_db->quoteIdentifier('attendeeaccounts.id'));

            $attenderPhysGrantCond = $this->_getContainGrantCondition('physgrants', 'attendeegroupmemberships', $_requiredGrant, $userExpr);
            // NOTE: this condition is weak! Not some attendee must have implicit grant.
            //       -> an attender we have reqired grants for his diplay cal must have implicit grants
            //$attenderImplicitGrantCond = $this->_getImplicitGrantCondition($_requiredGrant, $userExpr);
            //$sql = "($sql) AND ($attenderPhysGrantCond) OR ($attenderImplicitGrantCond)";
            $sql = "($sql) AND ($attenderPhysGrantCond)";
        }

        return "($sql)";
    }

    /****************************** attendee functions ************************/

    /**
     * creates given attender in database
     *
     * @param Webconference_Model_Attender $_attendee
     * @return Webconference_Model_Attender
     */
    public function createAttendee(Webconference_Model_Attender $_attendee)
    {
        if ($_attendee->user_id instanceof Addressbook_Model_Contact) {
            $_attendee->user_id = $_attendee->user_id->getId();
        } else if ($_attendee->user_id instanceof Addressbook_Model_List) {
            $_attendee->user_id = $_attendee->user_id->group_id;
        }
        
        if ($_attendee->displaycontainer_id instanceof Tinebase_Model_Container) {
            $_attendee->displaycontainer_id = $_attendee->displaycontainer_id->getId();
        }
	
        return $this->_attendeeBackend->create($_attendee);
    }    
    
    
    /**
     * updates given attender in database
     *
     * @param Webconference_Model_Attender $_attendee
     * @return Webconference_Model_Attender
     */
    public function updateAttendee(Webconference_Model_Attender $_attendee)
    {
        if ($_attendee->user_id instanceof Addressbook_Model_Contact) {
            $_attendee->user_id = $_attendee->user_id->getId();
        } else if ($_attendee->user_id instanceof Addressbook_Model_List) {
            $_attendee->user_id = $_attendee->user_id->group_id;
        }
        
        if ($_attendee->displaycontainer_id instanceof Tinebase_Model_Container) {
            $_attendee->displaycontainer_id = $_attendee->displaycontainer_id->getId();
        }
        
        return $this->_attendeeBackend->update($_attendee);
    }    
    
     /**
     * deletes given attender in database
     *
     * @param Webconference_Model_Attender $_attendee
     * @return void
     */
    public function deleteAttendee(array $_ids)
    {
        return $this->_attendeeBackend->delete($_ids);
    }
    
        /**
     * Creates new entry
     *
     * @param   Tinebase_Record_Interface $_record
     * @return  Tinebase_Record_Interface
     * @throws  Tinebase_Exception_InvalidArgument
     * @throws  Tinebase_Exception_UnexpectedValue
     */
    public function create(Tinebase_Record_Interface $_record) 
    {
	$room = parent::create($_record);
        return $this->get($room->getId());
    }
    
   /**
     * converts raw data from adapter into a single record
     *
     * @param  array $_data
     * @return Tinebase_Record_Abstract
     */
    protected function _rawDataToRecord(array $_rawData) {
        $room = parent::_rawDataToRecord($_rawData);
        $this->appendForeignRecordSetToRecord($room, 'attendee', 'id', Webconference_Backend_Sql_Attendee::FOREIGNKEY_ROOM, $this->_attendeeBackend);
        return $room;
    }
    
    /**
     * converts raw data from adapter into a set of records
     *
     * @param  array $_rawData of arrays
     * @return Tinebase_Record_RecordSet
     */
    protected function _rawDataToRecordSet(array $_rawData)
    {
        $rooms = new Tinebase_Record_RecordSet($this->_modelName);
        foreach ($_rawData as $rawRoom) {
            $rooms->addRecord(new Webconference_Model_Room($rawRoom, true));
        }      
        $this->appendForeignRecordSetToRecordSet($rooms, 'attendee', 'id', Webconference_Backend_Sql_Attendee::FOREIGNKEY_ROOM, $this->_attendeeBackend);
        return $rooms;
    }

    /**
     * search ID from room name
     * @param String Room Name
     * @return array of IDs
     */
    public function getRoomFromName($roomName)
    {
        $select = $this->_db->select()
                ->from(array($this->_tableName => $this->_tablePrefix . $this->_tableName), "id")
                ->where($this->_db->quoteIdentifier('room_name') . ' = ?', $roomName);
        $stmt = $this->_db->query($select);
        $rows = (array) $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
        return array('results' => $rows);
    }
}
