<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Http
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * Plugin for handling data device
 *
 * @package     Tinebase
 * @subpackage  Http
 */
class Tinebase_Http_UserAgent
{
    /**
     * default user agent mobile device tokens
     *
     * @var string
     */
    const MOBILE_PATTERN = '/android|blackberry|googlebot-mobile|iemobile|ipad|iphone|ipod|opera mobile|palmos|webos|windows phone/';

    /**
     * used user agent mobile device tokens
     *
     * @var string
     */
    private static $_mobilePattern = NULL;

    /**
     * neutralizes class
     *
     * @var boolean
     */
    private static $_disabled = FALSE;

    /**
     *
     * @param string $disabled
     */
    public static function setDisabled($disabled = TRUE)
    {
        self::$_disabled = $disabled;
    }

    /**
     * Change mobile device pattern
     * @param string $disabled
     */
    public static function setMobilePattern($pattern)
    {
        self::$_mobilePattern = $pattern;
    }

    /**
     *
     * @return boolean
     */
    public static function isMobile()
    {
        $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

        // Uses default pattern if another was not defined
        if (self::$_mobilePattern == NULL){
            self::$_mobilePattern = self::MOBILE_PATTERN;
        }

        $isMobile = (boolean) preg_match(self::$_mobilePattern, $userAgent);

        return $isMobile;
    }

    /**
     * Enable configuration into setup
     * and enable mobile request checking
     */
    public static function init()
    {
        Tinebase_PluginManager::addPluginConfigItem(
        'plugins_mobile_redirect',
        'checkbox',
        'Redirect to Mobile URL'
                );

        Tinebase_PluginManager::addPluginConfigItem(
        'plugins_mobile_url',
        'textfield',
        'Mobile URL'
                );

        self::$_disabled = FALSE;
    }

    /**
     * Call redirector class if plugin is enabled and request don't disable it.
     */
    public static function handleRedirectConditions()
    {
        if (self::$_disabled) return;

        // no user agent, no redirect
        if (!isset($_SERVER['HTTP_USER_AGENT'])) return;

        // login action must be not redirect
        if (Tinebase_Core::get(Tinebase_Core::REQUEST)->getMethod() == 'Tinebase.login') return;

        //=========================================================================================
        // Avoid redirect for specific cases
        //=========================================================================================

        //ActiveSync doesn't redirect
        if((isset($_SERVER['REDIRECT_ACTIVESYNC']) && $_SERVER['REDIRECT_ACTIVESYNC'] == 'true') ||
        (isset($_GET['frontend']) && $_GET['frontend'] == 'activesync')) return;

        //=========================================================================================

        // if user in mobile device wants to see web interface, add ?web to URL
        if (isset($_GET['web'])) return;

        self::redirectIfDeviceIsMobile();
    }

    /**
     * Does a HTTP request redirect if device is mobile
     *
     * config file must be a key:
     *
     * 'plugins' =>
     *    'mobile' => array(
     *        'redirect' => [TRUE | FALSE]
     *        'url' => [mobile url]
     * )
     */
    public static function redirectIfDeviceIsMobile()
    {
        $config = Tinebase_Config::getInstance();
        //Disabled into setup
        if (!isset($config->plugins->mobile->redirect) || !$config->plugins->mobile->redirect) return;

        if (self::isMobile()){
            $targetUrl = $config->plugins->mobile->url;
            header('Location: ' . $targetUrl);
            exit;
        }
    }
}