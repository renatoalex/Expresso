<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  User
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * User ldap backend
 *
 * @package     Tinebase
 * @subpackage  User
 */
class Tinebase_User_Ldap extends Tinebase_User_Sql implements Tinebase_User_Interface_SyncAble
{
    
    /**
     * backend type
     */
    const TYPE = Tinebase_User::LDAP;
    
    /**
     * @var array
     */
    protected $_options = array();

    /**
     * @var Tinebase_Ldap
     */
    protected $_ldap = NULL;

    /**
     * @var Tinebase_Ldap
     */
    protected $_masterLdap = NULL;

    /**
     * name of the ldap attribute which identifies a group uniquely
     * for example gidNumber, entryUUID, objectGUID
     * @var string
     */
    protected $_groupUUIDAttribute;

    /**
     * name of the ldap attribute which identifies a user uniquely
     * for example uidNumber, entryUUID, objectGUID
     * @var string
     */
    protected $_userUUIDAttribute;

    /**
     * mapping of ldap attributes to class properties
     *
     * @var array
     */
    protected $_rowNameMapping = array(
        'accountDisplayName'        => 'displayname',
        'accountFullName'           => 'cn',
        'accountFirstName'          => 'givenname',
        'accountLastName'           => 'sn',
        'accountLoginName'          => 'uid',
        'accountPrimaryGroup'       => 'gidnumber',
        'accountEmailAddress'       => 'mail',
        'accountHomeDirectory'      => 'homedirectory',
        'accountLoginShell'         => 'loginshell',
        'accountDN'                 => 'dn',
    );

    protected $_contactRowMapping = array(
            'n_given'               => 'givenname',
            'n_family'              => 'sn',
            'bday'                  => 'birthdate',
            'tel_cell'              => 'mobile',
            'tel_work'              => 'telephonenumber',
            'tel_home'              => 'homephone',
            'tel_fax'               => 'facsimiletelephonenumber',
            'org_name'              => 'o',
            'org_unit'              => 'ou',
            'email_home'            => 'mozillasecondemail',
            'jpegphoto'             => 'jpegphoto',
            'adr_two_locality'      => 'mozillahomelocalityname',
            'adr_two_postalcode'    => 'mozillahomepostalcode',
            'adr_two_region'        => 'mozillahomestate',
            'adr_two_street'        => 'mozillahomestreet',
            'adr_one_locality'      => 'l',
            'adr_one_postalcode'    => 'postalcode',
            'adr_one_street'        => 'street',
            'adr_one_region'        => 'st',
        );

    /**
     * objectclasses required by this backend
     *
     * @var array
     */
    protected $_requiredObjectClass = array(
        'top',
        'posixAccount',
        'inetOrgPerson',
    );

    /**
     * the base dn to work on (defaults to to userDn, but can also be machineDn)
     *
     * @var string
     */
    protected $_baseDn;

    /**
     * the basic group ldap filter (for example the objectclass)
     *
     * @var string
     */
    protected $_groupBaseFilter = 'objectclass=posixgroup';

    /**
     * the basic user ldap filter (for example the objectclass)
     *
     * @var string
     */
    protected $_userBaseFilter = 'objectclass=posixaccount';

    /**
     * the basic user search scope
     *
     * @var integer
     */
    protected $_userSearchScope = Zend_Ldap::SEARCH_SCOPE_SUB;

    /**
     * the query filter for the ldap search (for example uid=%s)
     *
     * @var string
     */
    protected $_queryFilter = '|(uid=%s)(cn=%s)(sn=%s)(givenName=%s)';
    
    protected $_ldapPlugins = array();
    
    protected $_isReadOnlyBackend     = false;

    /**
     * the constructor
     *
     * @param  array  $_options  Options used in connecting, binding, etc.
     * @throws Tinebase_Exception_Backend_Ldap
     */
    public function __construct(array $_options = array())
    {
        parent::__construct($_options);
        
        if(empty($_options['userUUIDAttribute'])) {
            $_options['userUUIDAttribute'] = 'entryUUID';
        }
        if(empty($_options['groupUUIDAttribute'])) {
            $_options['groupUUIDAttribute'] = 'entryUUID';
        }
        if(empty($_options['baseDn'])) {
            $_options['baseDn'] = $_options['userDn'];
        }
        if(empty($_options['userFilter'])) {
            $_options['userFilter'] = 'objectclass=posixaccount';
        }
        if(empty($_options['userSearchScope'])) {
            $_options['userSearchScope'] = Zend_Ldap::SEARCH_SCOPE_SUB;
        }
        if(empty($_options['groupFilter'])) {
            $_options['groupFilter'] = 'objectclass=posixgroup';
        }

        if (isset($_options['requiredObjectClass'])) {
            $this->_requiredObjectClass = (array)$_options['requiredObjectClass'];
        }
        if (array_key_exists('readonly', $_options)) {
            $this->_isReadOnlyBackend = (bool)$_options['readonly'];
        }

        $this->_options = $_options;

        $this->_userUUIDAttribute  = strtolower($this->_options['userUUIDAttribute']);
        $this->_groupUUIDAttribute = strtolower($this->_options['groupUUIDAttribute']);
        $this->_baseDn             = $this->_options['baseDn'];
        $this->_userBaseFilter     = $this->_options['userFilter'];
        $this->_userSearchScope    = $this->_options['userSearchScope'];
        $this->_groupBaseFilter    = $this->_options['groupFilter'];

        $this->_rowNameMapping['accountId'] = $this->_userUUIDAttribute;
        if(is_object(Tinebase_Core::getConfig()->redirecting) &&
                Tinebase_Core::getConfig()->redirecting->active &&
                !empty(Tinebase_Core::getConfig()->redirecting->ldapAttribute)
            ){
            $this->_rowNameMapping['balanceid'] = Tinebase_Core::getConfig()->redirecting->ldapAttribute;
        }

        if($_options['checkExpiredPassword'] == '1'){
            $this->_rowNameMapping['accountPasswordExpired'] = $_options['passwordExpirationAttribute'];
        }
        try {
            $ldap = Tinebase_Core::getUserBackend();
            $this->_ldap = $ldap['user'];
            $this->_masterLdap = $ldap['master'];
        } catch (Zend_Ldap_Exception $zle) {
            // @todo move this to Tinebase_Ldap?
            throw new Tinebase_Exception_Backend_Ldap('Could not bind to LDAP: ' . $zle->getMessage());
        }
        if(isset($_options['saveStatus']) && $_options['saveStatus'] == '1'){
            $this->_rowNameMapping['accountLastPasswordChange'] = 'shadowlastchange';
            $this->_rowNameMapping['accountExpires']            = 'shadowexpire';
            $this->_rowNameMapping['accountStatus']             = 'shadowinactive';
            $this->_requiredObjectClass[] = 'shadowAccount';
        }

        $this->_getExtraLDAPschemas();
        foreach ($this->_plugins as $plugin) {
            if ($plugin instanceof Tinebase_User_Plugin_LdapInterface) {
                $this->registerLdapPlugin($plugin);
            }
        }
    }
    
        /**
     * Returns the type of backend
     * @return string
     */
    public static function getType()
    {
        return self::TYPE;
    }
    
    /**
     * Returns default configurations of the backend
     * @return array
     */
    public static function getBackendConfigurationDefaults()
    {
        return array(
            'host' => '',
            'username' => '',
            'password' => '',
            'bindRequiresDn' => true,
            'syncWhenNotFound' => '1',
            'useRfc2307bis' => false,
            'userDn' => '',
            'userOus' =>'',
            'extraSchemas' =>'',
            'extraAttributes' =>'',
            'userLoginForUserIdgeneration' => 0,
            'userFilter' => 'objectclass=posixaccount',
            'userSearchScope' => Zend_Ldap::SEARCH_SCOPE_SUB,
            'groupsDn' => '',
            'groupFilter' => 'objectclass=posixgroup',
            'groupSchemas' => '',
            'groupAttributes' => '',
            'groupOU' => '',
            'groupSearchScope' => Zend_Ldap::SEARCH_SCOPE_SUB,
            'pwEncType' => 'CRYPT',
            'minUserId' => '10000',
            'maxUserId' => '29999',
            'minGroupId' => '11000',
            'maxGroupId' => '11099',
            'groupUUIDAttribute' => 'entryUUID',
            'userUUIDAttribute' => 'entryUUID',
            Tinebase_User::DEFAULT_USER_GROUP_NAME_KEY  => Tinebase_Group::DEFAULT_USER_GROUP,
            Tinebase_User::DEFAULT_ADMIN_GROUP_NAME_KEY => Tinebase_Group::DEFAULT_ADMIN_GROUP,
            'readonly' => false,
            'saveStatus' => '1',
            'masterLdapHost' => '',
            'masterLdapUsername' => '',
            'masterLdapPassword' => '',
            'masterLdapReadOnly' => false,
            'checkExpiredPassword' => '0',
            'passwordExpirationAttribute' => '',
            'passwordExpirationInterval' => '',
            'displayName' => '',
            'fullName' => '',
            'displaynameFormat'=> Tinebase_Config::DISPLAYNAME_FORMAT_SN_GIVENNAME,
            'mailListDn' => '',
            'mailListGid' => '',
            'mailListFilter' => '',
            'mailListSchemas' => '',
            'mailListAttributes' => '',
        );
    }

    /**
     * register ldap plugin
     *
     * @param Tinebase_User_Plugin_LdapInterface $plugin
     */
    public function registerLdapPlugin(Tinebase_User_Plugin_LdapInterface $plugin)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Registering " . get_class($plugin) . ' LDAP plugin.');
        
        $plugin->setLdap($this->_ldap);
        $this->_ldapPlugins[] = $plugin;
    }
    
    /**
     * get list of users
     *
     * @param string $_filter
     * @param string $_sort
     * @param string $_dir
     * @param int $_start
     * @param int $_limit
     * @param string $_accountClass the type of subclass for the Tinebase_Record_RecordSet to return
     * @return Tinebase_Record_RecordSet with record class Tinebase_Model_User
     */
    public function getUsersFromSyncBackend($_filter = NULL, $_sort = NULL, $_dir = 'ASC', $_start = NULL, $_limit = NULL, $_accountClass = 'Tinebase_Model_User')
    {
        $filter = $this->_getBaseFilter();

        if (!empty($_filter)) {
            $filter = $filter->addFilter(Zend_Ldap_Filter::orFilter(
                Zend_Ldap_Filter::contains($this->_rowNameMapping['accountFirstName'], Zend_Ldap::filterEscape($_filter)),
                Zend_Ldap_Filter::contains($this->_rowNameMapping['accountLastName'], Zend_Ldap::filterEscape($_filter)),
                Zend_Ldap_Filter::contains($this->_rowNameMapping['accountLoginName'], Zend_Ldap::filterEscape($_filter))
            ));
        }

        $accounts = $this->_ldap->search(
            $filter,
            $this->_baseDn,
            $this->_userSearchScope,
            array_values($this->_rowNameMapping),
            $_sort !== null ? $this->_rowNameMapping[$_sort] : null
        );

        $result = new Tinebase_Record_RecordSet($_accountClass);

        // nothing to be done anymore
        if (count($accounts) == 0) {
            return $result;
        }

        foreach ($accounts as $account) {
            $accountObject = $this->_ldap2User($account, $_accountClass);

            if ($accountObject) {
                $result->addRecord($accountObject);
            }

        }

        return $result;

        // @todo implement limit, start, dir and status
//         $select = $this->_getUserSelectObject()
//             ->limit($_limit, $_start);

//         if ($_sort !== NULL) {
//             $select->order($this->rowNameMapping[$_sort] . ' ' . $_dir);
//         }

//         // return only active users, when searching for simple users
//         if ($_accountClass == 'Tinebase_Model_User') {
//             $select->where($this->_db->quoteInto($this->_db->quoteIdentifier('status') . ' = ?', 'enabled'));
//         }
    }
    
    /**
     * returns user base filter
     *
     * @return Zend_Ldap_Filter_And
     */
    protected function _getBaseFilter()
    {
        return Zend_Ldap_Filter::andFilter(
            Zend_Ldap_Filter::string($this->_userBaseFilter)
        );
    }
    
    /**
     * search for user attributes
     *
     * @param array $attributes
     * @return array
     *
     * @todo allow multi value attributes
     * @todo generalize this for usage in other Tinebase_User_Ldap fns?
     */
    public function getUserAttributes($attributes)
    {
        $ldapCollection = $this->_ldap->search(
            $this->_getBaseFilter(),
            $this->_baseDn,
            $this->_userSearchScope,
            $attributes
        );
        
        $result = array();
        foreach ($ldapCollection as $data) {
            $row = array('dn' => $data['dn']);
            foreach ($attributes as $key) {
                $lowerKey = strtolower($key);
                if (isset($data[$lowerKey]) && isset($data[$lowerKey][0])) {
                    $row[$key] = $data[$lowerKey][0];
                }
            }
            $result[] = $row;
        }
        
        return (array)$result;
    }
    
    /**
     * fetch LDAP backend
     *
     * @return Tinebase_Ldap
     */
    public function getLdap()
    {
        return $this->_ldap;
    }

    /**
     * get user by login name
     *
     * @param   string  $_property
     * @param   string  $_accountId
     * @return Tinebase_Model_User the user object
     */
    public function getUserByPropertyFromSyncBackend($_property, $_accountId, $_accountClass = 'Tinebase_Model_User')
    {
        if (!array_key_exists($_property, $this->_rowNameMapping)) {
            throw new Tinebase_Exception_NotFound("can't get user by property $_property. property not supported by ldap backend.");
        }
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Getting from LDAP propety -> " . $_property . ' Value -> '.$_accountId);
        $ldapEntry = $this->_getLdapEntry($_property, $_accountId);
        Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " User from LDAP " . print_r($ldapEntry,true));
        $user = $this->_ldap2User($ldapEntry, $_accountClass);
        
        // append data from ldap plugins
        foreach ($this->_ldapPlugins as $class => $plugin) {
            $plugin->inspectGetUserByProperty($user, $ldapEntry);
        }
        
        return $user;
    }

    /**
     * set the password for given account
     *
     * @param   string  $_userId
     * @param   string  $_password
     * @param   bool    $_encrypt encrypt password
     * @param   bool    $_mustChange
     * @return  void
     * @throws  Tinebase_Exception_InvalidArgument
     */
    public function setPassword($_userId, $_password, $_encrypt = true, $_mustChange = false, $_oldPassword = null)
    {
        if( trim($_password) == "" ){
            $translate = Tinebase_Translation::getTranslation('Admin');
            throw new Tinebase_Exception_UnexpectedValue($translate->_("It's not possible to define password with an empty value"));
        }

        if(!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                    Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . " System is not configured to allow user to set a new password. User '$_userId' has tried");
                    $translate = Tinebase_Translation::getTranslation('Tinebase');
                    throw new Tinebase_Exception($translate->_('Server is readonly.'));
            }
        }

        //bypass password encryption; LDAP encrypts by itself
        if($this->_options['pwEncType'] == 'PLAIN')
        {
            $_encrypt = FALSE;
        }

        $encryptionType = isset($this->_options['pwEncType']) ? $this->_options['pwEncType'] : Tinebase_User_Abstract::ENCRYPT_SSHA;
        $userpassword = $_encrypt ? Hash_Password::generate($encryptionType, $_password) : $_password;

        $user = $_userId instanceof Tinebase_Model_FullUser ? $_userId : $this->getFullUserById($_userId);
        $ldapData = array(
            'userpassword'                                 => $userpassword,
        );
        $this->checkPasswordPolicy($_password, $user, $_oldPassword);

        $metaData = $this->_getMetaData($user);

        foreach ($this->_ldapPlugins as $plugin) {
            $plugin->inspectSetPassword($user, $_password, $_encrypt, $_mustChange, $ldapData);
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '  $dn: ' . $metaData['dn']);
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . '  $ldapData: ' . print_r($ldapData, true));
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " Trying to set a new password for user '$_userId'");
        $ldap->update($metaData['dn'], $ldapData);
        if((isset($this->_options['checkExpiredPassword'])) && ($this->_options['checkExpiredPassword'] != "") && ($this->_options['checkExpiredPassword'] == true))
        {
            if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " Setting password expiration attribute to '$_userId'");
            $todayPasswordChanged = $_mustChange?'0Z':$this->_DateDTFormatToLdap($user->getId());
            $ldapData = array(
                $this->_options['passwordExpirationAttribute'] => $todayPasswordChanged
            );
            $ldap->update($metaData['dn'], $ldapData);
        }


        // update last modify timestamp in sql backend too
        $values = array(
            'last_password_change' => Tinebase_DateTime::now()->get(Tinebase_Record_Abstract::ISO8601LONG),
        );

        $where = array(
            $this->_db->quoteInto($this->_db->quoteIdentifier('id') . ' = ?', $user->getId())
        );

        $this->_db->update(SQL_TABLE_PREFIX . 'accounts', $values, $where);

        $this->_setPluginsPassword($user->getId(), $_password, $_encrypt);
    }

    /**
     * update user status (enabled or disabled)
     *
     * @param   mixed   $_accountId
     * @param   string  $_status
     */
    public function setStatusInSyncBackend($_accountId, $_status)
    {
        if(isset($this->_options['saveStatus']) && $this->_options['saveStatus'] !== '1'){
            return;
        }
        if(!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                return;
            }
        }

        $metaData = $this->_getMetaData($_accountId);

        if ($_status == 'disabled') {
            $ldapData = array(
            'shadowMax'      => 1,
            'shadowInactive' => 1
            );
        } else {
            $ldapData = array(
            'shadowMax'      => 999999,
            'shadowInactive' => array(),
            'shadowexpire' => array()
            );
        }

        foreach ($this->_ldapPlugins as $plugin) {
            $plugin->inspectStatus($_status, $ldapData);
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " {$metaData['dn']}  $ldapData: " . print_r($ldapData, true));

        $ldap->update($metaData['dn'], $ldapData);
    }

    /**
     * sets/unsets expiry date in ldap backend
     *
     * expiryDate is the number of days since Jan 1, 1970
     *
     * @param   mixed      $_accountId
     * @param   Tinebase_DateTime  $_expiryDate
     */
    public function setExpiryDateInSyncBackend($_accountId, $_expiryDate)
    {
        if(isset($this->_options['saveStatus']) && $this->_options['saveStatus'] !== '1'){
            return;
        }
        if(!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                return;
            }
        }

        $metaData = $this->_getMetaData($_accountId);

        if ($_expiryDate instanceof DateTime) {
            // days since Jan 1, 1970
            $ldapData = array('shadowexpire' => floor($_expiryDate->getTimestamp() / 86400));
        } else {
            $ldapData = array('shadowexpire' => array());
        }

        foreach ($this->_ldapPlugins as $plugin) {
            $plugin->inspectExpiryDate($_expiryDate, $ldapData);
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " {$metaData['dn']}  $ldapData: " . print_r($ldapData, true));

        $ldap->update($metaData['dn'], $ldapData);
    }

    /**
     * updates an existing user
     *
     * @todo check required objectclasses?
     *
     * @param Tinebase_Model_FullUser $_account
     * @return Tinebase_Model_FullUser
     */
    public function updateUserInSyncBackend(Tinebase_Model_FullUser $_account)
    {
        $ldapEntry = $this->_getLdapEntry('accountId', $_account);

        if(!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                $fullUser = $this->_ldap2User($ldapEntry, 'Tinebase_Model_FullUser');
                return $fullUser;
            }
        }

        $ldapData = $this->_user2ldap($_account, $ldapEntry);
        $contact = Addressbook_Controller_Contact::getInstance()->get($_account->contact_id);
        $ldapData = $this->_contact2ldap($contact, $ldapData);
        
        foreach ($this->_ldapPlugins as $plugin) {
            $plugin->inspectUpdateUser($_account, $ldapData, $ldapEntry);
        }

        // no need to update this attribute, it's not allowed to change and even might not be updateable
        unset($ldapData[$this->_userUUIDAttribute]);

        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '  $dn: ' . $ldapEntry['dn']);
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . '  $ldapData: ' . print_r($ldapData, true));

        $errors = array();
        foreach($ldapData as $attibute => $value){
            try {
                $ldap->update($ldapEntry['dn'], array($attibute => $value));
            } catch (Exception $exc) {
                $errors[] = sprintf('%s => %s', $attibute, $value);
            }
        }

        if(!empty($errors)){
            Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . 'Error updating Attributes ' . implode(', ', $errors));
        }
        $newDN = $this->_generateDn($_account);
        // do we need to rename the entry?
        if ($newDN != $ldapEntry['dn']) {
            $groupsBackend = Tinebase_Group::factory(Tinebase_Group::LDAP);
            
            // get the current groupmemberships
            $memberships = $groupsBackend->getGroupMembershipsFromSyncBackend($_account);
            
            // remove the user from current groups, because the dn/uid has changed
            foreach ($memberships as $groupId) {
                $groupsBackend->removeGroupMemberInSyncBackend($groupId, $_account);
            }
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '  rename ldap entry to: ' . $newDN);
            $ldap->rename($ldapEntry['dn'], $newDN);
            
            // add the user to current groups again
            foreach ($memberships as $groupId) {
                $groupsBackend->addGroupMemberInSyncBackend($groupId, $_account);
            }
        }
        
        // refetch user from ldap backend
        $user = $this->getUserByPropertyFromSyncBackend('accountId', $_account, 'Tinebase_Model_FullUser');

        return $user;
    }

    /**
     * add an user
     *
     * @param   Tinebase_Model_FullUser  $_user
     * @return  Tinebase_Model_FullUser|NULL
     */
    public function addUserToSyncBackend(Tinebase_Model_FullUser $_user)
    {
        if(!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                return NULL;
            }
        }
        
        $ldapData = $this->_user2ldap($_user);
        $contact = $_user->contact;
        $contact['n_family'] = $_user->accountLastName;
        $contact['n_given'] = $_user->givenname;
        $contact = new Addressbook_Model_Contact($contact);
        $ldapData = $this->_contact2ldap($contact, $ldapData);

        $ldapData['uidnumber'] = $this->_generateUidNumber($_user->accountLoginName);
        $ldapData['objectclass'] = $this->_requiredObjectClass;

        foreach ($this->_ldapPlugins as $plugin) {
            $plugin->inspectAddUser($_user, $ldapData);
        }

        $dn = $this->_generateDn($_user);
        
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . 'Adding the user to LDAP with uidnumber: ' . $ldapData['uidnumber']);
        Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . 'Adding the user to LDAP ldapData: ' . print_r($ldapData, true));
        $ldap->add($dn, $ldapData);
        //Wait for the master LDAP server to synchronize with the slaves
        if($this->_useLdapMaster($ldap)){
            sleep(10);
        }
        $userId = $ldap->getEntry($dn, array($this->_userUUIDAttribute));

        $userId = $userId[$this->_userUUIDAttribute][0];

        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . 'User created with UserId: ' . $userId);
        $user = $this->getUserByPropertyFromSyncBackend('accountId', $userId, 'Tinebase_Model_FullUser');

        return $user;
    }

    /**
     * delete an user in ldap backend
     *
     * @param int $_userId
     */
    public function deleteUserInSyncBackend($_userId)
    {
        if (!$this->_useLdapMaster($ldap)) {
            if ($this->_isReadOnlyBackend) {
                return;
            }
        }

        try {
            $metaData = $this->_getMetaData($_userId);
    
            // user does not exist in ldap anymore
            if (!empty($metaData['dn'])) {
                $ldap->delete($metaData['dn']);
            }
        } catch (Tinebase_Exception_NotFound $tenf) {
            if (Tinebase_Core::isLogLevel(Zend_Log::CRIT)) Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__ . ' user not found in sync backend: ' . $_userId);
        }
    }

    /**
     * delete multiple users from ldap only
     *
     * @param array $_accountIds
     */
    public function deleteUsersInSyncBackend(array $_accountIds)
    {
        if ($this->_isReadOnlyBackend) {
            return;
        }
        
        foreach ($_accountIds as $accountId) {
            $this->deleteUserInSyncBackend($accountId);
        }
    }

    /**
     * return ldap entry of user
     *
     * @param string $_uid
     * @return array
     */
    protected function _getLdapEntry($_property, $_userId)
    {
        switch($_property) {
            case 'accountId':
                $value = Tinebase_Model_User::convertUserIdToInt($_userId);
                break;
            default:
                $value = $_userId;
                break;
        }

        $filter = Zend_Ldap_Filter::andFilter(
            Zend_Ldap_Filter::string($this->_userBaseFilter),
            Zend_Ldap_Filter::equals($this->_rowNameMapping[$_property], Zend_Ldap::filterEscape($value))
        );
        
        $attributes = array_values($this->_rowNameMapping);
        foreach ($this->_ldapPlugins as $plugin) {
            $attributes = array_merge($attributes, $plugin->getSupportedAttributes());
        }
        $attributes[] = 'objectclass';
        $attributes[] = 'uidnumber';
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' filter ' . $filter);
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' requested attributes ' . print_r($attributes, true));
        
        $accounts = $this->_ldap->search(
            $filter,
            $this->_baseDn,
            $this->_userSearchScope,
            $attributes
        );
        
        if (count($accounts) !== 1) {
            throw new Tinebase_Exception_NotFound('User with ' . $_property . ' =  ' . $value . ' not found.');
        }
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' current ldap values ' . print_r($accounts->getFirst(), true));
        
        return $accounts->getFirst();
    }
    
    /**
     * get metatada of existing user
     *
     * @param  string  $_userId
     * @return array
     */
    protected function _getMetaData($_userId)
    {
        $userId = Tinebase_Model_User::convertUserIdToInt($_userId);

        $filter = Zend_Ldap_Filter::equals(
            $this->_rowNameMapping['accountId'], Zend_Ldap::filterEscape($userId)
        );

        $result = $this->_ldap->search(
            $filter,
            $this->_baseDn,
            $this->_userSearchScope
        );

        if (count($result) !== 1) {
            throw new Tinebase_Exception_NotFound("user with userid $_userId not found");
        }

        return $result->getFirst();
    }

    /**
     * generates dn for new user
     *
     * @param  Tinebase_Model_FullUser $_account
     * @return string
     */
    protected function _generateDn(Tinebase_Model_FullUser $_account)
    {
        $baseDn = $this->_baseDn;

        $uidProperty = array_search('uid', $this->_rowNameMapping);
        if($_account->accountDN){
            $newDn = "uid={$_account->$uidProperty},{$_account->accountDN}";
        }else{
            $newDn = "uid={$_account->$uidProperty},{$baseDn}";
        }
        return $newDn;
    }
    
    /**
     * generates a uidnumber
     *

     * @param string $_loginName
     * @return int
     *
     * @todo add a persistent registry which id has been generated lastly to
     *       reduce amount of userid to be transfered
     */
    protected function _generateUidNumber($_loginName)
    {
        $conf = Tinebase_User::getBackendConfigurationWithDefaults();

        if($conf['Ldap']['userLoginForUserIdgeneration'] == 1 && preg_match("/[\d]{11}/",$_loginName) === 1){
            $return = (int)substr($_loginName,0,-2);
        }else{
            $return = (int)time();
        }
        return $return;
    }

    /**
     * return contact information for user
     *
     * @param  Tinebase_Model_FullUser    $_user
     * @param  Addressbook_Model_Contact  $_contact
     */
    public function updateContactFromSyncBackend(Tinebase_Model_FullUser $_user, Addressbook_Model_Contact $_contact)
    {
        $userData = $this->_getMetaData($_user);

        $userData = $this->_ldap->getEntry($userData['dn']);
        
        $this->_ldap2Contact($userData, $_contact);
        #if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . "  synced user object: " . print_r($_contact->toArray(), true));
    }
    
    /**
     * update contact data(first name, last name, ...) of user
     *
     * @param Addressbook_Model_Contact $contact
     * @todo implement logic
     */
    public function updateContactInSyncBackend($_contact)
    {
        
    }
    
    /**
     * Returns a user obj with raw data from ldap
     *
     * @param array $_userData
     * @param string $_accountClass
     * @return Tinebase_Record_Abstract
     */
    protected function _ldap2User(array $_userData, $_accountClass)
    {
        $errors = false;

        $userBackendOptions = Tinebase_User::getBackendConfigurationWithDefaults();
        if($userBackendOptions['Ldap']['displaynameFormat'] === Tinebase_Config::DISPLAYNAME_FORMAT_USE_OTHER){
            $this->_rowNameMapping['accountFullName']  = $userBackendOptions['Ldap']['fullName'];
            $this->_rowNameMapping['accountDisplayName'] = $userBackendOptions['Ldap']['displayName'];
        }
        foreach ($this->_rowNameMapping as $userInfo => $ldapAttribute) {
            if(!isset($_userData[$ldapAttribute])){
                continue;
            }
            switch($userInfo) {
                case 'accountDN':
                    $dn = Zend_Ldap_Dn::factory($_userData[$ldapAttribute], null);
                    $accountArray[$userInfo] = $dn->getParentDn()->tostring();
                    break;
                case 'accountLastPasswordChange':
                case 'accountExpires':
                    $accountArray[$userInfo] = new Tinebase_DateTime($_userData[$ldapAttribute][0] * 86400);
                    break;
                case 'accountPasswordExpired':
                    $accountArray[$userInfo] = $this->_ldap->testPasswordExpirationDate($_userData[$ldapAttribute][0])?'yes':'no';
                    break;
                case 'accountStatus':
                    if ( array_key_exists('shadowinactive', $_userData) && $_userData['shadowinactive'] == '1') {
                        $accountArray[$userInfo] = 'disabled';
                    } else {
                        $accountArray[$userInfo] = 'enabled';
                    }
                    break;
                default:
                    $accountArray[$userInfo] = $this->_fixEncoding($_userData[$ldapAttribute][0]);
                    break;
            }
        }

        if(array_key_exists('accountExpires',$accountArray) && $accountArray['accountExpires']  < new Tinebase_DateTime()){
            $accountArray['accountStatus'] = Tinebase_User::STATUS_DISABLED;
        }

        if ($errors) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Could not instantiate account object for ldap user ' . print_r($_userData, 1));
            $accountObject = null;
        } else {
            $accountObject = new $_accountClass($accountArray, TRUE);
        }

        return $accountObject;
    }

    /**
     * parse ldap result set and update Addressbook_Model_Contact
     *
     * @param array                      $_userData
     * @param Addressbook_Model_Contact  $_contact
     */
    protected function _ldap2Contact($_userData, Addressbook_Model_Contact $_contact)
    {
        $userBackendOptions = Tinebase_User::getBackendConfigurationWithDefaults();
        if($userBackendOptions['Ldap']['displaynameFormat'] === Tinebase_Config::DISPLAYNAME_FORMAT_USE_OTHER){
            $rowNameMapping['n_fn']  = $userBackendOptions['Ldap']['fullName'];
            $rowNameMapping['n_fileas'] = $userBackendOptions['Ldap']['displayName'];
         }
         foreach ($this->_contactRowMapping as $contactInfo => $ldapAttribute) {
                if(!isset($_userData[$ldapAttribute])){
                    continue;
                }
                switch($contactInfo) {
                    case 'bday':
                        $_contact->$contactInfo = Tinebase_DateTime::createFromFormat('Y-m-d', $_userData[$ldapAttribute][0]);
                        break;
                    default:
                        $_contact->$contactInfo = $this->_fixEncoding($_userData[$ldapAttribute][0]);
                        break;
                }
         }
    }

    /**
     * returns array of ldap data
     *
     * @param  Tinebase_Model_FullUser $_user
     * @return array
     */
    protected function _user2ldap(Tinebase_Model_FullUser $_user, array $_ldapEntry = array())
    {
        $ldapData = array();

         foreach ($_user as $key => $value) {
            $ldapProperty = array_key_exists($key, $this->_rowNameMapping) ? $this->_rowNameMapping[$key] : false;

            if ($ldapProperty) {
                switch ($key) {
                    case 'accountLastPasswordChange':
                        // field is readOnly
                        break;
                    case 'accountExpires':
                        $ldapData[$ldapProperty] = $value instanceof DateTime ? floor($value->getTimestamp() / 86400) : array();
                        break;
                    case 'accountPasswordExpired':
                        if((isset($this->_options['checkExpiredPassword'])) && ($this->_options['checkExpiredPassword'] != "") && ($this->_options['checkExpiredPassword'] == true) && ($value == 'yes')){
                            $ldapData[$this->_options['passwordExpirationAttribute']] = '0Z';
                        }
                        break;
                    case 'accountStatus':
                        if ($value == 'enabled') {
                            $ldapData['shadowMax']      = 999999;
                            $ldapData['shadowInactive'] = array();
                        } else {
                            $ldapData['shadowMax']      = 1;
                            $ldapData['shadowInactive'] = 1;
                        }
                        break;
                    case 'accountPrimaryGroup':
                        $ldapData[$ldapProperty] = Tinebase_Group::getInstance()->resolveUUIdToGIdNumber($value);
                        break;
                    default:
                        $ldapData[$ldapProperty] = $value;
                        break;
                }
            }
        }

        // homedir is an required attribute
        if (empty($ldapData['homedirectory'])) {
            $ldapData['homedirectory'] = '/dev/null';
        }
        
        $ldapData['objectclass'] = isset($_ldapEntry['objectclass']) ? $_ldapEntry['objectclass'] : array();
        
        // check if user has all required object classes. This is needed
        // when updating users which where created using different requirements
        foreach ($this->_requiredObjectClass as $className) {
            if (!in_array(strtolower($className), array_map('strtolower', $ldapData['objectclass']))) {
                // merge all required classes at once
                $arrayMerge = array_merge($ldapData['objectclass'],$this->_requiredObjectClass);
                $ldapData['objectclass'] = array_intersect_key($arrayMerge, array_unique(array_map('strtolower', $arrayMerge)));
               break;
            }
        }

        $ldapData = $this->_getExtraLDAPAttributes($ldapData);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' LDAP data ' . print_r($ldapData, true));
        
        return $ldapData;
    }

     /**
     * returns array of ldap data
     *
     * @param  adressbook_model_contact  $_contact
     * @param  array $_ldapdata
     * @return array
     */
    protected function _contact2ldap(Addressbook_Model_Contact $_contact, array $_ldapdata)
    {
        $ldapData = array();

        foreach ($_contact as $key => $value) {
            $ldapProperty = array_key_exists($key, $this->_contactRowMapping) ? $this->_contactRowMapping[$key] : false;

            if ($ldapProperty) {
                switch ($key) {
                    case 'bday':
                        $ldapData[$ldapProperty] = $value instanceof DateTime ? floor($value->getTimestamp() / 86400) : array();
                        break;
                    default:
                        $ldapData[$ldapProperty] = $value;
                        break;
                }
            }
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . 'contact LDAP data ' . print_r($ldapData, true));

        $return = array_merge($_ldapdata, $ldapData);
        return $return;
    }

    public function resolveUIdNumberToUUId($_uidNumber)
    {
        if ($this->_userUUIDAttribute == 'uidnumber') {
            return $_uidNumber;
        }

        $filter = Zend_Ldap_Filter::equals(
            'uidnumber', Zend_Ldap::filterEscape($_uidNumber)
        );

        $userId = $this->_ldap->search(
            $filter,
            $this->_baseDn,
            $this->_userSearchScope,
            array($this->_userUUIDAttribute)
        )->getFirst();

        return $userId[$this->_userUUIDAttribute][0];
    }

    /**
     * resolve UUID(for example entryUUID) to uidnumber
     *
     * @param string $_uuid
     * @return string
     */
    public function resolveUUIdToUIdNumber($_uuid)
    {
        if ($this->_groupUUIDAttribute == 'uidnumber') {
            return $_uuid;
        }

        $filter = Zend_Ldap_Filter::equals(
            $this->_userUUIDAttribute, Zend_Ldap::filterEscape($_uuid)
        );

        $groupId = $this->_ldap->search(
            $filter,
            $this->_options['userDn'],
            $this->_userSearchScope,
            array('uidnumber')
        )->getFirst();

        return $groupId['uidnumber'][0];
    }
    
     /**
     * converts current date/time value to LDAP format
     *
     * @param string $_uid
     * @return string
     */
    protected function _DateDTFormatToLdap($_uid)
    {
        $expirationInterval = "+" . Tinebase_User::getBackendConfiguration("passwordExpirationInterval") . " days";
        $newExpirationDate = date('YmdHis',  strtotime($expirationInterval));
        $ldapDateLayout = $newExpirationDate . 'Z';
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " Setting new password expiration time for user " . $_uid . ' on LDAP host: ' . $ldapDateLayout);
        return ($ldapDateLayout);
    }
    
     /**
     * search account vacation attributes from ldap server
     *
     * @param string $userId
     * @param array of string $attributes
     * @return array
     */
    public function getVacationAttributes($userId, $attributes)
    {
        $ldapCollection = $this->_ldap->search(
            "uid=$userId",
            $this->_baseDn,
            $this->_userSearchScope,
            $attributes
        );
        
        $result = array();
        foreach ($ldapCollection as $data) {
            $row = array('dn' => $data['dn']);
            foreach ($attributes as $key) {
                $lowerKey = strtolower($key);
                if (isset($data[$lowerKey]) && isset($data[$lowerKey][0])) {
                    $row[$key] = $data[$lowerKey][0];
                }
            }
            $result[] = $row;
        }
        return (array)$result;
    }


     /**
     * Get extra LDAP schemas from config and add it to $this->_requiredObjectClass
     *
     * @return void
     */
    protected function _getExtraLDAPschemas()
    {
        if(Tinebase_User::getBackendConfiguration('extraSchemas') != ''){
            $extraSchemas = explode(';', Tinebase_User::getBackendConfiguration('extraSchemas'));
            $extraSchemas = array_diff($extraSchemas, $this->_requiredObjectClass);
            $extraSchemas = array_map('strtolower', array_merge($extraSchemas, $this->_requiredObjectClass));
            $this->_requiredObjectClass = $extraSchemas;
        }
    }

     /**
     * Gets the ExtraLdapAttributes from Ldap Config and add it to the $ldapData array
     *
     * @param array $_LdapData
     * @return array
     */
    protected function _getExtraLDAPAttributes(array $_ldapData)
    {
        if(Tinebase_User::getBackendConfiguration('extraAttributes') != ''){
            $extraAttributes = explode(';', Tinebase_User::getBackendConfiguration('extraAttributes'));
            foreach ($extraAttributes as $attribute){
                $tmpArray = explode('=', $attribute);
                $_ldapData[$tmpArray[0]] = $tmpArray[1];
            }
        }
        return $_ldapData;
    }

    /**
     * Loads ldap object and returns a boolean telling if it can write
     * @param mixed $_ldap
     */
    protected function _useLdapMaster(&$_ldap)
    {
        $_ldap = $this->_ldap;
        if ($this->_masterLdap != null) {
            $_ldap = $this->_masterLdap;
            return true;
        }
        return false;
    }

    /**
     * Returns a connection to user backend
     * @param array $_options
     * @return Tinebase_Ldap
     * @throws Tinebase_Exception_Backend
     */
    public static function getBackendConnection(array $_options = array())
    {
        try {
            $userLdap = new Tinebase_Ldap($_options);
            $userLdap->bind();
            $ldap['user']   = $userLdap;
            $ldap['master'] = null;
            if(isset($_options['masterLdapHost']) && !empty($_options['masterLdapHost'])) {
                $_options['host']     = $_options['masterLdapHost'];
                $_options['username'] = $_options['masterLdapUsername'];
                $_options['password'] = $_options['masterLdapPassword'];
                $masterLdap = new Tinebase_Ldap($_options);
                $masterLdap->bind();
                $ldap['master'] = $masterLdap;
            }
        } catch (Zend_Ldap_Exception $zle) {
            throw new Tinebase_Exception_Backend_Ldap("Cannot connect to ldap: ".$zle->getMessage());
        }

        return $ldap;
    }

    /**
     * Checks if user backend is valid
     * @param mixed $_userBackend
     * @return boolean
     */
    public static function isValid($_userBackend)
    {
        return ($_userBackend instanceof Tinebase_Ldap);
    }

    /**
     * Check to see if the emails are already taken, return status 'failure' and the colision users
     *
     * @param array $_arrEmails
     * @return array
     */
    public function checkEmail(array $_arrEmails)
    {
        $arrReturn = array();
        $status = 'success';
        if (count($_arrEmails)>0){
            $ldapArrayReturn = array_values($this->_rowNameMapping);
            foreach ($_arrEmails as $email){
                $record = array(
                    'mail' => $email,
                    'failures' => array(),
                );
                $ldapFilter = Zend_Ldap_Filter::orFilter(
                        Zend_Ldap_Filter::equals('mail', $email),
                        Zend_Ldap_Filter::equals('mailAlternateAddress', $email)
                );
                $ldapCollection = $this->_ldap->search(
                    $ldapFilter,
                    $this->_baseDn,
                    $this->_userSearchScope,
                    $ldapArrayReturn
                );
                if ($ldapCollection->count()>0){
                    $status = 'failure';
                    foreach ($ldapCollection as $data) {
                        $record['failures'][] = $data;
                    }
                }
                $arrReturn[] = $record;
            }
        }else {
            $status = 'failure';
        }
        $return = array(
            'data'          => $arrReturn,
            'totalcount'    => count($arrReturn),
            'status'        => $status
        );
        return $return;
    }

    /**
     *
     * @param string $_value
     * @return string
    */
    protected function _fixEncoding($_value){
        if(mb_detect_encoding($_value, 'UTF-8', true) == 'UTF-8'){
            $return = $_value;
        }else{
            $return = iconv('ISO-8859-1','utf-8',$_value);
        }
        return $return;
    }

    /**
     * (non-PHPdoc)
     * @see Tinebase_User_Abstract::getUserVacationState
     */
    public function getUserVacationState($username)
    {
        $userlock = $this->getVacationAttributes($username, array(Tinebase_User::DEFAULT_ACCOUNT_VACATION_KEY, Tinebase_User::DEFAULT_VACATION_MSG));
        //uppercase atribute convertion
        if(strtoupper($userlock[0][Tinebase_User::DEFAULT_ACCOUNT_VACATION_KEY]) == 'TRUE'){
            if($userlock[0][Tinebase_User::DEFAULT_VACATION_MSG]){
                $vacation_msg = $userlock[0][Tinebase_User::DEFAULT_VACATION_MSG];
            }
            else {
                $vacation_msg = "Access denied. Account has restricted access by reason of vacation";
            }
            return(array(true, $vacation_msg));
        } else {
            return false;
        }
    }
}
