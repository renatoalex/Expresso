<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Exception
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 *
 */

/**
 * UniqueViolation exception
 * 
 * @package     Tinebase
 * @subpackage  Exception
 */
class Tinebase_Exception_UniqueViolation extends Tinebase_Exception
{
}
