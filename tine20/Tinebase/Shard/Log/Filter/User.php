<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Shard_Log
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@metaways.de>
 *
 */

/**
 * user filter for Zend_Log logger
 *
 * @package     Tinebase
 * @subpackage  Shard_Log
 */
class Tinebase_Shard_Log_Filter_User implements Zend_Log_Filter_Interface
{
    /**
     * @var string
     */
    protected $_name;

    /**
     * Filter out any log messages not matching $name.
     *
     * @param  string  $_name     Username to log message
     * @throws Zend_Log_Exception
     */
    public function __construct($_name)
    {
        if (! is_string($_name)) {
            require_once 'Zend/Log/Exception.php';
            throw new Zend_Log_Exception('Name must be a string');
        }

        $this->_name = $_name;
    }

    /**
     * Returns TRUE to accept the message, FALSE to block it.
     *
     * @param  array    $_event    event data
     * @return boolean            accepted?
     */
    public function accept($_event)
    {
        $username = (is_object(Tinebase_Core::getUser())) ? Tinebase_Core::getUser()->accountLoginName : '';
        return strtolower($this->_name) == strtolower($username) ? TRUE : FALSE;
    }
}
