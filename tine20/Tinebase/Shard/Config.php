<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase_Shard
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * The class provides functions to handle shard config options
 *
 * @package     Tinebase_Shard
 * @subpackage  Config
 *
 */
class Tinebase_Shard_Config extends Tinebase_Config_Abstract
{
    /**
     * path and name of config file used by this class
     *
     * @var string
     */
    protected static $_configFileName = 'shard.inc.php';

    /**
     * config file data.
     *
     * @var array
     */
    protected static $_configFileData;

    /*
     * NUMBER_OF_VIRTUAL_SHARDS
     *
     * @var integer
     */
    const NUMBER_OF_VIRTUAL_SHARDS = 'numberOfVirtualShards';


    /*
     * ASSOCIATION_VIRTUALSHARD_SHARDKEY_STRATEGY
     *
     * @var string
     */
    const ASSOCIATION_VIRTUALSHARD_SHARDKEY_STRATEGY = 'associationVirtualshardShardkeyStrategy';

    /*
     * SETUP_BACKEND_CONNECTION_CONFIG
     *
     * @var string
     */
    const SETUP_CONNECTION_CONFIG_KEY = 'setupConnectionConfigKey';

    /*
     * BACKEND_CONNECTION_CONFIG
     *
     * @var Tinebase_Config_Struct
     */
    const BACKEND_CONNECTION_CONFIG = 'backendConnectionConfig';

    /*
     * CONFIG_ASSOCIATION
     *
     * @var array
     */
    const CONFIG_ASSOCIATION = 'configAssociation';

    /*
     * CONNECTION_CONFIG_CLASS
     *
     * @var array
     */
    const CONNECTION_CONFIG_CLASS = 'connectionConfigClass';

    /*
     * ASSOCIATION_VIRTUALSHARD_CONNECTIONCONFIGKEY_CLASS
     *
     * @var array
     */
    const ASSOCIATION_VIRTUALSHARD_CONNECTIONCONFIGKEY_CLASS = 'associationVirtualshardConnectionconfigkeyClass';

    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Definition::$_properties
     */
    protected static $_properties = array(
        self::NUMBER_OF_VIRTUAL_SHARDS => array(
            //_('Number of Virtual Shards')
            'label' => 'Number of Virtual Shards',
            //_('Number Of Virtual Shards')
            'description' => 'Number of Virtual Shards',
            'type' => 'integer',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule' => FALSE,
            'setBySetupModule' => TRUE,
        ),
        self::ASSOCIATION_VIRTUALSHARD_SHARDKEY_STRATEGY => array(
            //_('Association Virtualshard Shardkey strategy')
            'label' => 'Association Virtualshard Shardkey strategy',
            //_('How Shardkeys and VirtualShards are associated')
            'description' => 'How Shardkeys and VirtualShards are associated',
            'type' => 'string',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule' => FALSE,
            'setBySetupModule' => TRUE,
        ),
        self::SETUP_CONNECTION_CONFIG_KEY => array(
            //_('Setup Connection Config Key')
            'label' => 'Setup Connection Config Key',
            //_('Connection Config Key used by Setup')
            'description' => 'Connection Config Key used by Setup',
            'type' => 'string',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule' => FALSE,
            'setBySetupModule' => TRUE,
        ),
        self::BACKEND_CONNECTION_CONFIG => array(
                                   //_('Backend Connection Config')
            'label'                 => 'Backend Connection Config',
                                   //_('Backend Connection Config')
            'description'           => 'Backend Connection Config',
            'type'                  => 'object',
            'class'                 => 'Tinebase_Config_Struct',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => FALSE,
            'setBySetupModule'      => TRUE,
        ),
        self::CONFIG_ASSOCIATION => array(
                                   //_('Backend Connection Config Associations')
            'label'                 => 'Backend Connection Config Associations',
                                   //_('Association between Virtual Shards and Backend Connection Configs.')
            'description'           => 'Association between Virtual Shards and Backend Connection Configs.',
            'type'                  => 'array',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => FALSE,
            'setBySetupModule'      => TRUE,
        ),
        self::CONNECTION_CONFIG_CLASS => array(
                                   //_('Backend Connection Config Associations')
            'label'                 => 'Connection Config Class',
                                   //_('Connection config class.')
            'description'           => 'Connection config class.',
            'type'                  => 'array',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => FALSE,
            'setBySetupModule'      => TRUE,
        ),
        self::ASSOCIATION_VIRTUALSHARD_CONNECTIONCONFIGKEY_CLASS => array(
                                   //_('Association between Virtualshard and Connection config class')
            'label'                 => 'Association between Virtualshard and Connection config class',
                                   //_('Association between Virtualshard and Connection config class.')
            'description'           => 'Association between Virtualshard and Connection config class.',
            'type'                  => 'array',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => FALSE,
            'setBySetupModule'      => TRUE,
        ),
    );

    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::$_appName
     */
    protected $_appName = 'Tinebase';

    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Shard_Config
     */
    private static $_instance = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    protected function __construct() {}

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __clone() {}

    /**
     * Returns instance of Tinebase_Config
     *
     * @return Tinebase_Config
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_Shard_Config();
        }

        return self::$_instance;
    }

    /**
     * gets login name
     * @return string
     */
    public static function getLoginName()
    {
        return self::$_loginName;
    }

    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::getProperties()
     */
    public static function getProperties()
    {
        return self::$_properties;
    }

    /**
     * verify if Shard configFile exists
     *
     * @return bool
     */
    public function configFileExists()
    {
        if (self::_hasIdentity()) {
            $fileNamePath = self::getDomainConfigFileNamePath();

            if (empty($fileNamePath)) {
                return FALSE;
            }

            if (file_exists($fileNamePath)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * returns data from shard configuration file
     *
     * @param string $_readFromFile
     * @return array
     */
      protected function _getConfigFileData($_readFromFile = FALSE)
      {
        $globalConfigData = array();
        if ($_readFromFile || ! static::$_configFileData) {
            $globalConfigData = $this->_readGlobalConfigFile();
        }

        $domainConfigData = array();
        if (Tinebase_Config_Manager::isMultidomain() && static::hasDomain()) {
            $domainConfigData = include(static::getDomainConfigFileNamePath(static::getDomain()));
        }

        if (! empty($domainConfigData) || ! empty($globalConfigData)) {
            if (self::$_domainOverrideGlobal){
                $configData = array_merge($globalConfigData, $domainConfigData);
            } else {
                $configData = array_merge($domainConfigData, $globalConfigData);
            }
            static::$_configFileData = $configData;
        }

        return static::$_configFileData;
    }

    /**
     * returns complete path to domain config file
     *
     * @param string | optional $_domain
     * @return string
     */
    public static function getDomainConfigFileNamePath($_domain = NULL)
    {
        $path = realpath(__DIR__ . '/../../');
        $fileNamePath = $path . DIRECTORY_SEPARATOR . 'domains'
                      . DIRECTORY_SEPARATOR . (empty($_domain)
                      ? static::getDomain() :
                        $_domain) . DIRECTORY_SEPARATOR . static::$_configFileName;

        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '::'.$fileNamePath.' loaded');

        return $fileNamePath;
    }

     /**
     * get config for client registry
     *
     * @return Tinebase_Config_Struct
     */
    public function getClientRegistryConfig()
    {
        // get config names to be included in registry
        $clientProperties = new Tinebase_Config_Struct(array());

        $clientProperties['Tinebase_Shard'] = new Tinebase_Config_Struct(array());
        $properties = self::getProperties();
        foreach ((array) $properties as $name => $definition) {

            if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__
                . ' ' . print_r($definition, TRUE));

            if (array_key_exists('clientRegistryInclude', $definition) && $definition['clientRegistryInclude'] === TRUE) {
                // add definition here till we have a better place
                $configRegistryItem = new Tinebase_Config_Struct(array(
                    'value'         => $this->{$name},
                    'definition'    => new Tinebase_Config_Struct($definition),
                ));
                if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__
                    . ' ' . print_r($configRegistryItem->toArray(), TRUE));
                $clientProperties['Tinebase_Shard'][$name] = $configRegistryItem;
            }
        }
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
             . ' Got ' . count($clientProperties['Tinebase_Shard']) . ' config items for ' . 'Tinebase_Shard' . '.');

        return $clientProperties;
    }

    /**
     * get option setting string
     *
     * @deprecated
     * @param Tinebase_Record_Interface $_record
     * @param string $_label
     * @return string
     */
    public static function getOptionString($_record, $_label)
    {
        $controller = Tinebase_Core::getApplicationInstance($_record->getApplication());
        $settings = $controller->getConfigSettings();
        $idField = $_label . '_id';

        $option = $settings->getOptionById($_record->{$idField}, $_label . 's');

        $result = (isset($option[$_label])) ? $option[$_label] : '';

        return $result;
    }
}