<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Session
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 SERPRO (http://www.serpro.gov.br)
 *
 */

/**
 * A proxy for storing values in session
 *
 * @package     Tinebase
 * @subpackage  Session
 */
abstract class Tinebase_Session_Storage_Abstract
{
    /**
     *
     * @var boolean
     */
    protected $_disabled = TRUE;

    /**
     *
     * @var Tinebase_Session_Storage
     */
    protected static $_instance = NULL;

    /**
     *
     * @return Tinebase_Session_Storage
     */
    protected static function getInstance()
    {
        if (NULL == static::$_instance){
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    /**
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return Tinebase_Session::getSessionNamespace(get_called_class())->$key;
    }

    /**
     *
     * @param string $key
     * @return boolean
     */
    public function has($key)
    {
        try {
            return isset(Tinebase_Session::getSessionNamespace(get_called_class())->$key);
        } catch (Zend_Session_Exception $zse) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . "::" . __LINE__ .
                    ":: It was not possible to get Tinebase Session Namespace");
            return false;
        }
    }

    /**
     *
     * don't save if disabled
     *
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        if ($this->_disabled || !Tinebase_Session::isWritable()) {
            return;
        }
        try {
            $sessionNamespace = Tinebase_Session::getSessionNamespace(get_called_class());
            $sessionNamespace->unlock();
            $sessionNamespace->$key = $value;
            $sessionNamespace->lock();
        } catch (Zend_Session_Exception $zse) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . "::" . __LINE__ .
                    ":: It was not possible to get Tinebase Session Namespace");
        }

    }

    /**
     *
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->_disabled = $disabled;
    }

    /**
     *
     * @param string $key
     */
    public function remove($key)
    {
        unset(Tinebase_Session::getSessionNamespace(get_called_class())->$key);
    }
}
