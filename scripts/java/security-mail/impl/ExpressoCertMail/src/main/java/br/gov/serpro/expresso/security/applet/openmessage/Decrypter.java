package br.gov.serpro.expresso.security.applet.openmessage;

import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.RecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientId;
import org.bouncycastle.mail.smime.SMIMEEnveloped;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMEUtil;
import static java.util.logging.Level.*;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.*;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.Error.*;

public class Decrypter {

    private static final Logger logger = Logger.getLogger(Decrypter.class.getName());

    private final TokenBroker tokenBroker;

    public Decrypter(TokenBroker tokenBroker) {
        this.tokenBroker = tokenBroker;
    }

    public MimeBodyPart process(MimePart mimePart) {

        PKCS11Config pkcs11Config = tokenBroker.pickTokenProvider();

        if(pkcs11Config == null) {
            raise(TOKEN_NOT_FOUND);
        }

        if( ! pkcs11Config.isLoaded()) {
            switch(tokenBroker.login(pkcs11Config, 3)) {
                case FAILURE:
                    raise(INCORRECT_PIN);

                case CANCELLED:
                    raise(OPERATION_CANCELLED);
            }
        }

        X509Certificate cert = null;

        Enumeration<String> aliases = null;
        try {
            aliases = pkcs11Config.getKeyStore().aliases();
        }
        catch (KeyStoreException ex) {
            logger.log(SEVERE, ex.getMessage(), ex);
            raise(ex);
        }

        String keyAlias = null;
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            try {
                if (pkcs11Config.getKeyStore().isKeyEntry(alias)) {
                    keyAlias = alias;
                }
            }
            catch (KeyStoreException ex) {
                raise(ex);
            }
        }
        if (keyAlias == null) {
            logger.severe("Não achou a chave primária do token");
            raise(PRIMARY_KEY_NOT_FOUND);
        }
        try {
            cert = (X509Certificate) pkcs11Config.getKeyStore().getCertificate(keyAlias);
        }
        catch (KeyStoreException ex) {
            raise(ex);
        }

        RecipientId recId = new JceKeyTransRecipientId(cert);

        SMIMEEnveloped m = null;
        try {
            m = (mimePart instanceof MimeMessage)
                    ? new SMIMEEnveloped((MimeMessage) mimePart)
                    : new SMIMEEnveloped((MimeBodyPart) mimePart);
        }
        catch(MessagingException ex) {
            raise(ex);
        }
        catch(CMSException ex) {
            raise(ex);
        }

        PrivateKey key = null;
        try {
            key = (PrivateKey) pkcs11Config.getKeyStore().getKey(keyAlias, null);
        }
        catch (GeneralSecurityException ex) {
            raise(ex);
        }

        JceKeyTransEnvelopedRecipient keyTransRec = new JceKeyTransEnvelopedRecipient(key);
        keyTransRec.setProvider(pkcs11Config.getProvider());

        RecipientInformationStore recipients = m.getRecipientInfos();
        RecipientInformation recipient = recipients.get(recId);

        try {
            return SMIMEUtil.toMimeBodyPart(recipient.getContent(keyTransRec));
        }
        catch (SMIMEException ex) {
            return raise(ex);
        }
        catch (CMSException ex) {
            return raise(ex);
        }
    }

}
