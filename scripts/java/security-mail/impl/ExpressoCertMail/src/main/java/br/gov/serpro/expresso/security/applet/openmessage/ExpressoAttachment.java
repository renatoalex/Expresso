package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.logging.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

// TODO: We get three different attachment types/models. Define some polymorphic types
// and a method getURI() or getURL() for easier implementation within the downloader
@JsonPropertyOrder({
    "error", "content-type", "filename", "partId", "size", "description", "cid", "eid"})
public class ExpressoAttachment {

    private static final Logger logger = Logger.getLogger(ExpressoAttachment.class.getName());

    private Integer error;
    @JsonProperty("error")
    public void setError(Integer error) {
        this.error = error;
    }
    public Integer getError() {
        return error;
    }
    
    
    private String contentType;
    @JsonProperty("content-type")
    public void setContentType(String value) {
        contentType = value;
    }
    public String getContentType() {
        return contentType;
    }


    private String fileName;
    @JsonProperty("filename")
    public void setFileName(String value) {
        fileName = value;
    }
    public String getFileName() {
        return fileName;
    }


    private String partId;
    @JsonProperty("partId")
    @JsonIgnore
    public void setPartId(String value) {
        partId = value;
    }
    @JsonIgnore
    public String getPartId() {
        return partId;
    }


    private Integer size;
    @JsonProperty("size")
    public void setSize(Integer value) {
        size = value;
    }
    public Integer getSize() {
        return size;
    }


    private String description;
    @JsonProperty("description")
    public void setDescription(String value) {
        description = value;
    }
    public String getDescription() {
        return description;
    }


    private String cid;
    @JsonProperty("cid")
    public void setCid(String value) {
        cid = value;
    }
    public String getCid() {
        return cid;
    }


    private String eid;
    @JsonProperty("eid")
    public void setEid(String value) {
        eid = value;
    }
    public String getEid() {
        return eid;
    }

    //==========================================================================
    private byte[] body;
    @JsonIgnore
    public void setBody(byte[] value) {
        body = value;
    }
    public byte[] getBody() {
        return body;
    }

}
