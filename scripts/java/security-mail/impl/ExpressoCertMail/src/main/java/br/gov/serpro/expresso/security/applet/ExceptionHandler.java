package br.gov.serpro.expresso.security.applet;

public interface ExceptionHandler {
    void onException(Exception ex);
}
