package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "id", "eid", "account-id", "subject", "from_email", "from_name",
    "sender", "sender_account", "to", "cc", "bcc", "sent", "received", "flags",
    "size", "body", "headers", "structure", "attachments", "has_attachment",
    "signatures", "has_signature",
    "original_id", "folder_id", "note", "preparedParts", "reading_conf",
    "importance", "signature_info", "embedded_images"})
public class ExpressoMessage {

    private static final Logger logger = Logger.getLogger(ExpressoMessage.class.getName());

    public enum Flag { flagged, draft, deleted, seen, answered, passed };

    public enum Importance { high, normal };


    private String id;
    @JsonProperty("id")
    public void setId(String value) {
        id = value;
    }
    public String getId() {
        return id;
    }


    private String eid;
    @JsonProperty("eid")
    public void setEid(String value) {
        eid = value;
    }
    public String getEid() {
        return eid;
    }


    private String accountId;
    @JsonProperty("account-id")
    public void setAccountId(String value) {
        accountId = value;
    }
    public String getAccountId() {
        return accountId;
    }


    private String subject;
    @JsonProperty("subject")
    public void setSubject(String value) {
        subject = value;
    }
    public String getSubject() {
        return subject;
    }


    private String fromEmail;
    @JsonProperty("from_email")
    public void setFromEmail(String value) {
        fromEmail = value;
    }
    public String getFromEmail() {
        return fromEmail;
    }


    private String fromName;
    @JsonProperty("from_name")
    public void setFromName(String value) {
        fromName = value;
    }
    public String getFromName() {
        return fromName;
    }


    private String sender;
    @JsonProperty("sender")
    public void setSender(String value) {
        sender = value;
    }
    public String getSender() {
        return sender;
    }


    private String senderAccount;
    @JsonProperty("sender_account")
    public void setSenderAccount(String value) {
        senderAccount = value;
    }
    public String getSenderAccount() {
        return senderAccount;
    }


    @JsonProperty("to")
    private final List<String> to = new ArrayList<String>();
    @JsonIgnore
    public void addTo(String value) {
        to.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getToIterator() {
        return to.listIterator();
    }


    @JsonProperty("cc")
    private final List<String> cc = new ArrayList<String>();
    @JsonIgnore
    public void addCc(String value) {
        cc.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getCcIterator() {
        return cc.listIterator();
    }


    @JsonProperty("bcc")
    private final List<String> bcc = new ArrayList<String>();
    @JsonIgnore
    public void addBcc(String value) {
        bcc.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getBccIterator() {
        return bcc.listIterator();
    }


    private Date sent;
    @JsonProperty("sent")
    public void setSent(Date value) {
        sent = value;
    }
    public Date getSent() {
        return sent;
    }


    private Date received;
    @JsonProperty("received")
    public void setReceived(Date value) {
        received = value;
    }
    public Date getReceived() {
        return received;
    }


    @JsonProperty("flags")
    private final List<Flag> flags = new ArrayList<Flag>();
    @JsonIgnore
    public void addFlag(Flag value) {
        flags.add(value);
    }
    @JsonIgnore
    public ListIterator<Flag> getFlagsIterator() {
        return flags.listIterator();
    }


    private Integer size;
    @JsonProperty("size")
    public void setSize(Integer value) {
        size = value;
    }
    public Integer getSize() {
        return size;
    }


    private String body;
    @JsonProperty("body")
    public void setBody(String value) {
        body = value;
    }
    public String getBody() {
        return body;
    }


    @JsonProperty("headers")
    private final List<String> headers = new ArrayList<String>();
    @JsonIgnore
    public void addHeader(String value) {
        headers.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getHeadersIterator() {
        return headers.listIterator();
    }


    //TODO: o que é isso ?
    @JsonProperty("structure")
    private final List<String> structure = new ArrayList<String>();
    @JsonIgnore
    public void addStructure(String value) {
        structure.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getStructureIterator() {
        return structure.listIterator();
    }


    @JsonProperty("attachments")
    private final List<ExpressoAttachment> attachments = new ArrayList<ExpressoAttachment>();
    @JsonIgnore
    public void addAttachment(ExpressoAttachment value) {
        attachments.add(value);
    }
    @JsonIgnore
    public ListIterator<ExpressoAttachment> getAttachmentsIterator() {
        return attachments.listIterator();
    }


    private Boolean hasAttachment;
    @JsonProperty("has_attachment")
    public void setHasAttachment(Boolean value) {
        hasAttachment = value;
    }
    public Boolean getHasAttachment() {
        return hasAttachment;
    }


    @JsonProperty("signatures")
    private final List<ExpressoSignature> signatures = new ArrayList<ExpressoSignature>();
    @JsonIgnore
    public void addSignature(ExpressoSignature value) {
        signatures.add(value);
    }
    @JsonIgnore
    public ListIterator<ExpressoSignature> getSignaturesIterator() {
        return signatures.listIterator();
    }


    private Boolean hasSignature;
    @JsonProperty("has_signature")
    public void setHasSignature(Boolean value) {
        hasSignature = value;
    }
    public Boolean getHasSignature() {
        return hasSignature;
    }


    private String originalId;
    @JsonProperty("original_id")
    public void setOriginalId(String value) {
        originalId = value;
    }
    public String getOriginalId() {
        return originalId;
    }


    private String folderId;
    @JsonProperty("folder_id")
    public void setFolderId(String value) {
        folderId = value;
    }
    public String getFolderId() {
        return folderId;
    }


    private String note;
    @JsonProperty("note")
    public void setNote(String value) {
        note = value;
    }
    public String getNote() {
        return note;
    }


    //TODO: o que é isso ?
    private String preparedParts;
    @JsonProperty("preparedParts")
    public void setPreparedParts(String value) {
        preparedParts = value;
    }
    public String getPreparedParts() {
        return preparedParts;
    }


    private Boolean readingConf;
    @JsonProperty("reading_conf")
    public void setReadingConf(Boolean value) {
        readingConf = value;
    }
    public Boolean getReadingConf() {
        return readingConf;
    }


    private Importance importance;
    @JsonProperty("importance")
    public void setImportance(Importance value) {
        importance = value;
    }
    public Importance getImportance() {
        return importance;
    }


    //TODO: o que é isso ?
    private String signatureInfo;
    @JsonProperty("signature_info")
    public void setSignatureInfo(String value) {
        signatureInfo = value;
    }
    public String getSignatureInfo() {
        return signatureInfo;
    }


    @JsonProperty("embedded_images")
    private final List<String> embeddedImages = new ArrayList<String>();
    @JsonIgnore
    public void addEmbeddedImage(String value) {
        embeddedImages.add(value);
    }
    @JsonIgnore
    public ListIterator<String> getEmbeddedImagesIterator() {
        return embeddedImages.listIterator();
    }

}
