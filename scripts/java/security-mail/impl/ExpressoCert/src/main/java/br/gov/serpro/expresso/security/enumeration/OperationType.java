/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.serpro.expresso.security.enumeration;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 */
public enum OperationType {
    SIGN, VERIFY, ENCRYPT, DECRYPT, SIGN_AND_ENCRYPT;
}
