
-dontskipnonpubliclibraryclassmembers
-target 1.6
-forceprocessing
#-dontoptimize
-optimizationpasses 6
# see http://sourceforge.net/p/proguard/bugs/462/
-optimizations !code/allocation/variable
-dontobfuscate
-verbose
-keepdirectories

-injars
    lib/httpclient-4.2.6.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/DEPENDENCIES,!META-INF/NOTICE,!META-INF/LICENSE):
    lib/httpcore-4.2.5.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/DEPENDENCIES,!META-INF/NOTICE,!META-INF/LICENSE):
    lib/commons-codec-1.6.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/LICENSE.txt,!META-INF/NOTICE.txt):
    lib/commons-logging-1.1.1.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/LICENSE,!META-INF/NOTICE):
    lib/commons-lang3-3.1.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/LICENSE.txt,!META-INF/NOTICE.txt):
    lib/guava-17.0.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF):
    lib/zip4j-1.3.2.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF):
    lib/jackson-core-asl-1.9.13.jar(!META-INF/MANIFEST.MF,!META-INF/ASL2.0,!META-INF/LICENSE,!META-INF/NOTICE):
    lib/jackson-mapper-asl-1.9.13.jar(!META-INF/MANIFEST.MF,!META-INF/ASL2.0,!META-INF/LICENSE,!META-INF/NOTICE):
    lib/javax.mail-1.5.1.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/LICENSE.txt):
    lib/bcprov-jdk15on-1.50.jar(!META-INF/MANIFEST.MF,!META-INF/BCKEY.SF,!META-INF/BCKEY.DSA):
    lib/bcpkix-jdk15on-1.50.jar(!META-INF/MANIFEST.MF,!META-INF/BCKEY.SF,!META-INF/BCKEY.DSA):
    lib/bcmail-jdk15on-1.50.jar(!META-INF/MANIFEST.MF,!META-INF/BCKEY.SF,!META-INF/BCKEY.DSA):
    lib/jericho-html-3.3.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF):
    lib/apache-mime4j-dom-0.7.2.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/DEPENDENCIES,!META-INF/LICENSE,!META-INF/NOTICE,!META-INF/README):
    lib/apache-mime4j-core-0.7.2.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/DEPENDENCIES,!META-INF/LICENSE,!META-INF/NOTICE):
    lib/picocontainer-2.14.3.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF):
    lib/expressocert-1.0.39.jar(!META-INF/maven/**,!META-INF/MANIFEST.MF,!META-INF/EXPRESSO.SF,!META-INF/EXPRESSO.RSA,!META-INF/INDEX.LIST):
    lib/ExpressoCertMail.jar(!META-INF/maven/**,!META-INF/INDEX.LIST)

-libraryjars
    lib/javax.servlet-api-3.1.0.jar:
    lib/jsr305-2.0.3.jar:
    lib/log4j-1.2.17-smime-applet.jar:
    lib/slf4j-api-1.7.5.jar:
    lib/avalon-logkit-2.1.jar:
    lib/avalon-framework-4.1.4.jar:
    lib/javax.inject-1.jar:
    lib/joda-time-2.3.jar

    #Warning: library class org.apache.log4j.net.SMTPAppender$1 extends or implements program class javax.mail.Authenticator
    # see http://proguard.sourceforge.net/manual/troubleshooting.html#dependency
    #lib/log4j-1.2.17.jar   SUBSTITUIDA POR: lib/log4j-1.2.17-smime-applet.jar


-keep public class br.gov.serpro.expresso.security.applet.SmimeApplet {

    #lifecycle

    public void init();
    public void start();
    public void stop();
    public void destroy();

    #callbacks

    public void signMessage(java.lang.String, java.lang.String, java.lang.String);

    public void encryptMessage(java.lang.String, java.lang.String, java.lang.String);

    public void signAndEncryptMessage(java.lang.String, java.lang.String, java.lang.String);

    public void decryptMessage(java.lang.String, java.lang.String, java.lang.String, boolean);

    public void decryptMessage(java.lang.String, java.lang.String, java.lang.String);

    public void verifyMessage(java.lang.String, java.lang.String, java.lang.String, boolean);

    public void verifyMessage(java.lang.String, java.lang.String, java.lang.String);
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.Opener {
    public Opener(
        br.gov.serpro.expresso.security.applet.openmessage.Decrypter,
        br.gov.serpro.expresso.security.applet.openmessage.SignatureChecker,
        br.gov.serpro.expresso.security.applet.openmessage.Converter);

    public static void main(java.lang.String[]);
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.WebServer {
    public <init>();
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.TokenBroker {
    public <init>();
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.Decrypter {
    public <init>(br.gov.serpro.expresso.security.applet.openmessage.TokenBroker);
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.SignatureChecker {
    public <init>();
}

-keep public class br.gov.serpro.expresso.security.applet.openmessage.Converter {
    public <init>(br.gov.serpro.expresso.security.applet.openmessage.WebServer);
}

-keep @org.codehaus.jackson.annotate.JsonPropertyOrder class * {
    *;
    <init>(...);
}

-keep class br.gov.serpro.expresso.security.applet.JsonException
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoMessage
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoMessage$Importance
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoMessage$Flag
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoAttachment
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoSignature
-keep class br.gov.serpro.expresso.security.applet.openmessage.ExpressoJsonAttachmentResponse$Status

-keep interface org.apache.commons.logging.Log
-keep class org.apache.commons.logging.impl.LogFactoryImpl
-keep class org.apache.commons.logging.impl.Jdk14Logger {
    *;
    <init>(...);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);

    public java.lang.String getDefaultPropertyName();
    public boolean creatorEnabled();
    public boolean fieldEnabled();
    public boolean getterEnabled();
    public boolean isGetterEnabled();
    public boolean setterEnabled();
    public boolean isVisible(java.lang.reflect.Member);
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclassmembers class * {
    @* *;
}

# mailcap handlers
-keep class com.sun.mail.handlers.** {*;}
-keep class org.bouncycastle.mail.smime.handlers.** {*;}


-keep class **509** {
    <init>(...);
    *;
}

-keep class **SHA** {
    <init>(...);
    *;
}

-keep class **RSA** {
    <init>(...);
    *;
}


#Warning: org.picocontainer.paranamer.AnnotationParanamer: can't find referenced class org.picocontainer.paranamer.NullParanamer
-dontwarn org.picocontainer.paranamer.AnnotationParanamer

#Note: com.google.common.base.internal.Finalizer accesses a method 'finalizeReferent()' dynamically
-keep class com.google.common.base.FinalizableReference {
    void finalizeReferent();
}

#Note: com.sun.mail.util.MimeUtil accesses a method 'cleanContentType(javax.mail.internet.MimePart,java.lang.String)' dynamically
-keep class com.sun.mail.util.MimeUtil {
    java.lang.String cleanContentType(javax.mail.internet.MimePart,java.lang.String);
}

#Note: org.apache.http.client.utils.JdkIdn accesses a method 'toUnicode(java.lang.String)' dynamically
-keepclassmembers class org.apache.http.client.utils.Idn { java.lang.String toUnicode(java.lang.String); }
-keepclassmembers class org.apache.http.client.utils.JdkIdn { java.lang.String toUnicode(java.lang.String); }
-keepclassmembers class org.apache.http.client.utils.Punycode { java.lang.String toUnicode(java.lang.String); }
-keepclassmembers class org.apache.http.client.utils.Rfc3492Idn { java.lang.String toUnicode(java.lang.String); }

#Note: com.google.common.cache.Striped64 accesses a declared field 'base' dynamically
#Note: com.google.common.cache.Striped64 accesses a declared field 'busy' dynamically
-dontnote com.google.common.cache.Striped64

#Note: com.google.common.cache.Striped64$Cell accesses a declared field 'value' dynamically
-dontnote com.google.common.cache.Striped64$Cell

#Note: org.apache.commons.lang3.ObjectUtils accesses a method 'clone()' dynamically
-dontnote org.apache.commons.lang3.ObjectUtils

#Note: org.picocontainer.converters.NewInstanceConverter accesses a constructor '<init>(java.lang.String)' dynamically
-dontnote org.picocontainer.converters.NewInstanceConverter

#Note: org.picocontainer.paranamer.DefaultParanamer accesses a declared field '__PARANAMER_DATA' dynamically
-keep public class org.picocontainer.paranamer.AdaptiveParanamer { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.AnnotationParanamer { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.AnnotationParanamer$Jsr330Helper { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.BytecodeReadingParanamer { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.BytecodeReadingParanamer$ClassReader { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.BytecodeReadingParanamer$MethodCollector { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.BytecodeReadingParanamer$Type { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.BytecodeReadingParanamer$TypeCollector { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.CachingParanamer { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.DefaultParanamer { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.ParameterNamesNotFoundException { java.lang.String __PARANAMER_DATA; }
-keep public class org.picocontainer.paranamer.Paranamer { java.lang.String __PARANAMER_DATA; }

#Note: org.picocontainer.visitors.AbstractPicoVisitor$1 accesses a method 'accept(org.picocontainer.PicoVisitor)' dynamically
-keep public class org.picocontainer.PicoVisitor
-keep public class org.picocontainer.ComponentAdapter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.ComponentFactory { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.DefaultPicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.DefaultPicoContainer$KnowsContainerAdapter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.Parameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.PicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.adapters.AbstractAdapter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.behaviors.AbstractBehavior { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.behaviors.AbstractBehaviorFactory { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.behaviors.AdaptingBehavior { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.classname.DefaultClassLoadingPicoContainer$AsPropertiesPicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.containers.AbstractDelegatingPicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.containers.CompositePicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.containers.EmptyPicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.containers.ImmutablePicoContainer { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.AbstractInjectionFactory { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.AbstractInjectionFactory$LifecycleAdapter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.AbstractInjector { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.CompositeInjector { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.FactoryInjector { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.injectors.ProviderAdapter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.BasicComponentParameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.CollectionComponentParameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.ComponentParameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.ConstantParameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.DefaultConstructorParameter { void accept(org.picocontainer.PicoVisitor); }
-keep public class org.picocontainer.parameters.NullParameter { void accept(org.picocontainer.PicoVisitor); }





